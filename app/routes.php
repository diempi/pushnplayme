<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/info',function(){
   echo phpinfo();
});
Route::get('/',['as'=>'home', 'uses'=>'BaseController@index']);

// LIENS DU MENU
Route::get('decouvrir','BaseController@discover');
Route::get('parcourir','BaseController@browse');
Route::get('local','BaseController@local');

//SOCIAL LOGIN
Route::get('login/fb','UserController@fblogin');
Route::get('login/fb/callback','UserController@fblogincallback');

// ROUTES USERS - DJS 
Route::get('login','UserController@login');
Route::get('register',[
    'before' => ['guest'],
    'as' => 'register',
    'uses' => 'UserController@create'
        ]);

Route::post('registered',[
    'before' => ['guest'],
    'as' => 'registered',
    'uses' => 'UserController@store'
        ]);

Route::get('registered',[
    'before' => ['guest'],
    'as' => 'registered',
    'uses' => 'AccessController@notAllowed'
        ]);
Route::post('friend/{id}','UserController@addFriend');
Route::post('unfriend/{id}','UserController@removeFriend');



Route::resource('djs','UserController',['except' => ['show']]);
Route::post('djs/{id}','UserController@update');
Route::get('djs/all','UserController@all');
Route::get('djs/{id}/friends','UserController@friendsList');
Route::get('djs/{id}/podcasts','UserController@podcastsList');

Route::get('djs/{slug}','UserController@slugShow');
Route::get('logout', 'UserController@destroy');

// DESACTIVATION DE COMPTE
Route::post('djs/{id}/disable',['as' => 'disableprofile', 'uses' => 'UserController@deactivateMe']);
Route::delete('djs/{id}/delete',['as' => 'deleteprofile', 'uses' => 'UserController@deleteMe']);

// MODIFICATION DE L'IMAGE DU PROFIL
Route::post('djs/update/profilepict','UserController@updateUserPicture');

//PASSWORD RESET
Route::get('forgot','PasswordResetsController@create');

Route::resource('sessions', 'SessionController',['only' => ['index','create','store','destroy']]);
Route::resource('studio/podcasts','PodcastController');
Route::get('studio/friends','UserController@userfriends');
Route::get('studio/podcasts/create/noverif','PodcastController@noverifform');
Route::post('studio/podcasts/create/noverif','PodcastController@noverifsave');
Route::get('podcasts/all','PodcastController@allpodcasts');
Route::post('studio','AccessController@connection');
Route::get('podcasts/download/{id}','PodcastController@download');
Route::get('podcasts/like/{id}','PodcastController@like');
Route::get('podcasts/play/{id}','PodcastController@play');

// LES GENRES
Route::resource('studio/genres','GenreController');
Route::get('styles','GenreController@allstyles');
Route::get('styles/{slug}','GenreController@singlestyle');

// LES PAYS
Route::resource('countries','CountrieController');

// RAPPELS DE MOT DE PASSE
Route::get('password_resets/reset/{token}', 'PasswordResetsController@reset');
Route::post('password_resets/reset/{token}', 'PasswordResetsController@postReset');
Route::resource('password_resets', 'PasswordResetsController', ['only' => ['create', 'store']]);


// CONTROLEUR D'AJOUT DE NOUVELLE PHOTO
Route::resource('studio/photos','PhotoController');

// AJOUT D'UNE NOUVELLE VIDÉO
Route::resource('studio/videos','VideoController');

// AJOUT D'UN NOUVEL EVENT
Route::resource('studio/events','EventController');

// ROUTES DE L'ACCES UTILISATEUR

Route::get('studio/dashboard','DashboardController@dashboard');

Route::get('studio/edit_profil','UserController@editProfile');

Route::get('studio/manage_podcasts','DashboardController@managePodcasts');

Route::get('studio/media','DashboardController@viewMedias');

Route::get('studio/comments','DashboardController@manageComments');

Route::get('studio/podcast_stats','DashboardController@podcastsStats');

Route::get('studio/add_podcast','PodcastController@create');

Route::get('studio/add_video','DashboardController@addVideo');

Route::get('studio/add_event','DashboardController@addEvent');


// ROUTES D'UNE RECHERCHE
Route::post('search',array('as' => 'search','uses' => 'BaseController@search'));

// ROUTES DE L'ADMIN
Route::get('admin','AdminController@connection');
Route::post('admindashboard','AdminController@verify');
Route::post('adminsave','AdminController@save');
Route::get('adminlogout','AdminController@logout');

//************* test de données

Route::get('/voir',function(){
    if(Auth::user())
    {
        return Auth::user()->roles[0]['name'];
    }
    else{
        return 'visiteur';
    }
});