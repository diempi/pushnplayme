<?php namespace Acme\Mailers;

use User;

class UserMailer extends Mailer {

    public function welcome(User $user)
    {
        $view = 'emails.welcome';
        $data = [];
        $subject = 'Bienvenue sur PushNPlay.me!';

        return $this->sendTo($user, $subject, $view, $data);
    }

    public function cancellation(User $user)
    {
        $view = 'emails.cancel';
        $data = [];
        $subject = 'Nous sommes tristes de vous voir partir mais nous espérons vous revoir!';

        return $this->sendTo($user, $subject, $view, $data);

    }
}