<?php namespace Acme\Mailers;

use Mail;

abstract class Mailer {

    public function sendTo($user, $subject, $view, $data = [])
    {
        Mail::pretend($view, $data, function($message) use($user, $subject)
        {
            $message->to($user->email)
                    ->subject($subject);
        });
    }

}