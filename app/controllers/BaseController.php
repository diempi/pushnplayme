<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

    public function index(){
        $djs = User::all();
        $genres = Genre::all();
        unset($genres[0]);
        $genreClasses = array("smalltag","bigtag","mediumtag");
        return View::make('home.index',compact('djs','genres','genreClasses'));
    }
    public function discover(){
        return View::make('decouvrir');
    }

    public function browse(){
        return View::make('parcourir');
    }

    public function local(){
        return View::make('local');
    }


    public function search(){
        // on récupère la recherche
        $query = Input::get('topsearchform');
        $users = User::all();
        // On fait une recherche dans les utilisateurs
        $results = User::where('username', 'LIKE', '%'.$query.'%')->get();
        return View::make('search.results')->with('results', $results);
    }

}
