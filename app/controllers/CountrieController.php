<?php 

	class CountrieController extends BaseController
	{

		public function index(){
			$countries = Country::all();
			return View::make('countries.index')->with('countries', $countries);
		}

		public function show($slug){
			$country = Country::where('countrySlug','=', $slug)->first();
			$users = $country->users;
			return View::make('countries.allusers')->with(array('users'=> $users, 'country' => $country));

		}


	}