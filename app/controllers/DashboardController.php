<?php 

	/**
	* 
	*/
	class DashboardController extends BaseController
	{
		
		function __construct()
		{
			# code...
		}

		public $restful = true;

		public function adminAccess(){
				// create view
					    	// On sauvegarde la date de dernière connexion
					    	Auth::user()->last_connection = date("Y-m-d H:i:s");
			return View::make('user.dashboard')->with('podcasts', $podcasts)->with('flash_message','Connection réussie!')->with('flash_type','flash-success');

		}

		public function dashboard(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$podcasts = $user->podcasts;
			$activities = $user->activities->sortByDesc('created_at');
			return View::make('user.dashboard',array('podcasts' => $podcasts, 'activities' => $activities));
		}


		public function managePodcasts(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$podcasts = $user->podcasts;
			return View::make('user.manage_podcasts')->with('podcasts', $podcasts)->with('flash_message','<span class="icon-ok"></span> Votre fichier a bien été modifié')->with('flash_type','flash-success');
		}

		public function manageComments(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$comments = $user->comments;
			return View::make('user.comments')->with('comments', $comments);			
		}

		public function podcastsStats(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$podcasts = Podcast::where('BelongsTo', Auth::user()->id)->get();
			return View::make('user.podcast_stats')->with('podcasts', $podcasts);
		}

		public function addPodcast(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('user.add_podcast');
		}

		public function saved(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('user.mix-saved');
		}

		public function uploadMix(){

			$file = Input::file('file');
/*
			// Validation
	        $rules = array(
	             'file' => 'file|required|mimes:audio/mpga|max:200000',
	        );

			$input = Input::all();

	         $validation = Validator::make($input, $rules);

	         if ($validation->fails())
	         {
	           return Redirect::to('add_podcast')->withErrors($validation);
	         }		*/	
			$destinationPath = public_path().'/uploads/';
			$filename = $file->getClientOriginalName();
			$uploadSuccess = $file->move($destinationPath, $destinationPath.$filename);

					if( $uploadSuccess ) {
			 			$podcast = new Podcast;
						$user = Session::get('user');

						$podcast->title = $data['title'] = Input::get('podTitle');	
						$podcast->description =  $data['description'] = Input::get('podDescription');	
						$podcast->picture = $data['picture'] = '../assets/img/'.'defaultmusicpict.jpg';	
			 			$podcast->location = $data['location'] = $destinationPath.$filename;
			 			$date = new DateTime;
			 			$podcast->created_at = $data['dateCreated'] =  $date->format('Y-m-d H:i:s');
			 			$podcast->updated_at = $data['dateUpdated'] =  $date->format('Y-m-d H:i:s');
			 			$podcast->status = $data['status'] = 'published';
			 			$podcast->length = $data['length'] = 0;
			 			$podcast->listenNb = $data['listenNb'] = 0;
			 			$podcast->DownloadNb = $data['downloadNb'] = 0;
			 			$podcast->BelongsTo = Auth::user()->id;
			 			$podcast->save();
			 			// Sauvegarde des données de la session
			 			Input::flash(); 
						return View::make('user.mix-saved');
					} else {
						return 'Vous devez sélectionner un fichier à envoyer';
					}

		}

		public function addPhoto(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('user.add_photo');
		}

		public function newPhoto(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$photo = new Photo;
			$user = Auth::user();
			$newphotofile = Input::file('newPhoto');
				
			// $destinationPath = public_path().'/assets/img/';
			// $filename = $newphotofile->getClientOriginalName();
			// $uploadSuccess = $newphotofile->move($destinationPath, $destinationPath.$filename);
			// $chemin =  '/assets/img/'.$filename;
			$fileName = time().'-'.$newphotofile->getClientOriginalName();
			$fileExtension = $newphotofile->getClientOriginalExtension();
			$file =  str_replace('.' . $fileExtension, '', $fileName);
			$file = Str::slug($file,'-');
			$file = $file.'.'.$fileExtension;
			$destinationPath = public_path().'/assets/img/';

			// upload new image
			Image::make($newphotofile->getRealPath())
			// original
			->save($destinationPath.$fileName)
			// thumbnail
			->resize(279, null, function ($constraint) {
			    $constraint->aspectRatio();
			})
			->crop(279,279)
			->save($destinationPath.'thumb_'.$fileName);

			$photo ->photoLocation = '/assets/img/'.$fileName;
			$photo ->photoThumbnail = '/assets/img/thumb_'.$fileName;
			$photo ->photoTitle = Input::get('photoTitle');
			$photo ->photoSlug = Str::slug($fileName,'-');
			$photo ->userId = $user->id;

			$photo->save();			

			$photos = $user->photos;
			$videos = $user->videos;
			return View::make('user.media', array('photos' => $photos, 'videos' => $videos));
		}		

		public function addVideo(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('user.add_video');
		}
		
		public function newVideo(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$video = new Video;
			$user = Auth::user();
			$video->videoLink = Input::get('videoLink');
			$video->videoTitle = Input::get('videoTitle');
			$video->videoSlug = Str::slug(Input::get('videoTitle'),'-');
			$video->userId = $user->id;
			$video->videoThumbnail = (Input::get('videoThumbnail' !== null)) ? Input::get('videoThumbnail') : 'lien généré' ;
			$video->save();
			// lien pour avoir le thumbnail HQ youtube http://i.ytimg.com/vi/ID-VIDEO/maxresdefault.jpg
			// lien pour avoir le thumbnail HQ dailymotion  http://www.dailymotion.com/thumbnail/video/video_id
			// lien pour avoir le thumbnail HQ vimeo http://vimeo.com/api/v2/video/video_id.output
			$photos = $user->photos;
			$videos = $user->videos;
			return View::make('user.media', array('photos' => $photos, 'videos' => $videos));
		}

		public function addEvent(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('user.add_event');
		}

		public function newEvent(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$event = new DJEvent;
			$user = Auth::user();

			// On récupère les données du formulaire:

			$event ->eventDate =  date("yy-mm-dd", strtotime(Input::get('eventDate')));
			$event ->eventName = Input::get('eventTitle');
			$event ->eventPlace = Input::get('eventPlace');
			$event ->eventTicketLink = Input::get('eventTicket');
			$event ->eventSlug = Str::slug(Input::get('eventTitle'),'-');
			$event ->userId = $user->id;

			$event->save();
			 dd($event);
			//return View::make('user.add_event');
		}

		public function viewMedias(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$photos = $user->photos;
			$videos = $user->videos;
			return View::make('user.media', array('photos' => $photos, 'videos' => $videos));		
		}

	}