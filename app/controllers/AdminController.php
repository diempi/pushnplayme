<?php  

	/**
	* 
	*/
	class AdminController extends BaseController
	{
		
		function __construct()
		{
			# code...
		}

		public function connection(){
			return View::make('admin.login');
		}

		public function verify(){
			if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'role'=>'admin'))) {
			    $users = User::all();
			    return View::make('admin.dashboard')->with('users', $users)->with('flash_message','Bienvenue Admin!!')->with('flash_type','flash-success');
			}

			return Redirect::back()->withInput()->withErrors();
		}

		public function save(){
			$users = User::all();
			// On récupère les statuts
			foreach ($users as $user) {
				$user->banned = Input::get($user->username);
				// on met à jour la BD
				$user->save();
			}
			$newusers = User::all();
			return View::make('admin.dashboard')->with('users', $newusers)->with('flash_message','Vous avez bien mis à jour vos utilisateurs')->with('flash_type','flash-success');
		}

		public function logout(){
		    Auth::logout();
		    return Redirect::to('admin')->with('flash_message','Vous vous êtes déconnecté avec succès')->with('flash_type','flash-success');
		}
	}