<?php 

	/**
	* 
	*/
	class PhotoController extends BaseController
	{

		public function index(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$photos = Auth::user()->photos;
			return View::make('photos.index')->with('photos',$photos);
		}

		public function create(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('photos.create');
		}

		public function store(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
				Input::flash();
				// tion de photo
				$rules = array(
						'photoTitle' => 'required',
						'newPhoto' => 'required|mimes:jpg,jpeg,png'
					);
				$input = Input::all();
				
				$validation = Validator::make($input, $rules);
				if($validation->fails())
				{
					$errors = $validation->messages();
					return View::make('photos.create')->withInput(Input::all())->withErrors($errors);
				}

				// Champs validés
				$photo = new Photo;
				$user = Auth::user();
				$newphotofile = Input::file('newPhoto');
					
				$fileName = time().'-'.$newphotofile->getClientOriginalName();
				$fileExtension = $newphotofile->getClientOriginalExtension();
				$file =  str_replace('.' . $fileExtension, '', $fileName);
				$file = Str::slug($file,'-');
				$file = $file.'.'.$fileExtension;
				$destinationPath = public_path().'/assets/img/';

				// upload new image
				Image::make($newphotofile->getRealPath())
				// original
				->save($destinationPath.$fileName)
				// thumbnail
				->resize(279, null, function ($constraint) {
				    $constraint->aspectRatio();
				})
				->crop(279,279)
				->save($destinationPath.'thumb_'.$fileName);

				$photo ->photoLocation = '/assets/img/'.$fileName;
				$photo ->photoThumbnail = '/assets/img/thumb_'.$fileName;
				$photo ->photoTitle = Input::get('photoTitle');
				$photo ->photoSlug = Str::slug($fileName,'-');
				$photo ->userId = $user->id;

				$photo->save();		
			
				// Activité
				$activity = new Activity;
				$activity->userId = Auth::user()->id;
				$date = new DateTime;
				$activity->type = '<span class="icon-plus adding"> Vous avez ajouté la photo '.$photo->photoTitle.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>.";
				$activity->save();

				$photos = $user->photos;
				$videos = $user->videos;
				return Redirect::to('studio/photos')->with(array('photos' => $photos, 'videos' => $videos))->with('flash_message','Votre photo a bien été ajoutée')->with('flash_type','flash-success');
		}

		public function show(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$photos = $user->photos;
			$videos = $user->videos;
			return View::make('photos.index', array('photos' => $photos, 'videos' => $videos));
		}

		public function edit($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$photo = Photo::find($id);
			return View::make('photos.edit')->with('photo',$photo);
		}		


		public function update($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$photo = Photo::find($id);
			$user = Auth::user();
			$rules = array(
						'userPhoto' => 'mimes:jpg,jpeg,png'
					);
				$input = Input::all();
				
				$validation = Validator::make($input, $rules);
				if($validation->fails())
				{
					Input::flash();
					$errors = $validation->messages();
					return View::make('photos.edit')->with('photo',$newphoto)->withInput(Input::all())->withErrors($errors);
				}

				// Si un fichier est envoyé on l'ajoute
				if(Input::hasFile('userPhoto'))
				{
					$newphotofile = Input::file('userPhoto');
						
					$fileName = time().'-'.$newphotofile->getClientOriginalName();
					$fileExtension = $newphotofile->getClientOriginalExtension();
					$file =  str_replace('.' . $fileExtension, '', $fileName);
					$file = Str::slug($file,'-');
					$file = $file.'.'.$fileExtension;
					$destinationPath = public_path().'/assets/img/';

					// upload new image
					Image::make($newphotofile->getRealPath())
					// original
					->save($destinationPath.$fileName)
					// thumbnail
					->resize(279, null, function ($constraint) {
					    $constraint->aspectRatio();
					})
					->crop(279,279)
					->save($destinationPath.'thumb_'.$fileName);

					$photo ->photoLocation = '/assets/img/'.$fileName;
					$photo ->photoThumbnail = '/assets/img/thumb_'.$fileName;
					$photo ->photoSlug = Str::slug($fileName,'-');
					$photo ->userId = $user->id;
				}

				// on sauvegarde le titre
				$photo ->photoTitle = Input::get('photoTitle');
				$photo->save();	
			
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-pencil edition"> Vous avez modifié la photo '.$photo->photoTitle.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>.";
			$activity->save();				
			
			$photos = $user->photos;
			return Redirect::to('studio/photos')->with('photos',$photos)->with('flash_message','Votre photo a bien été modifiée')->with('flash_type','flash-success');
		}		

		public function destroy($id){
			$photo = Photo::find($id);
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-trash cancelling"> Vous avez supprimé la photo '.$photo->photoTitle.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>.";
			$activity->save();
			$photo->delete();
			$refreshedData = Auth::user()->photos;
			
  			return Redirect::to('photos.index')->with('data', $refreshedData);
		}

	}