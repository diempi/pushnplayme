<?php 

	class VideoController extends BaseController
	{

		public function index(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$videos = Auth::user()->videos;
			return View::make('videos.index')->with('videos',$videos);
		}

		public function create(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('videos.create');
		}

		public function store(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$video = new Video;
			$user = Auth::user();

			// Validation
			$rules = array(
				'videoTitle' => 'required',
				'videoLink' => 'required|url',
				'videoThumbnail' => 'mimes:jpg,png,jpeg'
				);
			$input = Input::all();
	        $validation = Validator::make($input, $rules);

	        if($validation->passes())
	        {
				$video->videoLink = Input::get('videoLink');
				$video->videoTitle = Input::get('videoTitle');
				$video->videoSlug = Str::slug(Input::get('videoTitle'),'-');
				$video->userId = $user->id;
				
				if(Input::hasFile('videoThumbnail')){	
					$newThumb = Input::file('videoThumbnail');
					$fileName = time().'-'.$newThumb->getClientOriginalName();
					$fileExtension = $newThumb->getClientOriginalExtension();
					$file =  str_replace('.' . $fileExtension, '', $fileName);
					$file = Str::slug($file,'-');
					$file = $file.'.'.$fileExtension;
					$destinationPath = public_path().'/assets/img/';

					// upload new image
					Image::make($newThumb->getRealPath())
					// original
					->save($destinationPath.$fileName)
					// thumbnail
					->resize(305, null, function ($constraint) {
					    $constraint->aspectRatio();
					})
					->crop(305,228)
					->save($destinationPath.'videothumb_'.$fileName);
					$video->videoThumbnail = '/assets/img/videothumb_'.$fileName;
				}
				else{

							$url = Input::get('videoLink');
							$id = preg_match('@([v=])(\w{11})@', $url,$matches);
							$thumbLink = 'http://i.ytimg.com/vi/'.$matches[2].'/0.jpg';

							$fileName = time().'-'.Str::slug($video->videoTitle,'-');
							Image::make($thumbLink)->resize(null, 228, function ($constraint) {
							    $constraint->aspectRatio();
							})->crop(305, 228)->save(public_path().'/assets/img/videothumb_'.$fileName.'.jpg');
							$video->videoThumbnail = '/assets/img/videothumb_'.$fileName.'.jpg';
					
				}
				$video->save();
			
				// Activité
				$activity = new Activity;
				$activity->userId = Auth::user()->id;
				$activity->type = '<span class="icon-plus adding"> Vous avez ajouté la vidéo '.$video->videoTitle.' à '.$video->created_at."</span>";
				$activity->save();

				$videos = $user->videos;
				return Redirect::to('studio/videos')->with('data', $videos)->with('flash_message','Votre vidéo a bien été ajoutée')->with('flash_type','flash-success');
	        }
	        $messages = array(
			    'videoTitle.required' => 'Veuillez entrer un titre svp',
			    'videoLink.required' => 'Oups, vous avez oublié de mettre un lien'
			);
	        Input::flash();
	        $errors = $validation->messages();
	       return View::make('videos.create')->withErrors($errors)->withInput(Input::all()); 

		}

		public function edit($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$video = Video::find($id);
			return View::make('videos.edit')->with('video',$video);
		}

		public function update($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$video = Video::find($id);

			// Validation des champs
			$rules = array(
				 'videoLink' => 'url',
				'videoThumbnail' => 'mimes:jpg,png,jpeg'
				);
			$input = Input::all();
	        $validation = Validator::make($input, $rules);

	        if($validation->fails()){
		        Input::flash();
		        $errors = $validation->messages();
		        return View::make('videos.edit')->withErrors($errors)->withInput(Input::all()); 	        	
	        }

	        // Modification de la vidéo
				
				if(Input::hasFile('videoThumbnail')){	
					$newThumb = Input::file('videoThumbnail');
					$fileName = time().'-'.$newThumb->getClientOriginalName();
					$fileExtension = $newThumb->getClientOriginalExtension();
					$file =  str_replace('.' . $fileExtension, '', $fileName);
					$file = Str::slug($file,'-');
					$file = $file.'.'.$fileExtension;
					$destinationPath = public_path().'/assets/img/';

					// upload new image
					Image::make($newThumb->getRealPath())
					// original
					->save($destinationPath.$fileName)
					// thumbnail
					->resize(305, null, function ($constraint) {
					    $constraint->aspectRatio();
					})
					->crop(305,228)
					->save($destinationPath.'videothumb_'.$fileName);
					$video->videoThumbnail = '/assets/img/videothumb_'.$fileName;
				}
				else{
							$url = Input::get('videoLink');
							$id = preg_match('@([v=])(\w{11})@', $url,$matches);
							$thumbLink = 'http://i.ytimg.com/vi/'.$matches[2].'/0.jpg';

							$fileName = time().'-'.Str::slug($video->videoTitle,'-');
							//dd($fileName);
							Image::make($thumbLink)->resize(null, 228, function ($constraint) {
							    $constraint->aspectRatio();
							})->crop(305, 228)->save(public_path().'/assets/img/videothumb_'.$fileName.'.jpg');
							$video->videoThumbnail = '/assets/img/videothumb_'.$fileName.'.jpg';
					}

				$video->videoLink = Input::get('videoLink');
				$video->videoTitle = Input::get('videoTitle');
				$video->videoSlug = Str::slug(Input::get('videoTitle'),'-');
				$video ->save();
				$videos = $user->videos;
			
				// Activité
				$activity = new Activity;
				$activity->userId = Auth::user()->id;
				$activity->type = '<span class="icon-pencil edition"> Vous avez modifié la vidéo '.$video->videoTitle.' à '.$video->updated_at."</span>";
				$activity->save();
				return Redirect::to('studio/videos')->with('data', $videos)->with('flash_message','Votre vidéo a bien été modifiée')->with('flash_type','flash-success');	        
		}		

		public function destroy($id){
			$video = Video::find($id);
			$date = new DateTime;
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$activity->type = '<span class="icon-trash cancelling"> Vous avez supprimé la vidéo '.$video->videoTitle.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			$video->delete();

			$videos = Auth::user()->videos;
  			return View::make('videos.index')->with('videos', $videos);

		}

	}