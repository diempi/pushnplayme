<?php 

	/**
	* 
	*/
	class PodcastController extends BaseController
	{
		
		function __construct()
		{
			# code...
		}

		// Afficher les podcasts d'un utilisateur
		public function index(){
			$userPodcasts = Auth::user()->podcasts;
			return View::make('podcasts.index')->with('podcasts', $userPodcasts);
		}

		public function create(){
			return View::make('podcasts.create');
		}

		public function store(){
			
 			$file = Input::file('podcastFile');
 			//Validation
	        $rules = array(
	        	'podTitle' => 'required',
	            'podcastFile' => 'required|mimes:mpga|max:200000'
	        );
			
			$input = Input::all();

	         $validation = Validator::make($input, $rules);
	         if ($validation->fails())
	         {
	         	//Input::flash();
	         	$errors = $validation->messages();
				return Response::json(array('success' => false,'errors' => $errors));
				//return Response::json(array('success' => false,'errors' => $validation->errors()->toArray()));
	         }
	         else{
			        // Ajout du podcast 
			        $file = Input::file('podcastFile');
					$destinationPath = public_path().'/uploads/';
					$filename = time().'-'.$file->getClientOriginalName();	
					//$filename = time().'-'.Str::slug(Input::get('podTitle'),'-');	
					$fileExtension = $file->getClientOriginalExtension();
							
					$fileUp =  str_replace('.' . $fileExtension, '', $filename);
					$fileUp = Str::slug($fileUp,'-');

					$fileUploaded = $fileUp.'.'.$fileExtension;
					$uploadSuccess = $file->move($destinationPath, $fileUploaded);
					if( $uploadSuccess ) {
						$podcast = new Podcast;
						$user = Auth::user();
						$podcast->title = Input::get('podTitle');	
						$podcast->description = Input::get('podDescription');
						$podcast->location = '/uploads/'.$fileUploaded;
						$date = new Carbon;
						$date = Carbon::now();
						$podcast->created_at =  $date->format('Y-m-d H:i:s');
						$podcast->updated_at =  $date->format('Y-m-d H:i:s');
						$podcast->status = Input::get('podcastStatus');
						$podcast->slug = $fileUp;	
						$podcast->length = 0;
						$podcast->listenNb = 0;
						$podcast->DownloadNb = 0;
						$podcast->BelongsTo = $user->id;
                        // Image du podcast
                        $imgfile = $user->picture;
                        $destinationPath = public_path().'/assets/img/';
                        Image::make($imgfile->getRealPath())
                            // original
                            ->resize(300, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })
                            ->crop(300,300)
                            ->save($destinationPath.$imgfilename);
                        $podcast->picture = public_path().$imgfilename;


                        // Thumb

                        Image::make($imgfile->getRealPath())
                            // original
                            ->resize(100, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })
                            ->crop(100,100)
                            ->save($destinationPath.$imgfilename.'_thumb');
                        $podcast->pictureThumb = public_path().$imgfilename;
						$podcast->save();

						// Activité
						$activity = new Activity;
						$activity->userId = Auth::user()->id;
						$activity->type = '<span class="icon-plus adding"> Vous avez ajouté le podcast '.$podcast->title.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>.";
						$activity->save();
						$podcasts = Auth::user()->podcasts;
						return Response::json(array('success' => true), 200);
						//return Redirect::to('studio/podcasts')->with('data', $podcasts)->with('flash_message','Votre fichier a bien été ajouté')->with('flash_type','flash-success');
					} 	         	
	         }


		}

		// MODIFICATION DU PODCAST: Chargement du formulaire de modification
		public function edit($id){
			$podcast = Podcast::find($id);
			return View::make('podcasts.edit')->with('podcast',$podcast);
		}
		
		// MODIFICATION DU PODCAST: Sauvegarde de la modification
		public function update($id){
			$podcast= Podcast::find($id);
			// changement de fichier
			//Validation

	        $rules = array(
	        	//'podTitle' => 'required',
	            'podcastFile' => 'mimes:mpga|max:200000'
	        );
			
			$input = Input::all();

	         $validation = Validator::make($input, $rules);
	         if ($validation->fails())
	         {
	         	Input::flash();
	         	$errors = $validation->messages();
	         	return View::make('podcasts.edit')->withInput(Input::all())->withErrors($errors);
	         }
			
	        // Ajout du podcast 
	         if(Input::hasFile('podcastFile')){
		        $file = Input::file('podcastFile');
				$destinationPath = public_path().'/uploads/';
				$filename = time().'-'.$file->getClientOriginalName();	
				$imgfilename = time().'-'.Str::slug(Input::get('podTitle'),'-');
				$fileExtension = $file->getClientOriginalExtension();
						
				$fileUp =  str_replace('.' . $fileExtension, '', $filename);
				$fileUp = Str::slug($fileUp,'-');
				$fileUploaded = $fileUp.'.'.$fileExtension;
				$uploadSuccess = $file->move($destinationPath, $fileUploaded);
				if( $uploadSuccess ) {
					$podcast= Podcast::find($id);
					$user = Auth::user();
					$podcast->title = Input::get('title');	
					$podcast->description =  Input::get('description');
					$podcast->location =  '/uploads/'.$fileUploaded;
					$podcast->slug = $fileUp;	
					$podcast->length =  0;
					$podcast->listenNb = 0;
					$podcast->DownloadNb = 0;
					$podcast->likesNb = 0;
					$podcast->BelongsTo = Auth::user()->id;	
					$podcast->status = Input::get('status');

                    // Image du podcast
                    $imgfile = $user->picture;
                    $destinationPath = public_path().'/assets/img/';
                    Image::make($imgfile->getRealPath())
                        // original
                        ->resize(300, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->crop(300,300)
                        ->save($destinationPath.$imgfilename);
                    $podcast->picture = public_path().$imgfilename;


                        // Thumb

                    Image::make($imgfile->getRealPath())
                        // original
                        ->resize(100, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })
                        ->crop(100,100)
                        ->save($destinationPath.$imgfilename.'_thumb');
                    $podcast->pictureThumb = public_path().$imgfilename;


					$podcast->save();
					$podcasts = Auth::user()->podcasts;
					// Activité
					$activity = new Activity;
					$activity->userId = Auth::user()->id;
					$date = new DateTime;
					$activity->type = '<span class="icon-pencil edition"> Vous avez modifié le podcast '.$podcast->title.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
					$activity->save();
					return Redirect::to('studio/podcasts')->with('podcasts', $podcasts)->with('flash_message','Votre fichier a bien été modifié')->with('flash_type','flash-success');
				}
			}
			$podcast->title = Input::get('title');
			$podcast->description = Input::get('description');
			$podcast->status = Input::get('status');
			$podcast->save();
			$podcasts = Auth::user()->podcasts;
				
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-pencil edition"> Vous avez modifié le podcast '.$podcast->title.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			return Redirect::to('studio/podcasts')->with('podcasts', $podcasts)->with('flash_message','Votre fichier a bien été modifié')->with('flash_type','flash-success');
		}

		// ALL PODCASTS
		public function allpodcasts(){

			$podcasts = Podcast::paginate(10);
			$djs = User::all();
			return View::make('podcasts.all')->with(array('podcasts' => $podcasts, 'djs' => $djs));
		}

		// DOWNLOAD
		public function download($id){
			$podcast = Podcast::find($id);
			$user = User::find($podcast->BelongsTo);
			$username = $user->username;
			$path = public_path().$podcast->location;

			$filerawname = Str::slug($podcast->title,'-');
			$username = Str::slug($username,'-');
			$filename = $username.'_'.$filerawname;
			$headers = array(
              'Content-Type: audio/mpeg',
            );
			$podcast->DownloadNb = $podcast->DownloadNb +1;
			// Activité
			$activity = new Activity;
			$date = new DateTime;
			$activity->userId = $podcast->BelongsTo;
			$activity->type = '<span class="icon-download"> Le podcast '.$podcast->title.' a été téléchargé le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			$podcast->save();

			return Response::download($path,$filename.'.mp3',$headers);
		}
		// LIKE
		public function like($id){
			$user = Auth::user();
			$podcast = Podcast::find($id);
			$podcast->likesNb = $podcast->likesNb +1;
			// Activité
			$activity = new Activity;
			$date = new DateTime;
			$activity->userId = $podcast->BelongsTo;
			$activity->type = '<span class="icon-star favoriting"> Le podcast '.$podcast->title.' a été mis en favori par '.$user->username .' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			$podcast->save();
		}

		// PLAY
		public function play($id){
			$podcast = Podcast::find($id);		
			$podcast->ListenNb = $podcast->ListenNb +1;
			// Activité
			$activity = new Activity;
			$date = new DateTime;
			$activity->userId = $podcast->BelongsTo;
			$activity->type = '<span class="icon-headphones"> Le podcast '.$podcast->title.' a été joué à '.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			$podcast->save();
		}

		public function destroy($id){
			$podcast = Podcast::find($id);
			$date = new DateTime;
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$activity->type = '<span class="icon-trash cancelling"> Vous avez supprimé le podcast '.$podcast->title.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();

			$podcast->delete();
			$podcasts = Auth::user()->podcasts;

  			return Redirect::to('podcasts.index')->with('podcasts', $podcasts);

		}

// NO VERIF
		public function noverifform(){
			return View::make('podcasts.createnoverif');
		}
		
		public function noverifsave(){
			
 			$file = Input::file('podcastFile');
 		// 	//Validation
	  //       $rules = array(
	  //       	'podTitle' => 'required',
	  //           'podcastFile' => 'required|mimes:mpga|max:200000'
	  //       );
			
			// $input = Input::all();

	  //        $validation = Validator::make($input, $rules);
	  //        if ($validation->fails())
	  //        {
	  //        	Input::flash();
	  //        	$errors = $validation->messages();
	  //        	return View::make('podcasts.create')->withInput(Input::all())->withErrors($errors);
	  //        	//return Response::json(['success' => false,'error' => true, 'errors' => $validation->errors()->toArray()]);
	  //        }

	        // Ajout du podcast 
	        $file = Input::file('podcastFile');
			$destinationPath = public_path().'/uploads/';
			$filename = time().'-'.$file->getClientOriginalName();	
			//$filename = time().'-'.Str::slug(Input::get('podTitle'),'-');	
			$fileExtension = $file->getClientOriginalExtension();
					
			$fileUp =  str_replace('.' . $fileExtension, '', $filename);
			$fileUp = Str::slug($fileUp,'-');

			$fileUploaded = $fileUp.'.'.$fileExtension;
			$uploadSuccess = $file->move($destinationPath, $fileUploaded);
			if( $uploadSuccess ) {
				$podcast = new Podcast;
				$user = Session::get('user');
				$podcast->title = Input::get('podTitle');	
				$podcast->description = Input::get('podDescription');	
				$podcast->picture = public_path().'/assets/img/'.'defaultmusicpict.jpg';	
				$podcast->location = '/uploads/'.$fileUploaded;
				$date = new DateTime;
				$podcast->created_at =  $date->format('Y-m-d H:i:s');
				$podcast->updated_at =  $date->format('Y-m-d H:i:s');
				$podcast->status = Input::get('podcastStatus');
				$podcast->slug = $fileUp;	
				$podcast->length = 0;
				$podcast->listenNb = 0;
				$podcast->DownloadNb = 0;
				$podcast->BelongsTo = Auth::user()->id;
				$podcast->save();

				// Activité
				$activity = new Activity;
				$activity->userId = Auth::user()->id;
				$activity->type = '<span class="icon-plus adding"> Vous avez ajouté le podcast '.$podcast->title.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>.";
				$activity->save();
				$podcasts = Auth::user()->podcasts;
				return View::make('podcasts.index',array('podcasts' => $podcasts, 'flash_message' => 'Votre fichier a bien été ajouté', 'flash_type' => 'flash-success'));
			} 

		}

	}