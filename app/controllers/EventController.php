<?php 

	/**
	* 
	*/
	class EventController extends BaseController
	{

		public function index(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$events = $user->djevents;
			return View::make('events.index')->with('events', $events);
		}

		public function create(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			return View::make('events.create');
		}

		public function store(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$event = new DJEvent;
			$user = Auth::user();

			// Validation du formulaire
			$rules = array(
				'eventDate' => 'required|date',
				'eventTitle' => 'required',
				'eventPlace' => 'required',
				'eventTicket' => 'url'
				);
			$input = Input::all();
			$validation = Validator::make($input, $rules);
			if($validation->fails())
			{
	        $messages = array(
			    'eventDate.required' => 'Veuillez entrer une date svp',
			    'eventTitle.required' => 'Le nom de l\' évènement est obligatoire',
			    'eventPlace.required' => 'Le lieu de l\' évènement est obligatoire',
			    'eventTicket.active_url' => 'Vous avez entré un lien invalide',
			);
				Input::flash();
				$errors = $validation->messages();
	       		return View::make('events.create')->withErrors($errors)->withInput($input);	
			}
			
			// On récupère les données du formulaire:
			$event ->eventDate =  date("Y-m-d", strtotime(Input::get('eventDate')));
			$event ->eventName = Input::get('eventTitle');
			$event ->eventPlace = Input::get('eventPlace');
			$event ->eventTicketLink = Input::get('eventTicket');
			$event ->eventSlug = Str::slug(Input::get('eventTitle'),'-');
			$event ->userId = $user->id;
			$event->save();

			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-plus adding"> Vous avez ajouté l\'évènement '.$event->eventName.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();	
			$user = Auth::user();
			$events = $user->djevents;
			return Redirect::to('studio/events')->with('data', $events)->with('flash_message','Votre évènement a bien été ajouté')->with('flash_type','flash-success');			
		}


		public function edit($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$event = DJEvent::find($id);
			return View::make('events.edit')->with('event',$event);
		}	

		public function update($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$event = DJEvent::find($id);
			

			// Validation du formulaire
			$rules = array(
                //'eventDate' => 'required|date',
                'eventTitle' => 'required',
                'eventPlace' => 'required',
                'eventTicket' => 'url'
				);
			$input = Input::all();
			$validation = Validator::make($input, $rules);
			if($validation->fails())
			{
	        $messages = array(
			   // 'eventDate.required' => 'Veuillez entrer une date svp',
			    'eventTitle.required' => 'Le nom de l\' évènement est obligatoire',
			    'eventPlace.required' => 'Le lieu de l\' évènement est obligatoire',
			    'eventTicket.active_url' => 'Vous avez entré un lien invalide',
			);
				Input::flash();
				$errors = $validation->messages();
	       		return View::make('events.edit')->withErrors($errors)->withInput($input);	
			}
			$user = Auth::user();
			// On récupère les données du formulaire:
            if(Input::get('eventDate') != Input::old('eventDate'))
            {
                $event ->eventDate =  date("Y-m-d", strtotime(Input::get('eventDate')));
            }


			$event ->eventName = Input::get('eventTitle');
			$event ->eventPlace = Input::get('eventPlace');
			$event ->eventTicketLink = Input::get('eventTicket');
			$event ->eventSlug = Str::slug(Input::get('eventTitle'),'-');
			$event ->userId = $user->id;
			$event->save();

			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-pencil edition"> Vous avez modifié l\'évènement '.$event->eventName.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();

			$events = $user->djevents;
			return Redirect::to('studio/events')->with('data', $events)->with('flash_message','Votre évènement a bien été modifié')->with('flash_type','flash-success');	
		}

		public function destroy($id){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$event = DJEvent::find($id);
			// Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-trash cancelling"> Vous avez supprimé l\'évènement '.$event->eventName.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s')."</span>";
			$activity->save();
			$event->delete();
			$events = Auth::user()->djevents;
			return View::make('events.index')->with('events', $events);
		}

	}