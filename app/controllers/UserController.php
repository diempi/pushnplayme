<?php 

	/**
	* 
	*/
	class UserController extends BaseController
	{
		// Formulaire de connexion
		public function index(){
			$djs = User::all();
			return View::make('user.index')->with('djs',$djs);
		}

		// SOCIAL LOGINS
		//Facebook
		public function fblogin(){
			$facebook = new Facebook(Config::get('facebook'));
			$params = array(
					'redirect_uri' => url('/login/fb/callback'),
					'scope' => 'email',
				);
			return Redirect::away($facebook->getLoginUrl($params));
		}

		public function fblogincallback(){
			$code = Input::get('code');
			if(strlen($code) == 0) return Redirect::away('register')->with('message','Erreur de connection à facebook');

			$facebook = new Facebook(Config::get('facebook'));
			$uid = $facebook->getUser();

			if($uid == 0) return Redirect::away('register')->with('message','Erreur de connection à facebook');

			$me = $facebook->api('/me');

			$profile = Profile::whereUid($uid)->first();
			if(empty($profile)){
				$user = new User;
				$user->username = $me['first_name'].' '.$me['last_name'];
				$user->email = $me['email'];
				$user->picture = 'https://graph.facebook.com/'.$me['id'].'/picture?type=large';
				$user ->slug = Str::slug(Input::get('username'),'-');

				$user ->banned = 0;
				$user ->status = 1;	
				$user ->save();


                $user->roles()->attach(1);
				// Nouveau profile
				$profile = new Profile();
				$profile->uid = $uid;
				$profile->username = $me['first_name'].' '.$me['last_name'];
				$profile = $user->profiles()->save($profile);
			}

			$profile->access_token = $facebook->getAccessToken();
			$profile->save();

			$user = $profile->user;
			
			Auth::login($user);
			$activities = $user->activities;
			return Redirect::to('studio/dashboard')->with('activities',$activities);
		}

		// Formulaire d'inscription
		public function create(){

			return View::make('register');
		}	

		// Création d'un nouvel utilisateur
		public function store(){

		// Règles de validation
		$rules = array(
		    'username' => 'required|unique:users',
		    'email' => 'required|email|unique:users',
		    'password' => 'required|alphaNum|min:3'
		);

		$validator = Validator::make(Input::all(), $rules);

		// Vérification des règles de validation
			// En cas d'échec on affiche les message d'erreur concernant l'erreur
			if($validator->fails()){
				
				return Redirect::to('register')->withErrors($validator);
			}

			//En cas de réussite on sauvegarde l'utilisateur dans la base de données et on  redirige vers la page de confirmation d'inscription
			else{
					// On crée un nouvel utilisateur
					$user = new User;
					$user ->username = Input::get('username');
					$user ->email = Input::get('email');
					$user ->password = Hash::make(Input::get('password'));
					$user ->picture = '/assets/img/'.'defaultprofilepict.jpg';
					$user ->slug = Str::slug(Input::get('username'),'-');

					$user ->banned = 0;
					$user ->status = 1; // on active le compte de l'utilisateur

					$data = array( 'email' => Input::get('email'), 'username' => Input::get('username'));
						Mail::send('emails.welcome', $data, function($m)
						{
						    $m->to(Input::get('email'), Input::get('username'))->subject('Bienvenue!');
						});
					//$this->mailer->welcome->sendTo($user);
					// et on affiche la page de confirmation d'inscription
                    //$user->roles()->attach(Input::get('userRole'));

					$user ->save();
                    $user->roles()->attach(2);
					return View::make('register_success', $data)->with('flash_notice','Votre compte a bien été créé')->with('flash_type','flash-success');
			}	
		}


		// MODIFICATION DU PROFIL
		public function editProfile(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();

			$countries = Country::lists('countryNameFr','countryNameFr');
			$genres = Genre::lists('genreName','genreName');
			return View::make('user.edit')->with(array('countries'=> $countries, 'genres' => $genres));
		}

		// Déconnexion de l'utilisateur
		public function destroy(){
			if(Input::get('referer'))
			{
				$returnUrl = Input::get('referer');
				Auth::logout();
				return Redirect::to($returnUrl)->with('flash_message','Déconnexion réussie')->with('flash_type','flash-success');				
			}
			else{
				Auth::logout();
				return Redirect::home()->with('flash_message','Déconnexion réussie')->with('flash_type','flash-success');
			}

		}

		// Affichage du profil de l'utilisateur


        public function slugShow($slug){
		    $user = User::where('slug', $slug)->firstOrFail();
		    if(($user->banned) === '1')
		    {
		    	return View::make('bannedUser',array('user' => $user));
		    }
		    		$thepodcasts = $user->podcasts;
                    $podcasts = [];
                    foreach($thepodcasts as $thepodcast)
                    {
                        if($thepodcast->status == 1)
                        {
                            array_push($podcasts,$thepodcast);
                        }
                    }

                    if(Auth::user()){
                        $friends = Auth::user()->friends;
                    }
                    else{
                        $friends = [];
                    }

					$profileuserfriends = $user->friends;
					$photos = $user->photos;
					$videos = $user->videos;
					$events = $user->djevents;
					$genres = $user->genres;

                $friendids = [];
                    foreach($friends as $friend ){
                        array_push($friendids,$friend->id);
                    }

            //return $friendIds;
		   // return View::make('djprofile')->with(array('user' => $user, 'podcasts' => $podcasts, 'friends' => $friends, 'photos' => $photos, 'videos' => $videos, 'events' => $events, 'genres' => $genres));
		    return View::make('djprofile',compact('user', 'podcasts', 'friends', 'photos', 'videos', 'events', 'genres','friendids','profileuserfriends'));

		}

		// Mise à jour de l'utilisateur
		public function update(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			$genres = Genre::all();
			$user ->username = Input::get('username');
			$user ->country = Input::get('country');
			$thecountry = Country::where('countryNameFr','=',Input::get('country'))->first();
			$user ->country_id = $thecountry->id;
			$user ->city = Input::get('city');
			$user ->url = Input::get('website');
			$user ->biography = Input::get('biography');

			$user ->social_fb = Input::get('facebook_id');
			$user ->social_tw = Input::get('twitter_id');
			$user ->social_ig = Input::get('instagram_id');
			$user ->social_gp = Input::get('gplus_id');
			$user->slug = Str::slug(Input::get('username'),'-');
			// return Input::get('uGenre[]');
			//genres
			// if(Input::has('uGenre')){

			// 	$userGenres = explode(',',Input::get('uGenre'));
			// 	foreach ($userGenres as $userGenre) {
					
			// 	}
			// }
			$user->save();

			return Redirect::to('studio/edit_profil')->withInput()->with('flash_message','Votre profil a bien été modifié')->with('flash_type','flash-success');

		}
		
		public function all(){
            $djs = User::all();
            $genres = Genre::all();
            unset($genres[0]);
            $genreClasses = array("smalltag","bigtag","mediumtag");
			return View::make('user.index',compact('djs','genres','genreClasses'));
		}

		// Mise à jour de l'image de l'utilisateur
		public function updateUserPicture(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
				$file = Input::file('userpicture');
				
				// Validation de fichier
				$rules = array(
	            	'userpicture' => 'mimes:jpeg,png,jpg',
	        	);

				$input = Input::all();
		        $validation = Validator::make($input, $rules);

		        if($validation->passes()){

					$fileName = time().'-'.$file->getClientOriginalName();
					$fileExtension = $file->getClientOriginalExtension();
					$newfile =  str_replace('.' . $fileExtension, '', $fileName);
					$newfile = Str::slug($newfile,'-');
					$newfile = $newfile.'.'.$fileExtension;
					$destinationPath = public_path().'/assets/img/';

					Image::make($file->getRealPath())
					// original
					->resize(300, null, function ($constraint) {
					    $constraint->aspectRatio();
					})
					->crop(300,300)
					->save($destinationPath.$fileName);
					$user = Auth::user();
					$user->picture = '/assets/img/'.$fileName;
					$chemin =  '/assets/img/'.$fileName;
					$user->save();
					return $chemin;
                    //return Response::json('success', 200);
		        }
                else {
                    return Response::json('error', 400);
                }
		}

		// Liste des podcasts d'un utilisateur
		public function podcastsList($slug){
			$user = User::where('slug', $slug)->firstOrFail();
            $thepodcasts = $user->podcasts;
            $podcasts = [];
            foreach($thepodcasts as $thepodcast)
            {
                if($thepodcast->status == 1)
                {
                    array_push($podcasts,$thepodcast);
                }
            }

            if(Auth::user()){
                $friends = Auth::user()->friends;
            }
            else{
                $friends = [];
            }

            $profileuserfriends = $user->friends;
			$genres = $user->genres;
            $friendids = [];
            foreach($friends as $friend ){
                array_push($friendids,$friend->id);
            }
			return View::make('djpodcasts',compact('user', 'podcasts', 'friends', 'genres','friendids','profileuserfriends'));

		}


		// Liste des amis d'un utilisateur dans la section connecté
        public  function userfriends(){
            $user = Auth::user();
            $friends = Auth::user()->friends;
            $profileuserfriends = $user->friends;
            $genres = $user->genres;
            $friendids = [];
            foreach($friends as $friend ){
                array_push($friendids,$friend->id);
            }
            return View::make('user.friends',compact('user', 'podcasts', 'friends', 'genres','friendids','profileuserfriends'));
        }

		// Liste des amis d'un utilisateur
		public function friendsList($slug){
			$user = User::where('slug', $slug)->firstOrFail();
            if(Auth::user()){
                $friends = Auth::user()->friends;
            }
            else{
                $friends = [];
            }
            $profileuserfriends = $user->friends;
            $thepodcasts = $user->podcasts;
            $podcasts = [];
            foreach($thepodcasts as $thepodcast)
            {
                if($thepodcast->status == 1)
                {
                    array_push($podcasts,$thepodcast);
                }
            }
			$genres = $user->genres;
            $friendids = [];
            foreach($friends as $friend ){
                array_push($friendids,$friend->id);
            }
			return View::make('friends',compact('user', 'podcasts', 'friends', 'genres','friendids','profileuserfriends'));
		}

		public function login(){
		//return View::make('login');
			if(Auth::check())
			{
				return Redirect::to('studio/dashboard');
			}
			else{
				return View::make('login');
			}
		
		}

        // FRIENDSHIP

        public function addFriend($friendId){
            $user = Auth::user();
            $friends = $user->friends;
            // On vérifie si l'ami n'existe pas déjà dans sa friendlist
            $friendids = [];
            foreach($friends as $friend ){
                array_push($friendids,$friend->id);
            }
            $user->friends()->attach($friendId);
            // Notification à l'utilisateur que vous l'avez ajouté
        }
        public function removeFriend($friendId){
            $user = Auth::user();
            $friends = $user->friends;
            // On vérifie si l'ami n'existe pas déjà dans sa friendlist
            $user->friends()->detach($friendId);
            // Notification à l'utilisateur que vous l'avez supprimé
        }



        // DÉSACTIVATION DE COMPTE

        public function deactivateMe($id){
            $user = User::firstOrFail($id);
            return $user;
            // Désactivez le compte
            $user->status = 0;
            $user->save();
            // Déconnectez l'utilisateur

                Auth::logout();
                return Redirect::home()->with('flash_message','Votre compte a bien été désactivé')->with('flash_type','flash-success');
        }


        // SUPPRESSION DE COMPTE
        public function deleteMe($id){
            $user = User::firstOrFail($id);
            // Désactivez le compte
            $user->status = 0;
            $user->save();

            // Prévenir l'admin

            // Déconnectez l'utilisateur
                Auth::logout();
                return Redirect::home()->with('flash_message','Votre compte a bien été supprimé')->with('flash_type','flash-success');

        }

	}

	