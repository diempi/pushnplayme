<?php 

	/**
	* 
	*/
	class GenreController extends BaseController
	{

		public function index(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
            $genres = $user->genres;
			return View::make('genres.index')->with('genres', $genres);
		}

		public function create(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$allgenres = Genre::lists('genreName','id');
            $usergenres = Auth::user()->genres;
            $usergenresid = [];
            foreach($usergenres as $usergenre){
                array_push($usergenresid, $usergenre->id);
            }
            $genres = [];
            $genres = array_except($allgenres,$usergenresid);
			return View::make('genres.create',compact('genres'));
		}

        public function edit(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
            else{
                $genres = Auth::user()->genres;
                return Redirect::to('studio/genres')->with('data', $genres)->with('flash_message','Vous ne pouvez pas modifier les genres, merci de contacter l\'administrateur')->with('flash_type','flash-success');

            }
        }

		public function store(){
            if(Auth::guest())
            {
                return Redirect::to('login')->with('flash_message','Merci de vous connecter ')->with('flash_type','flash-success');
            }
			$user = Auth::user();
			
			// On récupère le genre
			$genre_Id = Input::get('genres');
			// ON lie le genre à l'utilisateur 
			$user->genres()->attach($genre_Id);
			
			$genre = Genre::find($genre_Id);
			//Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-plus adding"> Vous avez ajouté le genre '.$genre->genreNameFr.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s').' .</span>';
			$activity->save();	
			$genres = $user->genres;
			return Redirect::to('studio/genres')->with('data', $genres)->with('flash_message','Votre genre a bien été ajouté')->with('flash_type','flash-success');			
		}


		public function destroy($id){
			$user = Auth::user();
			$user->genres()->detach($id);
			$genre = Genre::find($genre_Id)->first();
			//Activité
			$activity = new Activity;
			$activity->userId = Auth::user()->id;
			$date = new DateTime;
			$activity->type = '<span class="icon-trash cancelling"> Vous avez supprimé le genre '.$genre->genreName.' le '.$date->format('d-m-Y').' à '.$date->format('H:i:s').'</span>';
			$activity->save();
			$genres = $user->genres;
			return View::make('genres.index')->with('genres', $genres);
		}

		public function allstyles(){
			$genres = Genre::all();
			//$genres = Genre::where('genreName','=','Choisir')->first();
			return View::make('genres.allstyles')->with('genres', $genres);
		}

		public function singlestyle($slug){
			$users = Genre::with('users')->where('genreSlug','=',$slug)->first();
			$genreUsers = $users->users;
		 	$genre = Genre::where('genreSlug','=',$slug)->first();
			return View::make('genres.singlestyle')->with(array('users'=> $genreUsers, 'genre' => $genre));
		}

	}