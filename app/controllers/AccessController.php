<?php  

	/**
	* 
	*/
	class AccessController extends BaseController
	{
		
		function __construct()
		{
			# code...
		}

		public function register(){
			return View::make('register');
		}

		public function inviteToConnect(){
			return View::make('register_success');
		}


		public function login(){
			if(Auth::check())
			{
				$user = Auth::user();
				$activities = $user->activities;
				return Redirect::to('studio/dashboard',array('user' => $user, 'activities' => $activities));
			}
			else{
				return View::make('login');
			}
			
		}

		public function connection(){
		// On valide les informations de connexion
						// Règles de validation
				$rules = array(
				    'email' => 'required|email',
				    'password' => 'required|alphaNum|min:3'
				);

				$validator = Validator::make(Input::all(), $rules);

				// Test de validité
				// En cas d'échec on renvoie vers la page de connexion avec le champ de login prérempli
				if($validator->fails()){
					
					return Redirect::to('login')->withErrors($validator->messages())->withInput(Input::except('password'));
				}
				// En cas de réussite on accède au tableau de bord
				else{
						if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password')))) {
					    	$remember = Input::get('remember');
					    	if(!empty($remember))
					    	{
					    		Auth::login(Auth::user(), true);
					    	 } 
					    	Session::put('user', Input::get('username'));
					    	$podcasts = Auth::user()->podcasts;
					    	// On sauvegarde la date de dernière connexion
					    	Auth::user()->last_connection = date("Y-m-d H:i:s");
					    	
					    	//$activities = Activity::where('userID','=',Auth::user()->id)->orderBy('updated_at','desc')->get();

                            $activities = Auth::user()->activities->sortByDesc('created_at');
					    	return View::make('user.dashboard',array('podcasts' => $podcasts, 'activities' => $activities))->with('flash_message','Vous vous êtes connecté avec succès')->with('flash_type','flash-success');
						}
						else{
							return Redirect::to('login')->withErrors($validator->messages());
						}
				}
		}

        // Accès réservé
        public function notAllowed(){
            return Redirect::home()->with('flash_message','Vous n\'avez pas les droits pour accéder à cette page')->with('flash_type','flash-alert');
        }
	}