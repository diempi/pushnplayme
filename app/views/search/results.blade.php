@extends('master.layout')
@section('newtitle')
    Résultats de la recherche
@stop
@include('home.header')
@include('home.footer')

@section('content')
<section class="high"></section>
<section id="searchresult" class="container">
    @if(count($results))
    <h2 class="title">Nous avons trouvé les DJS  suivants (<em>Cliquez sur les noms pour voir les profils</em>)</h2>
    <ul id="singlestyledj">
        @foreach($results as $result)
        
            @if($result->banned == 0)
                <li class="djgrid">
                    <a href="/djs/{{ $result->slug }}" title="{{ $result->username }}">
                        <section class="picture">
                            <img src="{{ $result->picture }}" alt="Voir la page de {{ $result->username }}">
                        </section>
                        <p class="name">{{ $result->username }}</p>
                    </a>
                </li>
            @else
            <!-- Cas d'un utilisateur banni -->
                <li class="djgrid"> 
                    <section class="picture">
                            <img src="/assets/img/userbanned.jpg" alt="Utilisateur banni">
                    </section>
                    <p class="banned">{{ $result->username }} a été banni.</p> 
                </li>
            @endif

        @endforeach
    </ul>
    @else
    <p class="nothing"> Désolé mais nous n'avons pas trouvé de résulats correspondant à votre recherche mais vous pouvez toujours par courir les <span><a href="/djs/all" title="Parcourir les DJS du site">DJS</a></span> et les <span><a href="/podcasts/all" title="Parcourir les podcasts du site">podcasts</a></span> du site</p>
    @endif

</section>
@stop