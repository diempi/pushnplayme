@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
            <section id="maincontent" class="container">

                <section id="djscontainer" class="featured">
                    <h2 class="bigtitle">
                        Les DJS du moment
                   <!--      <span class="prompt">
                            <a href="#" title="Jouer les mix de tous les djs">Jouer tous</a>
                        </span> -->
                    </h2>    
                    <section id="featureddjs">
                        <ul id="bigfeatured">
                            <li class="featuredj">
                                <a href="profile.html" title="Voir le profil de Nicky Romero">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/nickyromero.jpg" alt="Photo de Nicky Romero">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Nicky Romero</h3>
                                        <span class="djstyle"><a href="style.html">House</a>, <a href="style.html">Thech House</a></span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">                            
                                <a href="/djs/steve-angello" title="Voir le profil de Steve Angello">
                                    <span class="icon-headphones-1"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/1402642578-Steve_Angello-profile.jpg" alt="Photo de Steve Angello">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Steve Angello</h3>
                                        <span class="djstyle"><a href="/styles/edm">EDM</a>, <a href="/styles/house">House</a>, <a href="/styles/techno">Techno </a></span>
                                    </section>
                                </a>
                            </li>    

                            <li class="featuredj">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li>                               

                            <li class="featuredj">                            
                                <a href="/djs/didier-dmp" title="Voir le profil de Didier DMP'">
                                    <span class="icon-headphones-1"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/defaultprofilepict.jpg" alt="Photo de Didier DMP">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Didier DMP</h3>
                                        <span class="djstyle">
                                            <a href="/styles/rnb">RNB</a>, <a href="/styles/hip-hop-rap">Hip-Hop-Rap</a>
                                        </span>
                                    </section>
                                </a>
                            </li>    
                        </ul>  
                         <span class="clearfix"></span>
                    </section>
                </section>
                <span class="clearfix"></span>
                <section class="contentwithsidebar container">
                    <section id="djsearchbox">
                        <h2 class="bigtitle">Rechercher un DJ selon vos critères:</h2>
                        <form action="#" id="djsearch">
                            <section class="fields">
                                <label for="djname">Rechercher par nom:</label>
                                <input type="search" placeholder="Taper le nom du DJ" id="djname" name="djname">
                            </section>
                            <section class="fields">
                                <label for="musicstyle">Style musical:</label>
                                <select name="musicstyle" id="musicstyle">
                                    <option value="Ambient / Chillout">Ambient / Chillout</option>
                                    <option value="Bass">Bass</option>
                                    <option value="Classique">Classique</option>
                                    <option value="Club">Club</option>
                                    <option value="Dancehall">Dancehall</option>
                                    <option value="Deep House">Deep House</option>
                                    <option value="Drum And Bass">Drum And Bass</option>
                                    <option value="Dubstep">Dubstep</option>
                                    <option value="EDM">EDM</option>
                                    <option value="Electronique">Electronique</option>
                                    <option value="Funk / Soul">Funk / Soul</option>
                                    <option value="Hip Hop / Rap">Hip Hop / Rap</option>
                                    <option value="House">House</option>
                                    <option value="Indie">Indie</option>
                                    <option value="Jazz">Jazz</option>
                                    <option value="Pop">Pop</option>
                                    <option value="Reggae / Dub">Reggae / Dub</option>
                                    <option value="R&B">R&B</option>
                                    <option value="Rock">Rock</option>
                                    <option value="Sound Of London">Sound Of London</option>
                                    <option value="Tech House">Tech House</option>
                                    <option value="Techno">Techno</option>
                                    <option value="Trance">Trance</option>
                                    <option value="World">World</option>
                                </select>
                            </section>
                            <section class="fields">
                                <label for="djlocation">Lieu: </label>
                                <input type="text" id="djlocation" name="djlocation" placeholder="Entrer la ville du DJ">
                            </section>
                            <section class="fields">
                                <label for="sortcriteria">Trier par: </label>
                                <select name="sortcriteria" id="sortcriteria">
                                    <option value="Les plus récents">Les plus récents</option>
                                    <option value="Les mieux notés">Les mieux notés</option>
                                    <option value="Les plus joués">Les plus joués</option>
                                </select>
                            </section>
                            <section class="fields">
                                <input type="submit" value="Filtrer" id="filterbtn" class="pure-button pure-button-primary">
                            </section>
                        </form>
                    </section><!-- DJ Search box -->
                    <span class="clearfix"></span>
                    <section id="content" class="djlist">
                        <h2 class="bigtitle">
                            Les top DJS de <span class="currentcity"><a href="#" title="Liège">Liège</a></span>
                        </h2>
                        <p class="prompt">
                            <a href="#" class="allcharts" title="Voir tous les classements">Tous les DJS</a>
                            <span class="separate">  </span>
                            <a href="#" class="locationcheck" title="Changer de ville">Changer de ville</a>
                        </p>
                        <ul class="djlistitems">
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                            <li class="djitemlist">                            
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li> 
                        </ul><!-- DJS -->
                        <span class="clearfix"></span>
                        <ul class="pure-paginator">
                            <li><a class="pure-button prev" href="#">&#171;</a></li>
                            <li><a class="pure-button" href="#">1</a></li>
                            <li><a class="pure-button pure-button-active" href="#">2</a></li>
                            <li><a class="pure-button" href="#">3</a></li>
                            <li><a class="pure-button" href="#">4</a></li>
                            <li><a class="pure-button" href="#">5</a></li>
                            <li><a class="pure-button next" href="#">&#187;</a></li>
                        </ul>
                    </section><!-- content -->       

                    <section id="sidebar">
                        <section class="bloc">
                        <h2 class="bigtitle">Les derniers DJS Inscrits</h2>
                            <ol id="newdjs" class="pure-table">
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                </li>                              
                                <li class="pair">
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Steve Aoki"><img src="./img//steveaoki.jpg" alt="Steve Aoki"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>
                                    <section class="djstyle"><a href="style.html">House</a></section>
                                </li>                                
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil d'Afrojack"><img src="./img//afrojack.jpg" alt="Jacked"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil d'Afrojack">Afrojack</a></section>
                                    <section class="djstyle"><a href="style.html">Electro</a></section>
                                </li>                              
                                <li class="pair">
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Simina"><img src="./img//simina.jpg" alt="Simina"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Simina">Simina</a></section>
                                    <section class="djstyle"><a href="style.html">Funk</a></section>
                                </li> 
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Joyce"><img src="./img//joyce.jpg" alt="Joyce"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Joyce">Joyce</a></section>
                                    <section class="djstyle"><a href="style.html">Hip Hop</a></section>
                                </li>                            
                            </ol>                            
                        </section>
                        <section class="bloc">
                            <h2 class="bigtitle">Top 5 des DJS du site</h2>
                            <ol class="pure-table">
                                <li>
                                    <section class="chartnumber">1</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                </li>                              
                                <li class="pair">
                                    <section class="chartnumber">2</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Steve Aoki"><img src="./img//steveaoki.jpg" alt="Steve Aoki"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>
                                    <section class="djstyle"><a href="style.html">House</a></section>
                                </li>                                
                                <li>
                                    <section class="chartnumber">3</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil d'Afrojack"><img src="./img//afrojack.jpg" alt="Jacked"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil d'Afrojack">Afrojack</a></section>
                                    <section class="djstyle"><a href="style.html">Electro</a></section>
                                </li>                              
                                <li class="pair">
                                    <section class="chartnumber">4</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Simina"><img src="./img//simina.jpg" alt="Simina"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Simina">Simina</a></section>
                                    <section class="djstyle"><a href="style.html">Funk</a></section>
                                </li> 
                                <li>
                                    <section class="chartnumber">5</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Joyce"><img src="./img//joyce.jpg" alt="Joyce"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Joyce">Joyce</a></section>
                                    <section class="djstyle"><a href="style.html">Hip Hop</a></section>
                                </li>                            
                            </ol>
                        </section>
                        <section class="bloc">
                            <p class="recrute">Vous êtes DJ ou rêvez de le devenir? <a href="signup.html" title="Inscrivez-vous maintenant!!">Inscrivez-vous gratuitement</a> et faites découvrir votre talent! Et tout ça <a href="signup.html" title="Inscrivez-vous maintenant!!">gratuitement!</a></p>
                        </section>

                        <section class="bloc">
                            <h2 class="bigtitle">Nuages de Styles</h2>
                            <ul id="tags">
                                @foreach ($genres as $genre)
                                <li><a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}" class="{{ $genreClasses[array_rand($genreClasses,1)] }}">{{ $genre->genreName }}</a></li>
                                @endforeach
                            </ul>
                        </section>

                    </section><!-- sidebar -->

                </section><!-- Content with sidebar -->

                <span class="clearfix"></span>
            </section>
            <span class="clearfix"></span>
            <section class="push"></section>
@stop