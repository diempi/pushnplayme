<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>PushNPlay.me, vos podcasts à portée de vos fans! Accueil </title>
        <meta name="description" content="PushNPlay.me, platefore d'écoute et de gestion de podcasts">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="css/normalize.css" type="text/css" />
        <link rel="stylesheet/less" href="less/main.less" type="text/css" media="screen" />
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

            <!-- HEADER -->
            <header>
                <div id="top" class="wrapper">
                <h1><a href="index.html" title="PushNplay.me">PushNPlay.me</a></h1>
                    <nav title="Menu de navigation" role="navigation">
                        <ul id="main-menu" role="navigation">
                            <li><a href="djs.html" title="La liste des DJS">DJS</a></li>
                            <li><a href="podcasts.html" title="La liste des podcasts">Podcasts</a></li>
                        </ul>
                        <ul id="login" role="navigation">
                            <li><a itemprop="url" id="signform" href="#" title="Se connecter">Se connecter</a>
                                    <div class="box">
                                        <!-- Login par formulaire ordinaire -->
                                        <form action="auth/login" method="post" accept-charset="utf-8">
                                            <fieldset>
                                                <label for="username">Login</label><input type="text" name="Login" value="" id="username" placeholder="Entrez votre login" autofocus required >
                                            </fieldset>
                                            <fieldset>
                                                <label for="password">Mot de passe</label><input type="password" name="pass" placeholder="Mot de passe" id="pass" required>
                                            </fieldset>
                                            <fieldset class="keeplogin">
                                                <input type="checkbox" name="loginkeeping" id="loginkeeping" value="loginkeeping" /> 
                                                <label for="loginkeeping">Rester connecté</label>
                                                <a href="passremember.html" title="Retrouvez votre mot de passe">Mot de passe oublié?</a>
                                            </fieldset>
                                            <fieldset class="sub-button">
                                                <input type="submit" name="connect" value="Se connecter">
                                            </fieldset>
                                        </form>                          
                                                <hr>
                                            <!-- Login par Réseaux sociaux -->
                                            <a id="tw_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-twitter"></span>Se connecter avec Twitter</a>
                                            <a id="fb_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-facebook"></span>Se connecter avec  Facebook</a>
                                            <a id="ggl_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-gplus"></span>Se connecter avec  Google</a>
                                    </div>
                            </li>
                        </ul>
                    </nav>                    
                </div>

            </header>
            <div class="wrapper fullpage content-box" role="main">
                <!-- INSCRIPTION VIA R&seaux Sociaux -->
                <div id="sociallogin">
                    <!-- Login par Réseaux sociaux -->
                    <h2>Inscription via les réseaux sociaux</h2>
                        <div>
                            <a id="tw_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-twitter"></span>S'inscrire  avec Twitter</a>
                        </div>
                        <div>
                            <a id="fb_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-facebook"></span>S'inscrire  avec  Facebook</a>
                        </div>
                        <div>
                            <a id="ggl_login_link" class="social" href="#" rel="nofollow"><span class="social-btn icon-gplus"></span>S'inscrire  avec  Google</a>                            
                        </div>

                </div>
                <!-- FORMULAIRE D'INSCRIPTION -->
                <div id="tasign">
                    <?php echo ($this->load->view('$signupform')); ?>
                </div>
                <div id="insform" class="signupform container">
                    <h2>Inscription par email</h2>
                    <form action="fin_inscription.html" method="post" accept-charset="utf-8">
                        <fieldset>
                            <label for="email">Email</label><input type="email" name="EMail" value="" id="email" placeholder="Entrez votre email" autofocus required >
                        </fieldset>
                        <fieldset>
                            <label for="password">Mot de passe</label><input type="password" name="pass" placeholder="Mot de passe" id="pass" required>
                        </fieldset>

                        <fieldset>
                            <label for="password">Confirmer le mot de passe</label><input type="password" name="pass" placeholder="Mot de passe" id="pass" required>
                        </fieldset>

                        <fieldset class="sub-button">
                            <input type="submit" name="join" value="S'inscrire">
                        </fieldset>
                    </form>                     
                </div>
                
            </div>

        
        <!-- FOOTER  -->
            <footer role="contentinfo">
                <div id="footerinfos">
                    <h3>Retrouvez-nous sur:</h3>
                    <ul id="social-btn">
                        <li itemscope>
                            <a itemprop="url" href="#" id="rss">Flux RSS</a>
                        </li>
                        <li itemscope>
                            <a itemprop="url" href="#" id="twitter">Compte Twitter</a>
                        </li>
                        <li itemscope>
                            <a itemprop="url" href="#" id="fb">Compte Facebook</a>
                        </li>
                    </ul>    
                    <ul id="footer-menu">
                        <li><a href="apropos.html" title="à propos">à propos</a></li>
                        <li><a href="cgu.html" title="Conditions générales d'utilisation">Conditions générales d'utilisation</a></li>
                        <li><a href="faq.html" title="F.A.Q">F.A.Q</a></li>
                        <li><a href="contact.html" title="Contactez-nous">Contactez-nous</a></li>
                    </ul>
                    <p>2012 Copyrights</p>                   
                </div>
            </footer>

        <!-- SCRIPTS --> 
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="js/less-1.3.3.min.js" type="text/javascript"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
