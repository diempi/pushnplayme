                    <section class="userdash">
                            <ul class="userlinks">
                                    <section class="userpict">
                                        <img src="{{ Auth::user()->picture }}" alt="Photo de {{ Auth::user()->username }}">
                                    </section> 
                                <h3 class="usernameclass">
                                    {{ Auth::user()->username }}
                                </h3>

                                <li>
                                    {{ link_to('studio/dashboard',' Tableau de bord',array('class' => 'icon-home')) }}
                                </li>
                                <li>{{ link_to('studio/podcasts/create',' Ajouter un podcast',array('class' => 'icon-upload')) }}
                                </li>                                 
                                <li>{{ link_to('studio/podcasts',' Vos podcasts',array('class' => 'btn-follow icon-music')) }}
                                </li>                                  
                                <li>{{ link_to('studio/genres',' Vos styles',array('class' => 'btn-follow icon-music')) }}
                                </li>  
                                <li>{{ link_to('studio/edit_profil',' Gestion du profil',array('class' => 'icon-user')) }}</li>
                                <li>{{ link_to('studio/photos',' Vos photos',array('class' => 'btn-follow icon-picture')) }}
                                </li>                                
                                <li>{{ link_to('studio/videos',' Vos vidéos',array('class' => 'btn-follow icon-video-chat')) }}
                                </li>                                
                                <li>{{ link_to('studio/events',' Vos évènements ',array('class' => 'btn-follow icon-calendar')) }}
                                </li>
                                <li>{{ link_to('studio/friends',' Vos amis ',array('class' => 'btn-follow icon-users')) }}
                                </li>
                            </ul>
                        </section>