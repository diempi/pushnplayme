@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- TITLE -->
                            <h3 class="title">Succès d'ajout</h3>
                            @include('master/partials/_flash_message')
                            <section class="sectioncontent">
                                <section id="uploadsuccess" class="content-box">
                                    <section class="success">
                                        <p>Votre mix a été ajouté avec succès!</p>
                                    </section>
                                </section>
                            </section>
                    </section>

                </section>
        </section>
@stop