@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
<section class="wrapper" role="main">
    <section id="dashboard" class="content-box">

        <!-- Left Sidebar -->
        <section class="leftcontent">
            @include('user.left-menu')
        </section>

        <!-- Middle Content -->
        <section class="rightcontent">
            <!-- Gérer vos amis -->
            <h3 class="title">Vos amis</h3>

            <section class="sectioncontent">
                @include('master/partials/_flash_message')

                @if(count($friends))
                <ul id="friends">
                    @foreach( $friends as $friend )
                    <li id="{{ $friend->id }}" class="friend">
                        <a href="/djs/{{ $friend->slug }}" title="{{ $friend->username }}">
                            <img src="{{ $friend->picture }}" alt="{{ $friend->username }}" title="{{ $friend->username }}">
                        </a>
                        <p class="options">

                            {{ link_to('#',' Retirer de vos amis',array('class' => 'removefriend right', 'title' => 'Retirer de vos amis')) }}
                        </p>

                    </li>
                    @endforeach
                </ul>
                @else
                <p class="nothing"> Vous n'avez pas encore d'amis.</p>
                @endif
            </section>

        </section>

    </section>
    <section  class="wrapper">
    </section>
</section>
@stop