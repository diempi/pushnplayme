@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter un nouvel évènement</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/newevent','id'=> 'neweventadd')) }}
                                <section class="bloc">
                                    {{ Form::label('eventDate','Date de l\'évènement') }}
                                    {{ Form::text('eventDate',null,array('id' => 'eventdateid','placeholder' => 'Cliquez ici ou sur le calendrier pour choisir une date')) }}
                                </section>                                
                                <section class="bloc">
                                    {{ Form::label('eventTitle','Nom de l\'évènement') }}
                                    {{ Form::text('eventTitle') }}
                                </section>                                
                                <section class="bloc">
                                    {{ Form::label('eventPlace','Lieu de l\'évènement') }}
                                    {{ Form::text('eventPlace') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::label('eventTicket','Lien pour achetez un ticket') }}
                                    {{ Form::text('eventTicket') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Envoyez votre fichier', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop