@extends('master.dashboardmaster')



@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">
                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- RECENT ACTIVITY -->
                            <h3 class="title">Activité récente</h3>
                            <section class="formcontainer">
                                @include('master/partials/_flash_message')
                                @if(count($activities))
                                <ul id="activityboard">
                                    @foreach( $activities as $activity) 
                                        <li class="activityclass">{{ $activity->type }}</li>
                                    @endforeach
                                </ul> 
                                @else
                                    <p class="nothing">Vous n'avez pas encore d'activité</p>
                                @endif                                
                            </section>                 
                    </section>

                </section>
        </section>
@stop