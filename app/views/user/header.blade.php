@section('header')
                <div id="top" class="wrapper">
                <h1><a href="/" title="PushNplay.me"><img src="/assets/img/logopnpwhite-small.png" alt="Logo PNP"></a></h1>
                    <nav title="Menu de navigation" role="navigation">
                        <ul id="main-menu" role="navigation">
                            <li>{{ HTML::link('djs/all', 'DJS',  array('title' => 'Accéder aux DJS')) }}</li>
                            <li>{{ HTML::link('podcasts/all', 'Podcasts', array('title' => 'Accéder aux podcasts')) }}</li>
                        </ul>
                        <ul id="usermenu" role="navigation">
                            <button class="preview-button"><a href="/djs/{{ Auth::user()->slug }}" title="profil de {{ Auth::user()->slug }}">Voir votre profil</a></button>
                            <li><a href="{{ URL::to('logout') }}" title="Se déconnecter" class="icon-logout button-error">Se déconnecter </a></li>
                        </ul>
                    </nav>                    
                </div>
@stop