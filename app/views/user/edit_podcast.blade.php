@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <section id="editpod" class="content-box container">
                            <h3 class="title">Modification de  votre podcast</h3>

                            {{ Form::open(array('url' => 'studio/podcasts/'.$podcasts->id.'/edit', 'id' => 'podcastform', 'files' => true, 'enctype' => 'multipart/form-data', 'method' => 'put')) }}
                                            {{ $errors->first('title', '<p class="error">:message</p>') }}
                                            <section class='bloc'>
                                                {{ Form::label('title', 'Nom du mix', array('class' => 'bloctitle' )) }}
                                                
                                                {{ Form::text('title', $podcasts->title) }}
                                            </section>
                                            {{ $errors->first('description', '<p class="error">:message</p>') }}
                                            <section class='bloc'>
                                                {{ Form::label('description', 'Description du mix', array('class' => 'bloctitle' )) }}
                                                {{ Form::textarea('description', $podcasts->description) }}
                                            </section>
                                            <section class='bloc'>
                                                {{ Form::submit('Sauvegader les modifications', array('class' => 'submissionbtnok')) }}
                                                {{ Form::reset('Annuler',array('class' => 'cancellingbtn')) }}                                
                                            </section>

                            {{ Form::close() }}

                        </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop