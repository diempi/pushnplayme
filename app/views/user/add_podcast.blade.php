@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter un nouveau podcast</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/podcasts','files' => true,'id'=> 'uploadform')) }}
                                <section class="bloc">
                                    {{ Form::label('podTitle','Entrez le titre') }}
                                    {{ Form::text('podTitle') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::label('podDescription','Décrivez votre mix') }}
                                    {{ Form::textarea('podDescription') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::label('podcastFile','Sélectionner le fichier à envoyer: ') }}
                                    {{ Form::file('podcastFile') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Envoyez votre fichier', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop