@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter une nouvelle photo</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/add_photo','files' => true ,'id'=> 'uploadnewphotoform')) }}
                                <section class="bloc">
                                    {{ Form::label('photoTitle','Entrez le titre de la photo') }}
                                    {{ Form::text('photoTitle') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::label('newPhoto','Sélectionner la photo à envoyer: ') }}
                                    {{ Form::file('newPhoto') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Sauvegarder votre photo', array('class' => 'submissionbtnok')) }}
                                </section>
                            {{ Form::close() }}
                        </section>    
                        <section class="newaddedphoto">
                            <img src="#" alt="#" title="#">
                        </section>             
                    </section>

                </section>
        </section>
@stop