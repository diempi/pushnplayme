@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Succès -->
                        <section class="success">
                            <p>Votre mix a été envoyé avec succès!</p>
                        </section>
                        <section id="uploadsuccess" class="content-box container">
                            {{ Form::open(array('url' => 'saveFile','files' => true, 'enctype' => 'multipart/form-data')) }}
                                {{ Form::label('podTitle','Titre' ) }}
                                {{ Form::text('podTitle',Input::old('title')) }}
                                {{ Form::label('podDescription','Description') }}
                                {{ Form::textarea('podDescription',Input::old('description')) }}
                                {{ Form::submit('Sauvegarder votre fichier') }}
                            {{ Form::close() }}
                        </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop