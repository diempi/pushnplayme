@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter une nouvelle vidéo</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/videos','id'=> 'addvideolink','files' => true)) }}
                                <section class="bloc">
                                    {{ Form::label('videoTitle','Entrez le titre') }}
                                    {{ Form::text('videoTitle',null, array('placeholder' => 'Titre de la vidéo')) }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::label('videoLink','Choisir le type de video à envoyer ') }}
                                    {{ Form::select('videotype',array('Vim' => 'Vimeo', 'Yout' => 'Youtube', 'Daylim' =>'Daylimotion'),'Yout') }}
                                    {{ Form::text('videoLink',null, array('placeholder' => 'Entrez le lien de la vidéo')) }}
                                </section>
                                <section class="bloc">
                                    {{ Form::label('videoThumbnail','Envoyer une miniature de la vidéo(Optionnel)') }}
                                    {{ Form::file('videoThumbnail')}}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Ajouter la vidéo', array('class' => 'submissionbtnok')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop