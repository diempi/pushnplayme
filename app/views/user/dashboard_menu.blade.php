    <ul class="userlinks">
        <li><a href="dashboard" title="Retour au tableau de bord"><span class="icon-home"></span> Tableau de bord</a></li>
		<li><a href="modif_profil" title="Gestion du profil"><span class="icon-user"></span> Gestion son profil</a></li>
        <li><a href="manage_podcasts" title="Gestion des podcasts"><span class="icon-signal"></span> Gérer ses podcasts</a></li>
        <li><a href="comments" title="Gestion des commentaires"><span class="icon-chat"></span> Gérer les commentaires</a></li>
        <li><a href="podcast_stats" title="Voir les stats des podcasts"><span class="icon-chart-bar-1"></span> Statistiques du podcast</a></li>
       	<li><a href="add_podcast" title="Ajouter un nouveau podcast" class="btn-follow">Ajouter un podcast <span class="icon-upload"></span></a></li>       	
       	<li><a href="add_photo" title="Ajouter une nouvelle photo" class="btn-follow">Ajouter une photo <span class="icon-picture"></span></a></li>       	
       	<li><a href="add_video" title="Ajouter un nouvelle vidéo" class="btn-follow">Ajouter une vidéo <span class="icon-video-chat"></span></a></li>
    </ul>