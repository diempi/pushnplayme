@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
        <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                            <h3 class="title">Gérez vos commentaires</h3>
                            <section class="sectioncontent">
                                @if(count($comments))
                                    <table id="stats">
                                        <caption><h1>Vos commentaires</h1></caption>
                                        <tr>
                                            <th>Titre du podcast</th>
                                            <th>Date d'envoi</th>
                                            <th>Statut</th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                         @foreach( $comments as $comment )
                                        <tr id="{{ $comment->id }}" class="comment">
                                            <td class="comment-title"> {{ $comment->title }} </td>
                                            <td class="comment-date"> {{ $comment->created_at }} </td>
                                            <td class="comment-status"> 
                                                @if($podcast->status === '0')
                                                    Approuvé
                                                @else
                                                    Á approuver
                                                @endif
                                            </td>
                                            <td><a href="podcast/{{ $comment->id }}/edit" title="Approuver ce commentaire" class="podcastediting">Approuver</a> </td>
                                            <td><a href="#" title="Supprimer ce commentaire" class="podcastdeletion" >Supprimer</a></td> 
                                        </tr>
                                         @endforeach 
                                    </table>
                                @else
                                    <p class="nothing"> Vous n'avez pas de commentaires</p>
                                @endif 
                            </section>
                    </section>

                </section>
        </section>
@stop