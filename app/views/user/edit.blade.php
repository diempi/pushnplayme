@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- EDIT PROFILE -->
                            
                           <h3 class="title">Modification de  votre profil</h3>
                           @include('master/partials/_flash_message')
                            <section class="formcontainer">
                                <section class="bloc">
                                    <button class="preview-button"><a href="/djs/{{ Auth::user()->slug }}" title="profil de {{ Auth::user()->slug }}" target="_new">Voir votre profil</a></button>
                                </section>
                              {{ Form::open(array('url' => 'djs/update/profilepict','id' => 'profilepictform', 'files' => true, 'enctype' => 'multipart/form-data')) }}
                                    <!-- PICTURE -->
                                    {{ $errors->first('userpicture', '<p class="error">:message</p>') }}
                                    <section class="bloc">
                                        {{ Form::label('userpicture', 'Photo de profil') }}
                                        {{ Form::file('userpicture') }}
                                    </section>
                                    <section class="bloc">
                                        {{ Form::submit('Envoyez une image', array('id' => 'submitprofilepict')) }}
                                    </section>
                                {{ Form::close() }}
                                    <section class="userprofilepict">
                                        <img src="<?php echo Auth::user()->picture; ?>" alt="Photo de <?php echo Auth::user()->username; ?>"> 
                                        <section class="progress">
                                            <section class="bar"></section>
                                            <section class="percent">0%</section>
                                        </section>
                                    </section>
                            </section>

                            <section class="formcontainer">
                                {{ Form::open(array('url' => 'djs/'.Auth::user()->id, 'id' => 'profileform', 'method' => 'put', 'class' => 'pure-form-aligned')) }}
                               <!-- NAME -->
                               {{ $errors->first('username', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('username', 'Nom', array('class' => 'bloctitle' )) }}
                                    <?php 
                                        if(Auth::user()->username == null){
                                     ?>
                                            {{ Form::text('username', null, array('class' => 'field', 'id' => 'username', 'placeholder' => 'Hey t\'as oublié de mettre ton nom d\'artiste')) }}
                                    <?php }
                                        else{
                                    ?>
                                             {{ Form::text('username', Auth::user()->username, array('class' => 'field', 'id' => 'username' )) }}
                                       <?php } ?>
                                </section>  
                                
                                <!-- COUNTRY -->
                                {{ $errors->first('country', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    
                                    {{ Form::label('country', 'Pays', array('class' => 'bloctitle' )) }}
                                    <?php 
                                        if(Auth::user()->country == null){
                                     ?>
                                        {{ Form::select('country', $countries,$countries,array('class' => 'form-control'))}}
                                        
                                    <?php }
                                        else{
                                    ?> 
                                      
                                        {{ Form::select('country', $countries,Auth::user()->country)}}
                                    <?php } ?>  
                                </section>

                                <!-- CITY -->
                                {{ $errors->first('city', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                
                                    {{ Form::label('city', 'Ville', array('class' => 'bloctitle' )) }}
                                    <?php 
                                        if(Auth::user()->city == null){
                                     ?>
                                            {{ Form::text('city', null, array('class' => 'field' , 'id' => 'city', 'placeholder' => 'Tu viens de quelle ville?')) }}
                                    <?php }
                                        else{
                                    ?>            
                                             {{ Form::text('city', Auth::user()->city, array('class' => 'field' , 'id' => 'city', 'placeholder' => Auth::user()->city)) }}
                                    <?php } ?>          
                                </section>

                                <!-- WEBSITE -->
                                {{ $errors->first('website', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                
                                    {{ Form::label('website', 'Site Web', array('class' => 'bloctitle' )) }}
                                    <?php 
                                        if(Auth::user()->url == null){
                                     ?>
                                        {{ Form::text('website', null, array('class' => 'field' , 'id' => 'website', 'placeholder' => 'Entrez votre site')) }}
                                    <?php }
                                        else{
                                    ?>    
                                         {{ Form::text('website', Auth::user()->url, array('class' => 'field' , 'id' => 'website')) }}
                                    <?php } ?>  
                                    
                                </section>
                         
                                <!-- BIOGRAHPY -->
                                {{ $errors->first('biography', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                
                                    {{ Form::label('biography', null, array('class' => 'bloctitle bioclass' )) }}
                                    <?php 
                                        if(Auth::user()->biography == null){
                                     ?>
                                        {{ Form::textarea('biography', null, array('class' => 'field texta' , 'id' => 'biography', 'placeholder' => 'Entrez votre biographie')) }}
                                    <?php }
                                        else{
                                    ?>
                                       {{ Form::textarea('biography', Auth::user()->biography, array('class' => 'field texta' , 'id' => 'biography')) }}
                                    <?php } ?>  
                                </section>
                                <h2>Réseaux sociaux</h2>
                                {{ $errors->first('facebook_id', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('facebook_id','http://www.facebook.com/',array('class' => 'bloctitle')) }}
                                    <?php 
                                        if(Auth::user()->social_fb == null){
                                     ?>
                                        {{ Form::text('facebook_id',null, array('class' => 'field' , 'id' => 'facebook_id', 'placeholder' => 'Entrez votre compte')) }}
                                    <?php }
                                        else{
                                    ?> 
                                         {{ Form::text('facebook_id', Auth::user()->social_fb, array('class' => 'field' , 'id' => 'facebook_id')) }}
                                    <?php } ?> 
                                </section>
                                <!-- FACEBOOK -->

                                {{ $errors->first('twitter_id', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('twitter_id','http://www.twitter.com/',array('class' => 'bloctitle')) }}
                                    <?php 
                                        if(Auth::user()->social_tw == null){
                                     ?>
                                        {{ Form::text('twitter_id',null, array('class' => 'field' , 'id' => 'twitter_id', 'placeholder' => '@')) }}
                                    <?php }
                                        else{
                                    ?> 
                                         {{ Form::text('twitter_id', Auth::user()->social_tw, array('class' => 'field' , 'id' => 'twitter_id')) }}
                                    <?php } ?> 
                                </section>
                                <!-- TWITTER -->  

                                {{ $errors->first('instagram_id', '<p class="error">:message</p>') }}               
                                <section class="bloc">
                                    {{ Form::label('instagram_id','http://www.instagram.com/',array('class' => 'bloctitle')) }}
                                    <?php 
                                        if(Auth::user()->social_ig == null){
                                     ?>
                                        {{ Form::text('instagram_id',null, array('class' => 'field' , 'id' => 'instagram_id', 'placeholder' => 'Entrez votre compte instagram')) }}
                                    <?php }
                                        else{
                                    ?> 
                                         {{ Form::text('instagram_id', Auth::user()->social_ig, array('class' => 'field' , 'id' => 'instagram_id')) }}
                                    <?php } ?> 
                                </section>
                                <!-- INSTAGRAM -->     

                                {{ $errors->first('gplus_id', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('gplus_id','http://plus.google.com.com/',array('class' => 'bloctitle')) }}
                                    <?php 
                                        if(Auth::user()->social_gp == null){
                                     ?>
                                        {{ Form::text('gplus_id',null, array('class' => 'field' , 'id' => 'gplus_id', 'placeholder' => 'Entrez votre compte google')) }}
                                    <?php }
                                        else{
                                    ?> 
                                         {{ Form::text('gplus_id', Auth::user()->social_gp, array('class' => 'field' , 'id' => 'gplus_id')) }}
                                    <?php } ?> 
                                </section>
                                <!-- GOOGLE PLUS -->

                                <section id="profilesavebutton">
                                    {{ Form::submit('Enregistrer les modifications') }}
                                </section>

                              {{ Form::close() }}
                            </section>
                        <section class="bloc deldes">
                            {{ link_to('#',' Désactivez votre compte',array('class' => Auth::user()->id, 'title' => 'Désactivez votre compte', 'id' => 'disableaccount')) }}

                            <a href="/djs/{{ Auth::user()->id }}/delete" title="Supprimez votre compte" id="deleteaccount" class="{{ Auth::user()->id }}">Supprimez votre compte</a>
                        </section>
                        </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop