@extends('master.djslayout')

@section('newtitle')
    {{ $user->username }}
@stop

@include('home.header')
@include('home.footer')

@section('content')
                <section class="djprofilecontainer">
                    <section class="breadcrum">
                        <a href="/" title="Accueil"> Accueil</a>
                        <span> > </span>
                        <a href="/djs/all" title="La page des DJS">DJS</a>
                        <span> > </span>
                        <a href="./" title="Les podcasts de {{ $user->username }}">{{ $user->username }}</a>
                        <span> > </span>
                        A
                    </section>
                    <section class="profileheader">
                        <section class="profilepict">
                            <a href="./" title="Photo du DJ {{ $user->username }}" itemprop="url" ><img src="{{ $user->picture }}" alt="Photo du DJ {{ $user->username }}" title="Photo du DJ {{ $user->username }}" itemprop="image"></a>
                        </section>

                        <section class="profileheaderinfo">
                            <h1 itemprop="name">{{ $user->username }}</h1>
                                <p class="location" itemprop="nationality">  
                                    <?php 
                                        if((($user->country) !== '') AND (($user->country) !== null)) {
                                    ?>                             
                                        <a href="/countries/{{ Str::slug($user->country) }}" title="Voir tous les podcasts de {{ $user->country }}"> <span class="icon-location"></span> {{ $user->country }}</a>
                                    <?php 
                                        }
                                     ?>
                                </p>
                                <p class="style">
                                    @if(count($genres))
                                    {{-- */$i = 0;/* --}}
                                    @foreach( $genres as $genre )
                                    <a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a>
                                    {{-- */$i++/* --}}
                                    @if($i < count($genres) )|
                                    @endif
                                    @endforeach
                                    @else
                            <p class="nothing"> {{ $user->username }} n'a pas encore ajouté de genre</p>
                            @endif
                                </p>
                            <?php 
                                if(($user->url) != 'http://') {
                             ?>
                            <?php 
                                if((($user->url) != 'http://') AND (($user->url) != null)) {
                             ?>
                                <p class="website" itemprop="url"><a href="{{ $user->url }}" title="Visitez le site du DJ {{ $user->username }}"><span class="icon-globe-1"> </span> {{ $user->url }}</a></p>
                            <?php 
                                }
                             ?>
                            <?php 
                                }
                             ?>
                            <p class="socialnetworks">
                            <?php 
                                if(($user->social_fb) != null) {
                             ?>
                                <a href="http://www.facebook.com/{{ $user->social_fb }}" title="Visitez la page Facebook du DJ {{ $user->username }}"><span class="icon-facebook"></span>{{ $user->social_fb }}</a> |
                            <?php 
                                }
                             ?>    <!-- FACEBOOK -->                         

                             <?php 
                                if(($user->social_tw) != null) {
                             ?>
                                <a href="http://www.twitter.com/{{ $user->social_tw }}" title="Visitez la page Twitter du DJ {{ $user->username }}"><span class="icon-twitter"></span>{{ $user->social_tw }}</a> |
                            <?php 
                                }
                             ?> <!-- TWITTER -->   
                             
                             <?php 
                                if(($user->social_ig) != null) {
                             ?>
                                <a href="http://www.instagram.com/{{ $user->social_tw }}" title="Visitez la page Instagram du DJ {{ $user->username }}"><span class="icon-instagramm"></span>{{ $user->social_ig }}</a> 
                            <?php 
                                }
                             ?> <!-- INSTAGRAM -->   

                            </p>
                            <p class="stats">
                                <span class="followers"><span class="icon-users"></span> <span class="friendsNb" data-id="{{ count($profileuserfriends) }}">{{ count($profileuserfriends) }}</span> @if( count($profileuserfriends) == 1 || count($profileuserfriends) == 0)  ami  @else amis @endif </span> |
                                <span class="podcasts"><a href="/djs/{{ $user->slug }}/podcasts" title="Voir tous les podcasts de {{ $user->username }}"><span class="icon-rss"> {{ count($podcasts) }} Mix </span></a></span>
                            </p>

                            @if(Auth::guest())
                            <p class="addbtncontainer">
                                <section class="pure-button-primary pure-button">{{ HTML::link('login', 'Connectez-vous pour l\' ajouter', array('title' => 'Connectez-vous')) }}</section>
                            </p>
                            @elseif($user->id != Auth::user()->id)
                            @if(count($friendids) === 0 )
                            <p class="addbtncontainer">
                                <section class="button-success pure-button follow" data-id="{{ $user->id }}">Devenir ami</section>
                            </p>
                            @elseif(count($friendids) > 0)
                            @if(in_array($user->id,$friendids))
                            <p class="addbtncontainer">
                                <section class="button-error pure-button unfollow" data-id="{{ $user->id }}">Ne plus être ami</section>
                            </p>
                            @else
                            <p class="addbtncontainer">
                                <section class="button-success pure-button follow" data-id="{{ $user->id }}">Devenir ami</section>
                            </p>
                            @endif
                            @endif
                            @else

                            @endif
                        </section>
                    </section><!-- profileheader -->
                    <span class="clearfix"></span>
                    <section class="profilecontent">
                        <section id="fullpodcasts" class="podcasts">
                            <h2>Les amis de {{ $user->username }}</h2>
                            <ul class="fans">
                                @if(count($profileuserfriends))
                                    @foreach( $profileuserfriends as $friend )
                                <li>
                                    <a href="/djs/{{ $friend->slug }}" title="profil du fan">
                                        <h3 class="fanname">{{ $friend->username }}</h3>
                                        <section class="fanimage">
                                            <img src="{{ $friend->picture }}" alt="Fan">
                                        </section>

                                        <p class="style">
                                            @if(count($friend->genres))
                                            {{-- */$i = 0;/* --}}
                                            @foreach( $friend->genres as $genre )
                                            <a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a>
                                            {{-- */$i++/* --}}
                                            @if($i < count($genres) )|
                                            @endif
                                            @endforeach
                                            @else
                                        <p class="nothing"> {{ $user->username }} n'a pas encore ajouté de genre</p>
                                        @endif
                                        </p>
                                            @if((($friend->country) !== '') AND (($friend->country) !== null))
                                                <p class="countrycontainer"><a href="/countries/{{ Str::slug($friend->country) }}" title="Voir tous les podcasts de {{ $friend->country }}" itemprop="nationality"> <span class="icon-location"></span> {{ $friend->country }}</a></p>
                                            @else
                                                <span class="icon-location"></span>  </a>
                                            @endif


                                    @if(Auth::guest())
                                        <p class="addbtncontainer">
                                            <section class="pure-button-primary pure-button">{{ HTML::link('login', 'Connectez-vous pour l\' ajouter', array('title' => 'Connectez-vous')) }}</section>
                                        </p>
                                    @elseif($user->id != Auth::user()->id)
                                        @if(count($friendids) === 0 )
                                            <p class="addbtncontainer">
                                                <section class="button-success pure-button follow" data-id="{{ $friend->id }}">Devenir ami</section>
                                            </p>
                                        @elseif(count($friendids) > 0)
                                            @if(in_array($user->id,$friendids))
                                                <p class="addbtncontainer">
                                                    <section class="button-error pure-button unfollow" data-id="{{ $friend->id }}">Ne plus être ami</section>
                                                </p>
                                            @else
                                                <p class="addbtncontainer">
                                                    <section class="button-success pure-button follow" data-id="{{ $friend->id }}">Devenir ami</section>
                                                </p>
                                            @endif
                                        @endif
                                    @else
                                        <p class="addbtncontainer">
                                            <section class="button-error pure-button unfollow" data-id="{{ $friend->id }}">Ne plus être ami</section>
                                        </p>
                                    @endif


                                </li>
                                    @endforeach
                               @else
                                    <p class="nothing"> {{ $user->username }} n'a pas encore d'ami</p>
                               @endif 
                               
                            </ul><!-- fans list -->
                            <span class="clearfix"></span>
                            <section class="pagination">
                                <!-- pagination -->      
                            </section>
                            <span class="clearfix"></span>
                        </section>
                    </section><!-- profilecotent -->
                      <span class="clearfix"></span>
                </section>
@stop