@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos évènements</h3>
                            @include('master/partials/_flash_message')
                                <section class="addnewcontainer">
                                    {{ link_to('studio/events/create',' Ajouter un évènement',array('class' => 'pure-button pure-button-primary icon-calendar')) }}
                                </section>
                            <section class="sectioncontent">
                            @if(count($events))
                                    <table id="userEvents" class="pure-table" itemscope itemtype="http://schema.org/MusicEvent">
                                        <thead>
                                            <tr>
                                                <th><span class="icon-calendar"></span> Date</th>
                                                <th><span class="icon-note-beamed"></span> Évènement</th>
                                                <th><span class="icon-location"></span> Lieu</th>
                                                <th><span class="icon-tags"></span> Ticket(s)</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                @foreach( $events as $event )
                                            <tr  id="{{ $event->id }}" class="event">
                                                <td class="event-date" itemprop="startDate">{{ date("d M Y", strtotime($event->EventDate)) }}</td>
                                                <td class="event-name" itemprop="name">{{ $event->eventName }}</td>
                                                <td class="event-place" itemprop="location">{{ $event->eventPlace }}</td>
                                                <td class="event-ticket">{{ Str::limit($event->EventTicketLink, 20) }}</td>
                                                <td class="editing"><span class="icon-pencil"></span> <a href="events/{{ $event->id }}/edit" class="editevent editing" title="Modifier cet évènement">Modifier</a></td>                                             
                                                <td class="deletion"><span class="icon-trash"></span> <a href="#" class="eventdeletion deletion" title="Supprimer cet évènement">Supprimer</a></td>
                                            </tr>  
                                @endforeach                                                              
                                </tbody>
                            </table>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté d'évènement. Pourquoi ne pas en <a href="/studio/events/create" title="ajouter un évènement" class="addnew">ajouter un?</a></p>
                                @endif 
                            </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop