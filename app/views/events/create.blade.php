@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('username')
{{ Auth::user()->username }}
@stop

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        @include('master/partials/_flash_message')
                         <h3 class="title">Ajouter un nouvel évènement</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/events','id'=> 'neweventadd')) }}
                                {{ $errors->first('eventDate', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('eventDate','Date de l\'évènement') }}
                                    {{ Form::text('eventDate',null,array('id' => 'eventdateid','placeholder' => 'Cliquez ici ou sur le calendrier pour choisir une date')) }}
                                </section>           
                                {{ $errors->first('eventTitle', '<p class="error">:message</p>') }}                     
                                <section class="bloc">
                                    {{ Form::label('eventTitle','Nom de l\'évènement') }}
                                    {{ Form::text('eventTitle') }}
                                </section> 
                                {{ $errors->first('eventPlace', '<p class="error">:message</p>') }}               
                                <section class="bloc">
                                    {{ Form::label('eventPlace','Lieu de l\'évènement') }}
                                    {{ Form::text('eventPlace') }}
                                </section>
                                {{ $errors->first('eventTicket', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('eventTicket','Lien pour achetez un ticket') }}
                                    {{ Form::text('eventTicket') }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Ajouter l\'évènement', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop