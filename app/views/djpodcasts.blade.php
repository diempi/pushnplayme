@extends('master.djslayout')

@section('newtitle')
{{ $user->username }}
@stop

@include('home.header')
@include('home.footer')

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1414022232188157&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
            <section id="maincontent">
                <section class="djprofilecontainer">
                    <section class="breadcrum">
                        <a href="/" title="Accueil"> Accueil</a>
                        <span> > </span>
                        <a href="/djs/all" title="La page des DJS">DJS</a>
                        <span> > </span>
                        <a href="./" title="Les podcasts de {{ $user->username }}">{{ $user->username }}</a>
                        <span> > </span>
                        Podcasts
                    </section>
                    <section class="profileheader">
                        <section class="profilepict">
                            <a href="./" title="Photo du DJ {{ $user->username }}" itemprop="url" ><img src="{{ $user->picture }}" alt="Photo du DJ {{ $user->username }}" title="Photo du DJ {{ $user->username }}" itemprop="image"></a>
                        </section>

                        <section class="profileheaderinfo">
                            <h1 itemprop="name">{{ $user->username }}</h1>
                                <p class="location" itemprop="nationality">  
                                    <?php 
                                        if((($user->country) !== '') AND (($user->country) !== null)) {
                                    ?>                             
                                        <a href="/countries/{{ Str::slug($user->country) }}" title="Voir tous les podcasts de {{ $user->country }}"> <span class="icon-location"></span> {{ $user->country }}</a>
                                    <?php 
                                        }
                                     ?>
                                </p>
                                <p class="style">
                                    @if(count($genres))
                                    {{-- */$i = 0;/* --}}
                                    @foreach( $genres as $genre )
                                    <a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a>
                                    {{-- */$i++/* --}}
                                    @if($i < count($genres) )|
                                    @endif
                                    @endforeach
                                    @else
                            <p class="nothing"> {{ $user->username }} n'a pas encore ajouté de genre</p>
                            @endif
                                </p>
                            <?php 
                                if((($user->url) != 'http://') AND (($user->url) != null)) {
                             ?>
                                <p class="website"><a href="{{ $user->url }}" title="Visitez le site du DJ {{ $user->username }}"><span class="icon-globe-1"> </span> {{ $user->url }}</a></p>
                            <?php 
                                }
                             ?>
                            <p class="socialnetworks">
                            <?php 
                                if(($user->social_fb) != null) {
                             ?>
                                <a href="http://www.facebook.com/{{ $user->social_fb }}" title="Visitez la page Facebook du DJ {{ $user->username }}"><span class="icon-facebook"></span>{{ $user->social_fb }}</a> |
                            <?php 
                                }
                             ?>    <!-- FACEBOOK -->                         

                             <?php 
                                if(($user->social_tw) != null) {
                             ?>
                                <a href="http://www.twitter.com/{{ $user->social_tw }}" title="Visitez la page Twitter du DJ {{ $user->username }}"><span class="icon-twitter"></span>{{ $user->social_tw }}</a> |
                            <?php 
                                }
                             ?> <!-- TWITTER -->   
                             
                             <?php 
                                if(($user->social_ig) != null) {
                             ?>
                                <a href="http://www.instagram.com/{{ $user->social_tw }}" title="Visitez la page Instagram du DJ {{ $user->username }}"><span class="icon-instagramm"></span>{{ $user->social_ig }}</a> 
                            <?php 
                                }
                             ?> <!-- INSTAGRAM -->   

                            </p>
                            <p class="stats">
                                <span class="followers"><a href="/djs/{{ $user->slug }}/friends" title="Voir ses amis"><span class="icon-users"></span> <span class="friendsNb" data-id="{{ count($profileuserfriends) }}">{{ count($profileuserfriends) }}</span> @if( count($profileuserfriends) == 1 || count($profileuserfriends) == 0)  ami  @else amis @endif </a></span> |
                                <span class="podcasts"><span class="icon-rss"> {{ count($podcasts) }} Mix </span></span>
                            </p>

                            @if(Auth::guest())
                            <p class="addbtncontainer">
                                <section class="pure-button-primary pure-button">{{ HTML::link('login', 'Connectez-vous pour l\' ajouter', array('title' => 'Connectez-vous')) }}</section>
                            </p>
                            @elseif($user->id != Auth::user()->id)
                            @if(count($friendids) === 0 )
                            <p class="addbtncontainer">
                                <section class="button-success pure-button follow" data-id="{{ $user->id }}">Devenir ami</section>
                            </p>
                            @elseif(count($friendids) > 0)
                            @if(in_array($user->id,$friendids))
                            <p class="addbtncontainer">
                                <section class="button-error pure-button unfollow" data-id="{{ $user->id }}">Ne plus être ami</section>
                            </p>
                            @else
                            <p class="addbtncontainer">
                                <section class="button-success pure-button follow" data-id="{{ $user->id }}">Devenir ami</section>
                            </p>
                            @endif
                            @endif
                            @else

                            @endif
                        </section>
                    </section><!-- profileheader -->
                    <span class="clearfix"></span>
                    <section class="profilecontent">
                        <section id="fullpodcasts" class="podcasts">
                            <h2>Les podcasts de {{ $user->username }}</h2>
                            <ul id="djpodcasts">
                                @if(count($podcasts))
                                    @foreach( $podcasts as $podcast )
                                        @if( $podcast->status == '1' )
                                            <li itemscope itemtype="http://schema.org/track" class="ui360">
                                                <a href="/podcasts/play/{{ $podcast->id }}" title="Jouer ce mix" itemprop="url" class="playthis" data-id="{{ $podcast->id }}" data-url="{{ $podcast->location }}"><span class="icon-play" ></span></a>
                                                <span class="podcastitle" title="{{ $podcast->title }}" itemprop="name">{{ $podcast->title }}</span>
                                                @if($podcast->length)
                                                    <span class="podcastduration" title="Durée du mix" itemprop="duration"> {{ $podcast->length }}</span>
                                                @endif
                                                @if(Auth::check())
                                                <a href="/podcasts/like/{{ $podcast->id }}" title="Cliquer pour aimer ce mix" class="likethis" data-id="{{ $podcast->id }}"><span class="icon-heart-empty" title="Aimer ce mix"></span></a>
                                                @endif
                                                <a href="/podcasts/download/{{ $podcast->id }}" title="Cliquer pour télécharger ce podcast" class="downloadthis" data-id="{{ $podcast->id }}"><span class="icon-download" title="télécharger ce podcast"></span></a>

                                                <a href="https://twitter.com/share" class="twitter-share-button" data-url="" data-via="pushnplayme" data-text=" Écoutez '{{ $podcast->title }}' sur la page de {{ $user->username }}">Tweeter</a>
                                                <a href="https://plus.google.com/share?url={{ URL::current() }}" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img
  src="/assets/img/gplus-20.png" alt="Share on Google+" class="gplusshare" /></a>

<div class="fb-share-button" data-href="{{ URL::current() }}" data-type="button_count"></div>
                                            </li>
                                        @endif
                                    @endforeach

                               @else
                                    <p class="nothing"> {{ $user->username }} n'a pas encore mis en ligne de mix</p>
                               @endif 
                            </ul>
                        </section>
                    </section><!-- profilecotent -->
                      <span class="clearfix"></span>
                </section>
            </section>
            <span class="clearfix"></span>
            <section class="push"></section>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
@stop