<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	{{ HTML::style('assets/css/mainstyle.css') }}
	{{ HTML::style('assets/css/dash.css') }}
	<title>Tableau de bord - @yield('username')</title>
</head>
	<body>
		<header>
			@yield('header')	
		</header>

		<!-- CONTENT --> 
		<section id="dashboardmain">
			@yield('content')
		</section>
        <section id="footer-container">
			@yield('footer')
        </section>
{{ HTML::script('assets/js/jquery.min.js') }} 
{{ HTML::script('assets/js/jqueryuimin.js') }} 
{{ HTML::script('assets/js/jquery.form.js') }} 
{{ HTML::script('assets/js/script.js') }}
	</body>
</html>