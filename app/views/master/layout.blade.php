<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="@yield('meta_description', 'PushNPlay.me')">
	{{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400,700') }}
	{{ HTML::style('assets/css/mainstyle.css') }}
    {{ HTML::script('assets/js/modernizr.custom.86005.js') }}
	<title> PushNPlay.me | @yield('newtitle','Accueil')</title>
</head>
	<body>
		<section id="header-container">
			@yield('header')	
		</section>

		<!-- CONTENT --> 
		<section id="main-container">
			@yield('content')
		</section>
		<!-- FOOTER -->
        <section id="footer-container">
			@yield('footer')
        </section>

{{ HTML::script('assets/js/jquery.min.js') }} 
{{ HTML::script('assets/js/jqueryuimin.js') }}
{{ HTML::script('assets/js/jquery.form.min.js') }}
{{ HTML::script('assets/js/soundmanager2-jsmin.js') }}
{{ HTML::script('assets/js/script.js') }}
	</body>
</html>