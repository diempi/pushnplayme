@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
            <section class="djprofilecontainer"  itemscope itemtype="http://schema.org/Person">
                {{ Form::open(array('url' => 'showselect')) }}
                    {{ Form::label('vid','Choisir le type de vidéo') }}
                    {{ Form::select('vidt',array('Vim' => 'Vimeo', 'Yout' => 'Youtube', 'Daylim' =>'Daylimotion'),'Yout') }}
                    {{ Form::text('vid',null, array('class' => 'form-control')) }}

                    {{ Form::submit('Voir') }}
                {{ Form::close() }}
            </section>
            <span class="clearfix"></span>
            <section class="push"></section>
@stop