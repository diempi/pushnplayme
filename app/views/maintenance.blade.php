@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
    <section id="error404page">
        Le site est en maintenance merci de revenir plus tard
    </section>
@stop