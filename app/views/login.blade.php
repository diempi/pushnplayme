@extends('master.layout')

@section('meta_description')
    Connectez-vous
@stop

@section('newtitle')
    Connectez-vous
@stop

@include('home.header')
@include('home.footer')

@section('content')
                <section id="signformcontainer">
                    <section class="top">
                        <section class="topwrap">
                            <section class="formtitle">Se connecter</section>
                        </section>
                            <section class="fields">
                                <section class="socialconnect">
                                    <ul>
                                        <li>
                                            <a href="login/fb" title="Se connecter avec Facebook" id="fblogin">
                                                <span class="icon-facebook"></span>
                                                <span class="label">
                                                    Connexion via Facebook
                                                </span>
                                            </a>
                                        </li>                                        

                                        <li>
                                            <a href="login/tw" title="Se connecter avec Twitter" id="twlogin">
                                                <span class="icon-twitter"></span>
                                                <span class="label">
                                                    Connexion via Twitter
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="login/gp" title="Se connecter avec Google" id="gllogin">
                                                <span class="icon-gplus"></span>
                                                <span class="label">
                                                    Connexion via Google
                                                </span>
                                            </a>
                                        </li>

                                    </ul>
                                </section>
                                <section class="or">
                                    <span></span>
                                </section>

                                <section class="formcontainer">
                                    {{ Form::open(array('url' =>'studio', 'id' =>'loginform')) }}
                                        <section class="formfield">
                                        	{{ $errors->first('email', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('Email', 'Votre email') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::email('email',null, array('id' =>'email' ,'placeholder' => 'Votre email')) }}
                                            </section>
                                        </section>    
                                         <section class="formfield">
                                         	{{ $errors->first('password', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('password', 'Votre mot de passe') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::password('password',null, array('id' => 'password' ,'placeholder' => 'Votre mot de passe')) }}
                                            </section>                                             
                                         </section>                        

                                         <section class="formfield">
                                            <section class="submitfield">
                                            	{{Form::submit('Se connecter', array('id' => 'loginbtn', 'class' => 'pure-button-primary pure-button'))}}
                                            </section>
                                         </section>
                                         <section class="formfield">
                                            <section class="checklabel">
                                            	{{ Form::label('remember','Se souvenir de moi') }}
                                            </section>
                                            <section class="checkfield">
                                            	{{ Form::checkbox('remember',null, array('id' => 'remember','checked' => 'unchecked')) }}
                                            </section>   
                                         </section>
                                         <section class="formfield forgot">
                                             <a href="forgot" title="Retrouvez votre mot de passe">Mot de passe oublié?</a>
                                             <a href="register" title="Inscrivez-vous">Pas encore inscrit(e)?</a>
                                         </section>
                                    {{Form::close()}}
                                </section>
                            </section>
                        </section>
                        <span class="clearfix"></span>
                </section>
            <span class="clearfix"></span>
            <section class="push"></section>
@stop