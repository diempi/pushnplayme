@extends('master.layout')

@section('meta_description')
    Inscription
@stop

@section('newtitle')
    Inscription
@stop

@include('home.header')
@include('home.footer')

@section('content')
<section id="signformcontainer">
                    <section class="top">
                        <section class="topwrap">
                            <section class="formtitle">S'inscrire</section>
                        </section>
                            <section class="fields">
                                <section class="socialconnect">
                                    <ul>
                                        <li>
                                            <a href="login/fb" title="S'inscrire avec Facebook" id="fblogin">
                                                <span class="icon-facebook"></span>
                                                <span class="label">
                                                    S'inscrire via Facebook
                                                </span>
                                            </a>
                                        </li>                                        

                                        <li>
                                            <a href="login/tw" title="S'inscrire avec Twitter" id="twlogin">
                                                <span class="icon-twitter"></span>
                                                <span class="label">
                                                    S'inscrire via Twitter
                                                </span>
                                            </a>
                                        </li>

                                        <li>
                                            <a href="login/gp" title="S'inscrire avec Google" id="gllogin">
                                                <span class="icon-gplus"></span>
                                                <span class="label">
                                                    S'inscrire via Google
                                                </span>
                                            </a>
                                        </li>

                                    </ul>
                                </section>
                                <section class="or">
                                    <span></span>
                                </section>

                                <section class="formcontainer">
                                	{{Form::open(array('url' =>'registered', 'id' => 'registerform'))}}
                                        <section class="formfield">
                                        	{{ $errors->first('username', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('username','Votre nom d\'artiste') }}
                                            </section>
                                            <section class="inputfield">
                                            	{{ Form::text('username',null,array('id' => 'username', 'name' => 'username', 'placeholder' => 'Nom d\'artiste')) }}	
                                            </section>
                                        </section>                                         
                                        <section class="formfield">
                                        	{{ $errors->first('email', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('email', 'Votre email')}}
                                            </section>
                                            <section class="inputfield">
                                            	{{ Form::text('email',null,array('id' => 'email', 'name' => 'email', 'placeholder' => 'Votre email')) }}
                                            </section>
                                        </section>    
                                         <section class="formfield">
                                         	{{ $errors->first('password', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('password', 'Votre mot de passe')}}
                                            </section>
                                            <section class="inputfield">
                                            	{{ Form::password('password',null,array('id' => 'password', 'name' => 'password', 'placeholder' => 'Votre mot de passe'))}}	
                                            </section>                                             
                                         </section>                        
<!---->
<!--                                        <section class="formfield">-->
<!--                                            <section class="labelfield">-->
<!--                                                {{ Form::label('userRole','Quel type d\'utilisateur êtes-vous?') }}-->
<!--                                            </section>-->
<!--                                            <section class="inputfield">-->
<!--                                                {{ Form::select('userRole',array('1' => 'Normal', '2' => 'DJ')) }}-->
<!--                                            </section>-->
<!--                                        </section>-->


                                         <section class="formfield">
                                            <section class="submitfield">
                                            	{{ Form::submit('S\'inscrire',array('id' => 'loginbtn', 'value' => 'S\'inscrire', 'class' => 'button-success pure-button'))}}
                                            </section>
                                         </section>
                                         <section class="formfield alreadysign">
                                             <a href="login" title="Connectez-vous">Déjà inscrit(e)?</a>
                                         </section>
                                    {{Form::close()}}
                                </section>
                            </section>
                        </section>
                        <span class="clearfix"></span>
                </section>
@stop