<!DOCTYPE html>
<html lang="fr-BE">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Réinitialiser votre mot de passe</h2>

<div>
    Pour réinitialiser votre mot de passe, veuillez remplir ce formulaire se trouvant à cette adresse: <a href="{{ URL::to('password_resets/reset', array($token)) }}" title="Cliquez sur ce lien pour réinitialiser votre mot de passe"></a> {{ URL::to('password_resets/reset', array($token)) }} </a>.
</div>
</body>
</html>