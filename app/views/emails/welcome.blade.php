<!DOCTYPE HTML>
<html lang="fr_BE">
    <head>
        <meta charset="UTF-8" />
        <title>PushNPlay.me vous souhaite la bienvenue</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
		{{ HTML::style('assets/css/welcome.css') }}

        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>
    <body>
        <div id="wrapper">
            <header class="page-header">
                <h1>PushNPlay.me <small>Vos podcasts sur internet</small></h1>
            </header>

            <section class="hero-unit">
					<h2>Bienvenue sur PushNPlay.me, {{ $username }}</h2>
					<p>
						Toute l'équipe de PushNPlay.me vous souhaite la bienvenue! 
						Nous espérons que vous allez apprécier notre site, qu'attendez-vous pour mettre <a href="http://www.pushnplay.me/login" title="Allez connectez-vous">en ligne votre premier podcast?</a>
					</p>
            </section>
        </div>

    </body>
</html>
