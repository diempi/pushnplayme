@extends('master.layout')

@include('user.header')
@include('home.footer')

@section('content')
   <h3 class="title">Test de fomulaire</h3>
<div class="content-box container">
	  {{ Form::open(array('url' => 'formtest','id' => 'profilepictform', 'files' => true, 'enctype' => 'multipart/form-data')) }}
            <!-- PICTURE -->
            <div class="bloc">
                {{ Form::label('userpicture', 'Photo de profil') }}
                {{ Form::file('userpicture') }}
            </div>
            {{ Form::submit('Envoyez une image') }}
        {{ Form::close() }}
</div>
@stop