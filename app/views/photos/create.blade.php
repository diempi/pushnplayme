@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter une nouvelle photo</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/photos','files' => true ,'id'=> 'uploadnewphotoform')) }}
                                {{ $errors->first('photoTitle', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('photoTitle','Entrez le titre de la photo') }}
                                    {{ Form::text('photoTitle') }}
                                </section>
                                {{ $errors->first('newPhoto', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('newPhoto','Sélectionner la photo à envoyer: ') }}
                                    {{ Form::file('newPhoto') }}
                                </section>
                                <section class="bloc">
                                    <!-- Progress bar -->
                                    <section class="progress">
                                        <section class="bar"></section>
                                        <section class="percent">0%</section>
                                    </section>
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Ajouter votre photo', array('class' => 'submissionbtnok')) }}
                                     <a href="{{ URL::previous() }}" title="Annuler" class="cancellingbtn">Annuler</a>
                                 </section>
                            {{ Form::close() }}
                        </section>       
                    </section>

                </section>
        </section>
@stop