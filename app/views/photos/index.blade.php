@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos photos</h3>
                            
                            <section class="sectioncontent">
                            	@include('master/partials/_flash_message')
                            	<section class="addnewcontainer">
                            		{{ link_to('studio/photos/create',' Ajouter une photo',array('class' => 'pure-button pure-button-primary icon-picture')) }}
                            	</section>
                                @if(count($photos))
                                    <ul id="userphotos">
                                         @foreach( $photos as $photo )
                                            <li id="{{ $photo->id }}" class="photo">
                                                <a href="/studio/photos/{{ $photo->id }}/edit" title="{{ $photo->photoTitle }}">
                                                	<img src="{{ $photo->photoThumbnail }}" alt="{{ $photo->photoTitle }}" title="{{ $photo->photoTitle }}">
                                                </a>
                                                <p class="options">
                                                	{{ link_to('studio/photos/'.$photo->id.'/edit',' Modifier',array('class' => 'editing', 'title' => 'Modifier la photo')) }}                   

                                                	{{ link_to('#',' Supprimer',array('class' => 'photodeletion right', 'title' => 'Supprimer la photo')) }}                	
                                                </p>

                                            </li>
                                         @endforeach 
                                    </ul>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de photo. Pourquoi ne pas en <a href="/studio/photos/create" title="ajouter une photo" class="addnew">ajouter une?</a></p>
                                @endif 
                            </section>

                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop