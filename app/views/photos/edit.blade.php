@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <section id="editpod" class="content-box container">
                            <h3 class="title">Modification de  votre photo</h3>

                            {{ Form::open(array('url' => 'studio/photos/'.$photo->id, 'id' => 'photoform', 'files' => true, 'enctype' => 'multipart/form-data', 'method' => 'put')) }}
                                    {{ $errors->first('oldPhoto', '<p class="error">:message</p>') }}
                                    <section class="oldPhoto">
                                        <img src="{{ $photo->photoThumbnail }}" alt="{{ $photo->photoTitle }}" title="{{ $photo->photoTitle }}"> 
                                    </section>                                 
                                    {{ $errors->first('photoTitle', '<p class="error">:message</p>') }}
                                    <section class='bloc'>
                                        {{ Form::label('photoTitle', 'Titre de la photo', array('class' => 'bloctitle' )) }}
                                                
                                        {{ Form::text('photoTitle', $photo->photoTitle) }}
                                    </section>
                                    {{ $errors->first('userPhoto', '<p class="error">:message</p>') }}
                                    <section class="bloc">
                                        {{ Form::label('userPhoto', 'Photo de profil') }}
                                        {{ Form::file('userPhoto') }}
                                    </section>
                                    <section class="bloc">
                                        <!-- Progress bar -->
                                        <section class="progress">
                                            <section class="bar"></section>
                                            <section class="percent">0%</section>
                                        </section>
                                    </section>
                                    <section class='bloc'>
                                        {{ Form::submit('Sauvegader les modifications', array('class' => 'submissionbtnok')) }}                           
                                        <a href="{{ URL::previous() }}" title="Annuler" class="cancellingbtn">Annuler</a>                          
                                    </section>

                            {{ Form::close() }}

                        </section>
                    </section>

                </section>
            <section  class="wrapper">
            </section>
        </section>
@stop