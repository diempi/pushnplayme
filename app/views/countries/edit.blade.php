@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        @include('master/partials/_flash_message')
                         <h3 class="title">Modifier votre évènement</h3>
                        <section class="sectioncontainer">
                            {{ Form::open(array('url' => 'studio/events/'.$event->id,'id'=> 'neweventadd')) }}
                                {{ $errors->first('eventDate', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('eventDate','Date de l\'évènement') }}
                                    {{ Form::text('eventDate',$event->EventDate,array('id' => 'eventdateid')) }}
                                </section>           
                                {{ $errors->first('eventTitle', '<p class="error">:message</p>') }}                     
                                <section class="bloc">
                                    {{ Form::label('eventTitle','Nom de l\'évènement') }}
                                    {{ Form::text('eventTitle',$event->eventName) }}
                                </section> 
                                {{ $errors->first('eventPlace', '<p class="error">:message</p>') }}               
                                <section class="bloc">
                                    {{ Form::label('eventPlace','Lieu de l\'évènement') }}
                                    {{ Form::text('eventPlace',$event->eventPlace) }}
                                </section>
                                {{ $errors->first('eventTicket', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('eventTicket','Lien pour achetez un ticket') }}
                                    {{ Form::text('eventTicket',$event->EventTicketLink) }}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Modifier l\'évènement', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop