@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
                <section class="djprofilecontainer">
                    <section class="breadcrum">
                        <a href="/" title="Accueil"> Accueil</a>
                        <span> >  </span>
                        Tous les pays
                    </section>
                    <section class="profileheader">

                        <ul id="allstyles">
                            @if(count($countries))
                                 @foreach ($countries as $countrie)
                                    <li class='genretag' ><a href="/countries/{{ $countrie->countrySlug }}" title="{{ $countrie->countryNameFr }}">{{ $countrie->countryNameFr }}</a></li>
                                 @endforeach 
                            @else
                            <p class="nothing">Il n'y aucun pays</p>
                            @endif
                        </ul>
                            <span class="clearfix"></span>
                            <section class="pagination">
                                <!-- pagination -->      
                            </section>
                            <span class="clearfix"></span>
                        </section>
                    
                </section>
@stop