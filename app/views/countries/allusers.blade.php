@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
                <section class="djprofilecontainer">
                    <section class="breadcrum">
                        <a href="/" title="Accueil"> Accueil</a>
                        <span> >  </span>
                        <a href="/countries" title="Tous les pays"> Tous les pays</a>
                        <span> >  </span>
                        {{ $country->countryNameFr }}
                    </section>
                    <section class="profileheader">
                            <h1 class="singlegenretag">Tous les DJS {{ $country->countryNameFr }}</h1>
                        <ul id="singlestyledj">
                            @if(count($users))
                                 @foreach ($users as $user)
                                    <li class="djgrid">
                                        <a href="/djs/{{ $user->slug }}" title="Voir la page de {{ $user->username }}">
                                            <section class="picture">
                                                <img src="{{ $user->picture }}" alt="Voir la page de {{ $user->username }}">
                                            </section>
                                            <p class="name">{{ $user->username }}</p> 
                                        </a>
                                    </li>
                                 @endforeach 
                            @else
                            <p class="nothing">Il n'y aucun membre venant du pays :  {{ $country->countryNameFr }}</p>
                            @endif
                        </ul>
                            <span class="clearfix"></span>
                            <section class="pagination">
                                <!-- pagination -->      
                            </section>
                            <span class="clearfix"></span>
                        </section>
                    
                </section>
@stop