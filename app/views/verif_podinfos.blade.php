<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Bienvenue <?php echo $username ; ?> </title>
        <meta name="description" content="PushNPlay.me, platefore d'écoute et de gestion de podcasts">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

         <link rel="stylesheet/less" href="<?php echo(LESS.'normalize.less'); ?>" type="text/less" />
         <link rel="stylesheet/less" href="<?php echo(LESS.'main.less'); ?>" type="text/less" />
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700' rel='stylesheet' type='text/css'>
        <script src="./../js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

            <!-- HEADER -->
            <header>
                <div id="top" class="wrapper">
                <h1><a href="./../index.html" title="PushNplay.me">PushNPlay.me</a></h1>
                    <nav title="Menu de navigation" role="navigation">
                        <ul id="main-menu" role="navigation">
                            <li><a href="./../djs.html" title="La liste des DJS">DJS</a></li>
                            <li><a href="./../podcasts.html" title="La liste des podcasts">Podcasts</a></li>
                        </ul>
                        <ul id="logout" role="navigation">
                            <li><a href="#"></a></li>
                            <li><a href="./../index.html" id="signout" title="Se déconnecter">Se déconnecter</a></li>
                        </ul>
                    </nav>                    
                </div>

            </header>
            <div class="wrapper" role="main">
                <div id="dashboard" class="content-box container">
                    <h2 class="title">Bienvenue Utilisateur</h2>
                    <div class="userdash">
                        <div class="userpict">
                            <img src="" alt="">
                        </div>
                        <div class="userinfos">
                            
                        </div>
                        <ul class="userlinks">
                            <li><a href="dashboard.html" title="Retour au tableau de bord"><span class="icon-home"></span> Tableau de bord</a></li>
                            <li><a href="modif_profil.html" title="Gestion du profil"><span class="icon-user"></span> Gérer son profil</a></li>
                            <li><a href="manage_podcast.html" title="Gestion des podcasts"><span class="icon-signal"></span> Gérer ses podcasts</a></li>
                            <li><a href="comments.html" title="Gestion des commentaires"><span class="icon-chat"></span> Gérer les commentaires</a></li>
                            <li><a href="podcast_stats.html" title="Voir les stats des podcasts"><span class="icon-chart-bar-1"></span> Statistiques du podcast</a></li>
                            <li><a href="send_podcast.html" title="Ajouter un nouveau podcast" class="btn-follow">Ajouter un podcast <span class="icon-upload"></span></a></li>
                        </ul>
                    </div>
                </div>
                <!-- RECENT ACTIVITY -->
                <div id="recentactiv" class="content-box container">
                    <h3 class="title">Activité récente</h3>
                        <section id="dj-main">
                            <table>
                                <caption><h1>Vos derniers mix</h1></caption>
                                <tr>
                                    <th>Titre du podcast</th>
                                    <th>Date d'envoi</th>
                                    <th>Nb. de tél.</th>
                                    <th>Nb. d'écoute</th>
                                    <th>Nb. de vote</th>
                                </tr>
                                <tr>
                                    <td class="podcast-title">HTML Rocks</td>
                                    <td class="upload-date">10 Novembre 2012</td>                       
                                    <td class="dl-count">20</td>
                                    <td class="listen-count">100</td>
                                    <td class="vote-count">3</td>
                                </tr>
                                <tr>
                                    <td class="podcast-title">CSS 4 life mix</td>
                                    <td class="upload-date">5 Novembre 2012</td>                        
                                    <td class="dl-count">10</td>
                                    <td class="listen-count">100</td>
                                    <td class="vote-count">8</td>
                                </tr>
                                <tr>
                                    <td class="podcast-title">Code mixiter</td>
                                    <td class="upload-date">22 Octobre 2012</td>                        
                                    <td class="dl-count">100</td>
                                    <td class="listen-count">200</td>
                                    <td class="vote-count">50</td>
                                </tr>                                                               
                                <tr>
                                    <td class="podcast-title">Drupal is the way</td>
                                    <td class="upload-date">5 Octobre 2012</td>                     
                                    <td class="dl-count">1000</td>
                                    <td class="listen-count">2000</td>
                                    <td class="vote-count">150</td>
                                </tr>   
                            </table>
                        </section>
                </div>
                <!-- SIDEBAR -->
                <aside id="dashsidebar" class="content-box container">
                    <h3 class="title">Derniers Tweets</h3>
                    sffsdf
                </aside>
            </div>

        <!-- FOOTER  -->
            <footer role="contentinfo">
                <div id="footerinfos">
                    <h3>Retrouvez-nous sur:</h3>
                    <ul id="social-btn">
                        <li itemscope>
                            <a itemprop="url" href="#" id="rss">Flux RSS</a>
                        </li>
                        <li itemscope>
                            <a itemprop="url" href="#" id="twitter">Compte Twitter</a>
                        </li>
                        <li itemscope>
                            <a itemprop="url" href="#" id="fb">Compte Facebook</a>
                        </li>
                    </ul>    
                    <ul id="footer-menu">
                        <li><a href="apropos.html" title="à propos">à propos</a></li>
                        <li><a href="cgu.html" title="Conditions générales d'utilisation">Conditions générales d'utilisation</a></li>
                        <li><a href="faq.html" title="F.A.Q">F.A.Q</a></li>
                        <li><a href="contact.html" title="Contactez-nous">Contactez-nous</a></li>
                    </ul>
                    <p>2012 Copyrights</p>                   
                </div>
            </footer>

        <!-- SCRIPTS --> 
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
        <script src="./../js/less-1.3.3.min.js" type="text/javascript"></script>
        <script src="./../js/plugins.js"></script>
        <script src="./../js/main.js"></script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src='//www.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
    </body>
</html>
