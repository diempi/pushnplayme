@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos vidéos</h3>
                            
                            <section class="sectioncontent">
                            	@include('master/partials/_flash_message')
                            	<section class="addnewcontainer">
                            		{{ link_to('studio/videos/create',' Ajouter une vidéo',array('class' => 'pure-button pure-button-primary icon-picture')) }}
                            	</section>
                                @if(count($videos))
                                    <ul id="uservideos">
                                         @foreach( $videos as $video )
                                            <li id="{{ $video->id }}" class="video">
                                                <a href="/studio/videos/{{ $video->id }}/edit" title="{{ $video->videoTitle }}">
                                                	<img src="{{ $video->videoThumbnail }}" alt="{{ $video->videoTitle }}" title="{{ $video->videoTitle }}">
                                                </a>
                                                <p class="options">
                                                	{{ link_to('studio/videos/'.$video->id.'/edit',' Modifier',array('class' => 'editing', 'title' => 'Modifier la vidéo')) }}                   

                                                	{{ link_to('#',' Supprimer',array('class' => 'videodeletion right', 'title' => 'Supprimer la vidéo')) }}                	
                                                </p>

                                            </li>
                                         @endforeach 
                                    </ul>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de vidéo. Pourquoi ne pas en {{ link_to('studio/videos/create',' ajouter une?',array('class' => 'addnew')) }}</p>
                                @endif 
                            </section>

                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop