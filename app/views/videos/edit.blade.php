@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Modifier la vidéo</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/videos/'.$video->id,'id'=> 'addvideolink','files' => true,'method' => 'put')) }}
                                {{ $errors->first('videoTitle', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('videoTitle','Titre de la vidéo') }}
                                    {{ Form::text('videoTitle',$video->videoTitle) }}
                                </section>
                                <section class="bloc oldVideo">
                                    <img src="{{ $video->videoThumbnail }}" alt="{{ $video->videoTitle }}" title="{{ $video->videoTitle }}"> 
                                </section>  
                                {{ $errors->first('videoLink', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('videoLink','Lien de la vidéo(Youtube uniquement)') }}
                                    {{ Form::text('videoLink',$video->videoLink)}}
                                </section>
                                {{ $errors->first('videoThumbnail', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('videoThumbnail','Envoyer une miniature de la vidéo(Optionnel)') }}
                                    {{ Form::file('videoThumbnail')}}
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Modifier la vidéo', array('class' => 'submissionbtnok')) }}
                                     <a href="{{ URL::previous() }}" title="Annuler" class="cancellingbtn">Annuler</a>
                                 </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop