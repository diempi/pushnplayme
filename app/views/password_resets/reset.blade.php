@extends('master.layout')

@section('newtitle')
    Réinitialisez votre mot de passe
@stop

@include('home.header')
@include('home.footer')

@section('content')
                <section id="signformcontainer">
                    <section class="top">
                        <section class="topwrap">
                            <section class="formtitle">Réinialisez votre mot de passe</section>
                        </section>
                            <section class="fields">
                                <section class="fullformcontainer">
                                    {{ Form::open() }}
                                        {{ Form::hidden('token', $token) }}
                                        <section class="formfield">
                                            <section class="labelfield">
                                                {{ Form::label('email','Entrez votre email:') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::text('email',null,array('required' => 'required', 'placeholder' => 'Votre email')) }}
                                            </section>
                                        </section>  
                                        <section class="formfield">
                                            <section class="labelfield">
                                                {{ Form::label('password','Entrez votre nouveau mot de passe:') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::password('password',array('required' => 'required', 'placeholder' => 'Votre nouveau mot de passe')) }}
                                            </section>
                                        </section>  
                                        <section class="formfield">                                            
                                            <section class="labelfield">
                                                {{ Form::label('password_confirmation','Confirmez le mot de passe:') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::password('password_confirmation',array('required' => 'required', 'placeholder' => 'Confirmez votre nouveau mot de passe')) }}
                                            </section>
                                        </section>    
                                        <section class="formfield">
                                            <section class="submitfield">
                                                {{ Form::submit('Réinitaliser votre mot de passe',array('id' => 'loginbtn', 'class' => 'pure-button-primary pure-button')) }}
                                            </section>
                                        </section>
                                    {{ Form::close() }}
                                </section>
                                <section class="fields">
                                    @if (Session::has('error'))
                                        <p>{{ Session::get('reason') }}</p>
                                    @endif
                                </section>
                            </section>
                        </section>
                        <span class="clearfix"></span>
                </section>
@stop