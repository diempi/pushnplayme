@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
    <section id="signformcontainer">
                  <p class="banneduser">
                      Nous sommes désolés mais l'utilisateur <span class="bannedusername"> {{ $user->username }} </span>a été banni. Vous pouvez toujours voir le profil d'autres DJS en visitant <a href="/djs/all" title="Voir le profil d'autres DJS">la page des DJS.</a>
                  </p>
    </section>
    <span class="clearfix"></span>
    <section class="push"></section>
@stop