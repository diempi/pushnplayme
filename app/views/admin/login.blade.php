<!doctype html> 
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	{{ HTML::style('assets/css/dash.css') }}
	<title>Admin Login</title>
</head>
	<body>
		<!-- CONTENT --> 
		@include('master/partials/_flash_message')
		<section id="main">
			{{ Form::open(array('url'=>'admindashboard', 'method' => 'post','id' =>'adminloginform', 'class'=>'pure-form pure-form-stacked')) }}
				<p>
					{{ $errors->first('email', '<p class="error">:message</p>') }}
					{{ Form::label('Email', 'Votre email') }}
					{{ Form::email('email',null, array('placeholder' => 'Votre email')) }}
				</p>

				<p>
					{{ $errors->first('password', '<p class="error">:message</p>') }}
					{{ Form::label('password', 'Votre mot de passe') }}
					{{ Form::password('password',null, array('placeholder' => 'Votre mot de passe')) }}
				</p>

				<p>
					{{ Form::label('remember','Restez connecté') }}
					{{ Form::checkbox('remember') }}
				</p>

				<p class="adminsubmission">
					{{ Form::submit('Se connecter', ['class' => 'pure-button pure-button-primary']) }}
				</p>
			{{ Form::close() }}
		</section>
		<!-- FOOTER -->
	    <footer role="contentinfo">
			<p>2014 Copyrights</p>     
        </footer>
{{ HTML::script('assets/js/jquery.min.js') }} 
{{ HTML::script('assets/js/jqueryuimin.js') }}
{{ HTML::script('assets/js/jquery.form.min.js') }}
{{ HTML::script('assets/js/script.js') }}
	</body>
</html>