@extends('master.dashboardmaster')

@section('content')
<div id="layout">
    <!-- Menu toggle -->
    <a href="#menu" id="menuLink" class="menu-link">
        <!-- Hamburger icon -->
        <span></span>
    </a>

    <div id="menu">
        <div class="pure-menu pure-menu-open">
            <a class="pure-menu-heading" href="#">Admin</a>

            <ul>
                <li><a href="#">Accueil</a></li>
                <li><a href="{{ URL::to('adminlogout') }}" title="Se déconnecter" class="icon-logout">Se déconnecter</a></li>
            </ul>
        </div>
    </div>

    <div id="main">
    <h1>Bienvenue {{ Auth::user()->username }} </h1>
    @include('master/partials/_flash_message')
    <table id="usersinadmin" class="pure-table">
        <thead>
            <tr>
                <th>Nom de l'utilisateur</th>
                <th>Bannir</th>
            </tr>
        </thead>
        <tbody>
        {{ Form::open(array('url' => 'adminsave','id' => 'adminUsers', 'files' => true, 'enctype' => 'multipart/form-data')) }}
        @if(count($users))
            @foreach ($users as $user)
                    <tr>
                        <td>{{ Form::label($user->username) }}</td>
                        <td>
                            {{ Form::select($user->username,array('1' => 'Banni', '0' => 'Non banni'),$user->banned) }}
                        </td>
                    </tr>
            @endforeach
        @endif
        </tbody>
    </table>
        {{ Form::submit('Sauvegader les modifications', array('class' => 'submissionbtnok')) }}   
        {{ Form::close(); }}
@stop 
    </div>
</div>
