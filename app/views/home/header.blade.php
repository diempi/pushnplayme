@section('header')
            <header class="container">
                <h1 id="logo">
                    <a href="/" title="PushNplay.me"><img src="/assets/img/logopnpwhite-169px.png" alt="Logo PNP"></a>
                </h1>
                {{ Form::open(array('url' => 'search','id' => 'topsearch', 'class' => 'pure-form')) }}
                    {{ Form::text('topsearchform',null,array('id' => 'topsearchform','placeholder' => 'Rechercher')) }}
                    {{ Form::submit('OK',array('class' => 'pure-button pure-button-primary')) }}
                {{ Form::close() }}
                <nav id="topmenu" class="pure-menu pure-menu-open pure-menu-horizontal">
                    <ul>
                        <li>{{ HTML::link('djs/all', 'DJS',  array('title' => 'Accéder aux DJS')) }}</li>
                        <li>{{ HTML::link('podcasts/all', 'Podcasts', array('title' => 'Accéder aux podcasts')) }}</li>
                    </ul>
                    @if(Auth::check())
                        <ul id="usermenu" role="navigation">
                            <li class="pure-button-primary pure-button"><a href="{{ URL::to('studio/dashboard') }}"><span class="icon-user"></span> Mon compte</a></li>
                            <li><a href="{{ URL::to('logout') }}?referer={{Request::segment(1)}}/{{Request::segment(2)}}" title="Se déconnecter" class="icon-logout button-error">Se déconnecter</a></li>
                        </ul>
                    @else
                        <section class="pure-button-primary pure-button">{{ HTML::link('login', 'Connectez-vous', array('title' => 'Connectez-vous')) }}</section>
                        <section class="recrute button-success pure-button">{{ HTML::link('register', 'Inscrivez-vous gratuitement', array('title' => 'Rejoignez-nous')) }}</section>
                    @endif
                        <a href="#" id="pull">Menu</a>  
                </nav>
            </header>
                        @include('master/partials/_flash_message')
        </section>
@stop
