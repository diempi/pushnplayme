@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
            @if(Auth::guest())
              <section class="joinus container">
                  <section>Vous avez des mix que vous voulez partager avec le monde entier? Vous cherchez un moyen simple et efficace? Alors PushNPlay est fait pour vous!
                  </section>

                      <button class="recrute button-success pure-button"><a href="register" title="Rejoignez-nous">Inscrivez-vous gratuitement</a></button>

              </section>
            @endif
            <section id="maincontent" class="container">

                <section class="featured">
                    <h2 class="bigtitle">
                        Les DJS du moment
<!--                         <span class="prompt">
                            <a href="#" title="Jouer les mix de tous les djs">Jouer tous</a>
                            <span class="separate"></span>
                            <a href="#" title="Voir tous les djs">Voir tous</a>
                        </span> -->
                    </h2>

                    <section id="featureddjs">
                        <ul id="bigfeatured">
                            <li class="featuredj">
                                <a href="/djs/nicky-romero" title="Voir le profil de Nicky Romero">
                                    <span class="icon-headphones"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/nickyromero.jpg" alt="Photo de Nicky Romero">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Nicky Romero</h3>
                                        <span class="djstyle"><a href="/styles/house">House</a>, <a href="/styles/tech-house">Tech House</a></span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="/djs/steve-angello" title="Voir le profil de Steve Angello">
                                    <span class="icon-headphones"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/1402642578-Steve_Angello-profile.jpg" alt="Photo de Steve Angello">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Steve Angello</h3>
                                        <span class="djstyle"><a href="/styles/edm">EDM</a>, <a href="/styles/house">House</a>, <a href="/styles/techno">Techno </a></span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="/djs/afrojack" title="Voir le profil d'Afrojack'">
                                    <span class="icon-headphones"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/1402502194-afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="/styles/edm">EDM</a>, <a href="/styles/electronique">Electronique</a>, <a href="/styles/tech-house">Tech House</a>
                                        </span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="/djs/didier-dmp" title="Voir le profil de Didier DMP'">
                                    <span class="icon-headphones"></span>
                                    <section class="djpict">
                                        <img src="./assets/img/defaultprofilepict.jpg" alt="Photo de Didier DMP">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Didier DMP</h3>
                                        <span class="djstyle">
                                            <a href="/styles/rnb">RNB</a>, <a href="/styles/hip-hop-rap">Hip-Hop-Rap</a>
                                        </span>
                                    </section>
                                </a>
                            </li>
                        </ul>
                         <span class="clearfix"></span>
                    </section>
                </section>
                <span class="clearfix"></span>
                <section class="contentwithsidebar container">

                    <section id="content" class="charts local">
                        <h2 class="bigtitle">
                            Le top podcasts du site
                        </h2>
<!--                         <p class="prompt">
                            <a href="#" class="allcharts" title="Voir tous les classements">Tous les classements</a>
                            <span class="separate">  </span>
                            <a href="#" class="locationcheck" title="Changer de ville">Changer de ville</a>
                        </p> -->
                        <ol class="pure-table" id="welcomechart">
                         <?php $i =1; ?>
                            @foreach($djs as $dj)
                                @if($dj->banned == 0)
                                    @if( $i % 2 === 0)
                                        <li class='pair'>
                                            <section class="chartnumber">{{ $i }}</section>
                                            <section class="chartpict"><a href="/djs/{{ $dj->slug }}" title="Voir le profil de {{ $dj->username }}"><img src="{{ $dj->picture }}" alt="{{ $dj->username }}"></a></section>
                                            <section class="charsectiondjname"><a href="/djs/{{ $dj->slug }}" title="Voir le profil de {{ $dj->username }}">{{ $dj->username }}</a></section>
                                            <section class="djstyle">
                                                @if(count($dj->genres))
                                                    {{-- */$k = 0;/* --}}
                                                    @foreach( $dj->genres as $genre )
                                                        <a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a>
                                                        {{-- */$k++/* --}}
                                                        @if($k < count($dj->genres) )
                                                            |
                                                        @endif
                                                    @endforeach
                                                @else
                                                 <p class="nothing"> {{ $dj->username }} n'a pas encore ajouté de genre</p>
                                                @endif
                                            </section>

                                        </li>
                                    @else
                                        <li>
                                            <section class="chartnumber">{{ $i }}</section>
                                            <section class="chartpict"><a href="/djs/{{ $dj->slug }}" title="Voir le profil de {{ $dj->username }}"><img src="{{ $dj->picture }}" alt="{{ $dj->username }}"></a></section>
                                            <section class="charsectiondjname"><a href="/djs/{{ $dj->slug }}" title="Voir le profil de {{ $dj->username }}">{{ $dj->username }}</a></section>
                                            <section class="djstyle">
                                                @if(count($genres))
                                                    @foreach( $dj->genres as $genre )
                                                        <a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a> |
                                                    @endforeach
                                            @else
                                             <p class="nothing"> {{ $user->username }} n'a pas encore ajouté de genre</p>
                                            @endif
                                            </section>

                                        </li>
                                    @endif
                                    <?php $i++ ?>
                                @endif
                            @endforeach

                        </ol>
                    </section><!-- content -->

                    <section id="sidebar">
                        <h2 class="bigtitle">Top 5 des DJS du site</h2>
                        <ol class="pure-table">
                            <li>
                                <section class="chartnumber">1</section>
                                <section class="chartpict"><a href="/djs/nicky-romero" title="Voir le profil de Nicky Romero"><img src="/assets/img/nicky-romero-2013.jpg" alt="The One"></a></section>
                                <section class="charsectiondjname"><a href="/djs/nicky-romero" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>

                            </li>
                            <li class="pair">
                                <section class="chartnumber">2</section>
                                <section class="chartpict"><a href="/djs/steve-aoki" title="Voir le profil de Steve Aoki"><img src="/assets/img/steve_aoki.jpg" alt="Steve Aoki"></a></section>
                                <section class="charsectiondjname"><a href="/djs/steve-aoki" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>

                            </li>
                            <li>
                                <section class="chartnumber">3</section>
                                <section class="chartpict"><a href="/djs/afrojack" title="Voir le profil d'Afrojack"><img src="/assets/img/1402502194-afrojack.jpg" alt="Jacked"></a></section>
                                <section class="charsectiondjname"><a href="/djs/afrojack" title="Voir le profil d'Afrojack">Afrojack</a></section>

                            </li>
                            <li class="pair">
                                <section class="chartnumber">4</section>
                                <section class="chartpict"><a href="/djs/simina" title="Voir le profil de Simina"><img src="/assets/img/simina grigoriu.jpg" alt="Simina"></a></section>
                                <section class="charsectiondjname"><a href="/djs/simina" title="Voir le profil de Simina">Simina</a></section>

                            </li>
                            <li>
                                <section class="chartnumber">5</section>
                                <section class="chartpict"><a href="/djs/joyce" title="Voir le profil de Joyce"><img src="/assets/img/joycemuniz.jpg" alt="Joyce"></a></section>
                                <section class="charsectiondjname"><a href="/djs/joyce" title="Voir le profil de Joyce">Joyce</a></section>

                            </li>
                        </ol>

                        <section class="bloc">
                            <p class="recrute">Vous êtes DJ ou rêvez de le devenir? <a href="register" title="Inscrivez-vous maintenant!!">Inscrivez-vous gratuitement</a> et faites découvrir votre talent! Et tout ça <a href="register" title="Inscrivez-vous maintenant!!">gratuitement!</a></p>
                        </section>

                        <section class="bloc">
                            <h2 class="bigtitle">Nuages de Styles</h2>
                            <ul id="tags">
                                @foreach ($genres as $genre)
                                <li><a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}" class="{{ $genreClasses[array_rand($genreClasses,1)] }}">{{ $genre->genreName }}</a></li>
                                @endforeach
                            </ul>
                        </section>

                        <section class="bloc">

                        </section>
                    </section><!-- sidebar -->

@stop
