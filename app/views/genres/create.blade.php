@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        @include('master/partials/_flash_message')
                         <h3 class="title">Ajouter un nouveau style</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/genres','id'=> 'neweventadd')) }}
                                {{ $errors->first('eventDate', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('genres','Choisir votre genre', array('class' => 'bloctitle' )) }}
                                    {{ Form::select('genres', $genres, 'Choisir')}}
                                </section>           
                                 <section class="bloc">
                                    {{ Form::submit('Ajouter le genre', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop