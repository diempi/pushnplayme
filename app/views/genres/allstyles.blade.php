@extends('master.layout')

@section('newtitle')
    Tous les styles de musique du site
@stop

@include('home.header')
@include('home.footer')

@section('content')
                <section class="djprofilecontainer">
                    <section class="breadcrum">
                        <a href="/" title="Accueil"> Accueil</a>
                        <span> >  </span>
                        Tous les styles
                    </section>
                    <section class="profileheader">

						<ul id="allstyles">
							@if(count($genres))
								 @foreach ($genres as $genre)
								 	<li class='genretag' ><a href="styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}">{{ $genre->genreName }}</a></li>
								 @endforeach 
							@else
							<p class="nothing">Il n'y aucun style disponible</p>
							@endif
						</ul>
                            <span class="clearfix"></span>
                            <section class="pagination">
                                <!-- pagination -->      
                            </section>
                            <span class="clearfix"></span>
                        </section>
                    
                </section>
@stop