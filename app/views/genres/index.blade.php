@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')

@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos styles musicaux</h3>
                            @include('master/partials/_flash_message')
                                <section class="addnewcontainer">
                                    {{ link_to('studio/genres/create',' Ajouter un style',array('class' => 'pure-button pure-button-primary icon-music')) }}
                                </section>
                            <section class="sectioncontent">
                            @if(count($genres))
                                <ul id="userGenres" itemscope itemtype="http://schema.org/MusicEvent">
                                @foreach( $genres as $genre )
                                        <li id="{{ $genre->id }}" class="genre"> 
                                            <p>{{ $genre->genreName }}</p> 
                                            {{ link_to('#',' Supprimer',array('class' => 'genredeletion right', 'title' => 'Supprimer le genre')) }}  
                                        </li>
                                    @endforeach                                                              
                                </ul>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de style. Pourquoi ne pas en <a href="/studio/genres/create" title="ajouter un évènement" class="addnew">ajouter un?</a></p>
                                @endif 
                            </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop