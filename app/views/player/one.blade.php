@extends('master.layout')



@section('content')
	<section id="player">
		<section class="player-control">
            <section class="progress">
                <section class="bar"></section>
                <section class="percent"></section>
            </section>
		</section>
		<section class="player-btn">
			<a href="#"  data-url="/uploads/twerk.mp3" class="icon-play" title="play" role="button"></a>
			<a href="#" class="icon-stop" title="stop" role="button"></a>
		</section>
	</section>

<ul id="djpodcasts">
	<li>		
		<a href="/podcasts/like/32"  data-url="/uploads/twerk.mp3" class="icon-play" title="play" role="button"></a>
	</li>
</ul>
@stop