@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
                <section id="signformcontainer">
                    <section class="top">
                        @include('master/partials/_flash_message')
                        <section class="topwrap">
                            <section class="formtitle">Bienvenue parmi nous {{ $email }} !</section>
                        </section>
                            <section class="fields">
                                <section class="fullformcontainer">
                                    <p class="success">Votre inscription s'est terminée avec succès! Vous pouvez vous connectez et accéder à votre compte.</p>
                                    <section class="firstlogin">
                                    {{ Form::open(array('url' =>'studio', 'id' =>'loginform')) }}
                                        <section class="formfield">
                                            {{ $errors->first('email', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('Email', 'Votre email') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::email('email',null, array('id' =>'email' ,'placeholder' => 'Votre email')) }}
                                            </section>
                                        </section>    
                                         <section class="formfield">
                                            {{ $errors->first('password', '<p class="error">:message</p>') }}
                                            <section class="labelfield">
                                                {{ Form::label('password', 'Votre mot de passe') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::password('password',null, array('id' => 'password' ,'placeholder' => 'Votre mot de passe')) }}
                                            </section>                                             
                                         </section>                        

                                         <section class="formfield">
                                            <section class="submitfield">
                                                {{Form::submit('Se connecter', array('id' => 'loginbtn', 'class' => 'pure-button-primary pure-button'))}}
                                            </section>
                                         </section>
                                         <section class="formfield">
                                            <section class="checklabel">
                                                {{ Form::label('remember','Se souvenir de moi') }}
                                            </section>
                                            <section class="checkfield">
                                                {{ Form::checkbox('remember',null, array('id' => 'remember')) }}
                                            </section>   
                                         </section>
                                         <section class="formfield forgot">
                                             <a href="forgot" title="Retrouvez votre mot de passe">Mot de passe oublié?</a>
                                             <a href="register" title="Inscrivez-vous">Pas encore inscrit(e)?</a>
                                         </section>
                                    {{Form::close()}}
                                    </section>
                                </section>
                            </section>
                        </section>
                        <span class="clearfix"></span>
                </section>
@stop