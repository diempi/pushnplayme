@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
<section id="error404page">
    Stop là! Vous n'avez pas la permission d'accéder à cette zone.
</section>
@stop