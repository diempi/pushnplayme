@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <section id="editpod" class="content-box container">
                            <h3 class="title">Modification de  votre podcast</h3>

                            {{ Form::open(array('url' => 'studio/podcasts/'.$podcast->id, 'id' => 'podcastform', 'files' => true, 'enctype' => 'multipart/form-data', 'method' => 'put')) }}
                                            {{ $errors->first('title', '<p class="error">:message</p>') }}
                                            <section class='bloc'>
                                                {{ Form::label('title', 'Nom du mix', array('class' => 'bloctitle' )) }}
                                                
                                                {{ Form::text('title', $podcast->title) }}
                                            </section>
                                            {{ $errors->first('description', '<p class="error">:message</p>') }}
                                            <section class='bloc'>
                                                {{ Form::label('description', 'Description du mix', array('class' => 'bloctitle' )) }}
                                                {{ Form::textarea('description', $podcast->description) }}
                                            </section>
                                            {{ $errors->first('podcastFile', '<p class="error">:message</p>') }}
                                             <section class="bloc">
                                                {{ Form::label('podcastFile','Sélectionner le fichier à envoyer: ') }}
                                                {{ Form::file('podcastFile') }}
                                            </section>
                                            <section class="bloc">
                                                <span class="warning"> Attention les stats du podcast seront réinitialisés si vous changez de fichier!!</span>
                                            </section>
                                            {{ $errors->first('status', '<p class="error">:message</p>') }}
                                            <section class="bloc">
                                                {{ Form::label('status','Voulez-vous le mettre en ligne?',array('class' => 'bloctitle')) }}
                                                {{ Form::select('status',array('1' => 'Oui', '0' => 'Non'),$podcast->status) }}
                                            </section>
                                            <section class="bloc">
                                                <!-- Progress bar -->
                                                <section class="progress">
                                                    <section class="bar"></section>
                                                    <section class="percent">0%</section>
                                                </section>
                                            </section>
                                            <section class='bloc'>
                                                {{ Form::submit('Sauvegader les modifications', array('class' => 'submissionbtnok')) }}                           
                                                <a href="{{ URL::previous() }}" title="Annuler" class="cancellingbtn">Annuler</a>                          
                                            </section>

                            {{ Form::close() }}

                        </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop