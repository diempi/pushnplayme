@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- TITLE -->
                            <h3 class="title">Vos podcasts</h3>
                            <section class="sectioncontent">
                                @if(count($podcasts))
                                    <table id="stats">
                                        <caption><h1>Vos derniers mix</h1></caption>
                                        <tr>
                                            <th>Titre du podcast</th>
                                            <th>Date d'envoi</th>
                                            <th>Nb. de tél.</th>
                                            <th>Nb. d'écoute</th>
                                            <th>Nb. de vote</th>
                                        </tr>
                                         @foreach( $podcasts as $podcast )
                                        <tr id="{{ $podcast->id }}" class="podcast">
                                            <td class="podcast-title"> {{ $podcast->title }} </td>
                                            <td class="upload-date"> {{ $podcast->created_at }} </td>
                                            <td class="dl-count"> {{ $podcast->DownloadNb }} </td>
                                            <td class="listen-count"> {{ $podcast->ListenNb }} </td>
                                            <td class="vote-count">150</td> 
                                        </tr>
                                         @endforeach 
                                    </table>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore mis en ligne de mix</p>
                                @endif 
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop