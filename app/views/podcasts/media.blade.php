@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos médias</h3>
                            <section class="sectioncontent">
                                <h4>Vos photos</h4>
                                @if(count($photos))
                                    <ul>
                                         @foreach( $photos as $photo )
                                            <li>
                                                <img src="{{ $photo->photoLocation }}" alt="{{ $photo->photoTitle }}" title="{{ $photo->photoTitle }}">
                                                <a href="#" title="Supprimer" class="photodeletion">Supprimer</a>
                                            </li>
                                         @endforeach 
                                    </ul>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de photo. Pourquoi ne pas en <a href="add_photo" title="ajouter une vidéo" class="addnew">ajouter une?</a></p>
                                @endif 
                            </section>
                            <section class="sectioncontent">
                                <h4>Vos vidéos</h4>
                                @if(count($videos))
                                    <ul>
                                         @foreach( $videos as $video )
                                            <li>
                                                <img src="{{ $video->videoThumbnail }}" alt="{{ $video->videoTitle }}" title="{{ $video->videoTitle }}">
                                                <a href="#" title="Supprimer" class="videodeletion">Supprimer</a>
                                            </li>
                                         @endforeach 
                                    </ul>
                                @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de Vidéo. Pourquoi ne pas en <a href="add_video" title="ajouter une vidéo" class="addnew">ajouter une?</a></p>
                                @endif 
                            </section>
                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop