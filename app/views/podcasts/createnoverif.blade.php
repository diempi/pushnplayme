@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                         <h3 class="title">Ajouter un nouveau podcast</h3>
                        <section class="formcontainer">
                            {{ Form::open(array('url' => 'studio/podcasts/create/noverif','files' => true,'id'=> 'uploadformnoverif')) }}
                                {{ $errors->first('podTitle', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('podTitle','Entrez le titre') }}
                                    {{ Form::text('podTitle') }}
                                </section>
                                {{ $errors->first('podDescription', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('podDescription','Décrivez votre mix') }}
                                    {{ Form::textarea('podDescription') }}
                                </section>
                                {{ $errors->first('podcastFile', '<p class="error">:message</p>') }}
                                 <section class="bloc">
                                    {{ Form::label('podcastFile','Sélectionner le fichier à envoyer: ') }}
                                    {{ Form::file('podcastFile') }}
                                </section>
                                {{ $errors->first('podcastStatus', '<p class="error">:message</p>') }}
                                <section class="bloc">
                                    {{ Form::label('podcastStatus','Voulez-vous le mettre en ligne?',array('class' => 'bloctitle')) }}
                                    {{ Form::select('podcastStatus',array('1' => 'Oui', '0' => 'Non'),'Oui') }}
                                </section>
                                <section class="bloc">
                                    <!-- Progress bar -->
                                    <section class="progress">
                                        <section class="bar"></section>
                                        <section class="percent">0%</section>
                                    </section>
                                </section>
                                 <section class="bloc">
                                    {{ Form::submit('Ajouter le podcast', array('id' => 'submitnewpodcast')) }}
                                </section>
                            {{ Form::close() }}
                        </section>                 
                    </section>

                </section>
        </section>
@stop