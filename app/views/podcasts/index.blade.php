@extends('master.dashboardmaster')

@include('user.header')
@include('user.footer')
@section('username')
{{ Auth::user()->username }}
@stop
@section('content')
            <section class="wrapper" role="main">
                <section id="dashboard" class="content-box">

                    <!-- Left Sidebar -->
                    <section class="leftcontent">
                        @include('user.left-menu')                        
                    </section>

                    <!-- Middle Content -->
                    <section class="rightcontent">
                        <!-- Gérer vos épisodes -->
                            <h3 class="title">Vos podcasts</h3>
                            @include('master/partials/_flash_message')
                            <section class="sectioncontent">

                            	<section class="addnewcontainer">
                            		{{ link_to('studio/podcasts/create',' Ajouter un podcast',array('class' => 'pure-button pure-button-primary icon-picture')) }}
                            	</section>
                                <section class="sectioncontent">
                                    @if(count($podcasts))
                                        <table id="stats">
                                            <caption><h1>Statistiques</h1></caption>
                                            <tr>
                                                <th>Titre du podcast</th>
                                                <th>Date d'envoi</th>
                                                <th>Statut</th>
                                            <th>Nb. de tél.</th>
                                            <th>Nb. d'écoute</th>
                                            <th>Nb. de likes</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                             @foreach( $podcasts as $podcast )
                                            <tr id="{{ $podcast->id }}" class="podcast">
                                                <td class="podcast-title"> {{ $podcast->title }} </td>
                                                <td class="upload-date"> {{ $podcast->created_at }} </td>
                                                <td class="pod-status"> 
                                                    @if($podcast->status == '1')
                                                        Publié
                                                    @else
                                                        Non publié
                                                    @endif
                                                </td>
                                            <td class="dl-count"> {{ $podcast->DownloadNb }} </td>
                                            <td class="listen-count"> {{ $podcast->ListenNb }} </td>
                                            <td class="vote-count">{{ $podcast->likesNb }}</td> 
                                                <td><a href="podcasts/{{ $podcast->id }}/edit" title="Modifier ce podcast" class="podcastediting editing"><span class="icon-pencil"></span> Modifier</a> </td>
                                                <td><a href="#" title="Supprimer ce podcast" class="podcastdeletion deletion" ><span class="icon-trash"></span> Supprimer</a></td> 
                                            </tr>
                                             @endforeach 
                                        </table>
                                    @else
                                    <p class="nothing"> Vous n'avez pas encore ajouté de podcast. Pourquoi ne pas en <a href="/studio/podcasts/create" title="ajouter un podcast" class="addnew">ajouter un?</a></p>
                                    @endif 
                                </section>
                            </section>

                    </section>

                </section>
                <section  class="wrapper">
            </section>
        </section>
@stop