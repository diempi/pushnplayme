@extends('master.layout')

@section('meta_description')
    Les podcasts du site
@stop

@section('newtitle')
    Les podcasts du site
@stop

@include('home.header')
@include('home.footer')

@section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1414022232188157&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<section class="high"></section>
<section id="maincontent" class="container">
    <span class="clearfix"></span>
    <section class="contentwithsidebar container">
		<section id="content" class="charts local">
			<h2 class="bigtitle">Les Podcasts</h2>
			<ul id="allpodcasts">
                                @if(count($podcasts))
                                    @foreach( $podcasts as $podcast )
                                        @if( $podcast->status == '1' )
                                            <li itemscope itemtype="http://schema.org/track" class="ui360 podcastview">
                                                <section class="podcastimage">
                                                    <a href="/djs/{{ User::find($podcast->BelongsTo)->slug }}">
                                                        <img src="{{ User::find($podcast->BelongsTo)->picture }}" alt="{{ User::find($podcast->BelongsTo)->username }}"/>
                                                    </a>
                                                </section>
                                                <section class="podcastplayicon">
                                                    <a href="/podcasts/play/{{ $podcast->id }}" title="Jouer ce mix" itemprop="url" class="playthis" data-id="{{ $podcast->id }}" data-url="{{ $podcast->location }}"><span class="icon-play" ></span></a>
                                                </section>
                                                <section class="podcastinfos">
                                                    <span class="podcastitle" title="{{ $podcast->title }}" itemprop="name">{{ $podcast->title }}</span>
                                                    par <span class="podcastdj"><a
                                                            href="/djs/{{ User::find($podcast->BelongsTo)->slug }}">{{ User::find($podcast->BelongsTo)->username }}</a></span>

                                                           <p> <span class="icon-headphones"></span><span class="podcastlisten" title="Nombre d'écoutes"> {{ $podcast->ListenNb }}  </span>

                                                            <span class="icon-heart"></span><span class="podcastlisten" title="Nombre de likes"> {{ $podcast->likesNb }}   </span></p>

                                                        @if($podcast->length)
                                                            <span class="podcastduration" title="Durée du mix" itemprop="duration"> {{ $podcast->length }}</span>
                                                        @endif
                                                </section>

                                            </li>
                                        @endif
                                    @endforeach
                                @endif
			</ul>
            <div class="pagination">
                {{ $podcasts->links() }}
            </div>
        </section><!-- content -->       

                    <section id="sidebar">
                        <h2 class="bigtitle">Top 5 des Podcasts</h2>
                        <ol class="pure-table">
                            <li>
                                <section class="chartnumber">1</section>
                                <section class="chartpict"><a href="/djs/nicky-romero" title="Voir le profil de Nicky Romero"><img src="/assets/img/nicky-romero-2013.jpg" alt="The One"></a></section>
                                <section class="charsectiondjname"><a href="/djs/nicky-romero" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>

                            </li>                              
                            <li class="pair">
                                <section class="chartnumber">2</section>
                                <section class="chartpict"><a href="/djs/steve-aoki" title="Voir le profil de Steve Aoki"><img src="/assets/img/steve_aoki.jpg" alt="Steve Aoki"></a></section>
                                <section class="charsectiondjname"><a href="/djs/steve-aoki" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>

                            </li>                                
                            <li>
                                <section class="chartnumber">3</section>
                                <section class="chartpict"><a href="/djs/afrojack" title="Voir le profil d'Afrojack"><img src="/assets/img/1402502194-afrojack.jpg" alt="Jacked"></a></section>
                                <section class="charsectiondjname"><a href="/djs/afrojack" title="Voir le profil d'Afrojack">Afrojack</a></section>

                            </li>                              
                            <li class="pair">
                                <section class="chartnumber">4</section>
                                <section class="chartpict"><a href="/djs/simina" title="Voir le profil de Simina"><img src="/assets/img/simina grigoriu.jpg" alt="Simina"></a></section>
                                <section class="charsectiondjname"><a href="/djs/simina" title="Voir le profil de Simina">Simina</a></section>

                            </li> 
                            <li>
                                <section class="chartnumber">5</section>
                                <section class="chartpict"><a href="/djs/joyce" title="Voir le profil de Joyce"><img src="/assets/img/joycemuniz.jpg" alt="Joyce"></a></section>
                                <section class="charsectiondjname"><a href="/djs/joyce" title="Voir le profil de Joyce">Joyce</a></section>

                            </li>                            
                        </ol>

                        <section class="bloc">
                            <p class="recrute">Vous êtes DJ ou rêvez de le devenir? <a href="register" title="Inscrivez-vous maintenant!!">Inscrivez-vous gratuitement</a> et faites découvrir votre talent! Et tout ça <a href="register" title="Inscrivez-vous maintenant!!">gratuitement!</a></p>
                        </section>

                        <section class="bloc">
                            <h2 class="bigtitle">Nuages de Styles</h2>
                            <ul id="tags">
                                @foreach ($genres as $genre)
                                    <li><a href="/styles/{{ $genre->genreSlug }}" title="{{ $genre->genreName }}" class="{{ $genreClasses[array_rand($genreClasses,1)] }}">{{ $genre->genreName }}</a></li>
                                @endforeach
                            </ul>

                        </section>

                    </section><!-- sidebar -->
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
@stop

