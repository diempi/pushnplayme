@extends('master.layout')

@include('home.header')
@include('home.footer')

@section('content')
    <section class="container">
        <h1>Les podcasts de nos DJS</h1>
        <ul id="allpodcasts">
        	@foreach( $podcasts as $podcast )
        		<li class="podcastlistitem">
		                        <article class="container podcast">
		                                <section class="play_border">
		                                    <a href="{{ $podcast->location }}" class="play_button" title="Ecouter {{ $podcast->BelongsTo }}"></a>
		                                    <audio src="{{ $podcast->location }}" controls></audio>
		                                </section>
		                                <section class="metainfos" itemscope itemtype="http://schema.org/AudioObject">
		                                    <span class="pod-title" itemprop="name">{{ $podcast->title }} envoyé par <a href="djs/{{ $podcast->BelongsTo }}" title="Visiter le profil de {{ $podcast->BelongsTo }}">{{ $podcast->BelongsTo }}</a> </span>
		                                </section>                          
		                        </article> 
        		</li>
        	@endforeach
        </ul>
    </section>
@stop