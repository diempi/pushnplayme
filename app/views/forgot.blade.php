@extends('master.layout')

@section('newtitle')
    Retrouvez votre mot de passe
@stop

@include('home.header')
@include('home.footer')

@section('content')
                <section id="signformcontainer">
                    <section class="top">
                        <section class="topwrap">
                            <section class="formtitle">Retrouvez votre mot de passe</section>
                        </section>
                            <section class="fields">
                                <section class="fullformcontainer">
                                    {{ Form::open(['route' => 'password_resets.store']) }}
                                        <section class="formfield">
                                            <section class="labelfield">
                                                {{ Form::label('email','Entrez votre email:') }}
                                            </section>
                                            <section class="inputfield">
                                                {{ Form::text('email',null,array('required' => true, 'placeholder' => 'Votre email')) }}
                                            </section>
                                        </section>    
            
                                        <section class="submitfield">
                                            {{ Form::submit('Réinitaliser votre mot de passe',array('id' => 'loginbtn', 'class' => 'pure-button-primary pure-button')) }}
                                        </section>

                                    {{ Form::close() }}
                                </section>
                                <section class="fields">
                                    @if (Session::has('status'))
                                        <p>{{ Session::get('status') }}</p>
                                    @endif
                                </section>
                            </section>
                        </section>
                        <span class="clearfix"></span>
                </section>
@stop