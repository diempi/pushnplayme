@extends('master.layout')
@section('newtitle')
    Acceuil
@stop
@include('home.header')

@include('home.footer')


@section('content')

            @if(Auth::guest())
            <section class="joinus container">
                <p>Vous avez des mix que vous voulez partager avec le monde entier? Vous cherchez un moyen simple et efficace? Alors PushNPlay est fait pour vous!</p>
                <form action="#">
                    <button class="recrute button-success pure-button"><a href="signup.html" title="Rejoignez-nous">Inscrivez-vous gratuitement</a></button>
                </form>
            </section>
            @endif
            <section id="maincontent" class="container">

                <section class="featured">
                    <h2 class="bigtitle">
                        Les DJS mis en avant
                        <span class="prompt">
                            <a href="#" title="Jouer les mix de tous les djs">Jouer tous</a>
                            <span class="separate"></span>
                            <a href="#" title="Voir tous les djs">Voir tous</a>
                        </span>
                    </h2>

                    <section id="featureddjs">
                        <ul id="bigfeatured">
                            <li class="featuredj">
                                <a href="profile.html" title="Voir le profil de Nicky Romero">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/nickyromero.jpg" alt="Photo de Nicky Romero">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Nicky Romero</h3>
                                        <span class="djstyle"><a href="style.html">House</a>, <a href="style.html">Thech House</a></span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="profile.html" title="Voir le profil de Simina">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/simina.jpg" alt="Photo de Simina">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Simina</h3>
                                        <span class="djstyle"><a href="style.html">Funk</a></span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="profile.html" title="Voir le profil d'Afrojack'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/afrojack.jpg" alt="Photo d'Afrojack">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Afrojack</h3>
                                        <span class="djstyle">
                                            <a href="style.html">Electro</a>, <a href="style.html">Electro House</a>
                                        </span>
                                    </section>
                                </a>
                            </li>

                            <li class="featuredj">
                                <a href="profile.html" title="Voir le profil de Joyce'">
                                    <span class="icon-play"></span>
                                    <section class="djpict">
                                        <img src="./img/joyce.jpg" alt="Photo de Joyce">
                                    </section>
                                    <section class="djmetainfos">
                                        <h3 class="djname">Joyce</h3>
                                        <span class="djstyle"><a href="style.html">Hip-Hop</a></span>
                                    </section>
                                </a>
                            </li>
                        </ul>
                         <span class="clearfix"></span>
                    </section>
                </section>
                <span class="clearfix"></span>
                <section class="contentwithsidebar container">

                    <section id="content" class="charts local">
                        <h2 class="bigtitle">
                            Les top DJS de <span class="currentcity"><a href="#" title="Liège">Liège</a></span>
                        </h2>
                        <p class="prompt">
                            <a href="#" class="allcharts" title="Voir tous les classements">Tous les classements</a>
                            <span class="separate">  </span>
                            <a href="#" class="locationcheck" title="Changer de ville">Changer de ville</a>
                        </p>
                        <ol class="pure-table" id="welcomechart">
                                <li>
                                    <section class="chartnumber">1</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Major Lazer"><img src="./img//bomaye.jpg" alt="Bomayé"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Major Lazer">Major Lazer</a></section>
                                    <section class="djstyle"><a href="style.html">Moombathon</a>, <a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="1"><a href="#" title="Écouter Bomayé de Major Lazer"><span class="icon-play"></span></a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartnumber">2</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="2"><a href="#" title="Écouter The One de Nicky Romero"><span class="icon-play"></span></a></section>
                                </li>
                                <li>
                                    <section class="chartnumber">3</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Major Lazer"><img src="./img//bomaye.jpg" alt="Bomayé"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Major Lazer">Major Lazer</a></section>
                                    <section class="djstyle"><a href="style.html">Moombathon</a>, <a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="3"><a href="#" title="Écouter Bomayé de Major Lazer"><span class="icon-play"></span></a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartnumber">4</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="4"><a href="#" title="Écouter The One de Nicky Romero"><span class="icon-play"></span></a></section>
                                </li>
                                <li>
                                    <section class="chartnumber">5</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Major Lazer"><img src="./img//bomaye.jpg" alt="Bomayé"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Major Lazer">Major Lazer</a></section>
                                    <section class="djstyle"><a href="style.html">Moombathon</a>, <a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="5"><a href="#" title="Écouter Bomayé de Major Lazer"><span class="icon-play"></span></a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartnumber">6</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="6"><a href="#" title="Écouter The One de Nicky Romero"><span class="icon-play"></span></a></section>
                                </li>
                                <li>
                                    <section class="chartnumber">7</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Major Lazer"><img src="./img//bomaye.jpg" alt="Bomayé"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Major Lazer">Major Lazer</a></section>
                                    <section class="djstyle"><a href="style.html">Moombathon</a>, <a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="7"><a href="#" title="Écouter Bomayé de Major Lazer"><span class="icon-play"></span></a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartnumber">8</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="8"><a href="#" title="Écouter The One de Nicky Romero"><span class="icon-play"></span></a></section>
                                </li>
                                <li>
                                    <section class="chartnumber">9</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Major Lazer"><img src="./img//bomaye.jpg" alt="Bomayé"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Major Lazer">Major Lazer</a></section>
                                    <section class="djstyle"><a href="style.html">Moombathon</a>, <a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="9"><a href="#" title="Écouter Bomayé de Major Lazer"><span class="icon-play"></span></a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartnumber">10</section>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                    <section class="playbutton" data-id="10"><a href="#" title="Écouter The One de Nicky Romero"><span class="icon-play"></span></a></section>
                                </li>
                                <li>
                                    <!--  PAGINATION -->
                                </li>
                        </ol>
                    </section><!-- content -->

                    <section id="sidebar">
                        <h2 class="bigtitle">Top 5 des DJS du site</h2>
                        <ol class="pure-table">
                            <li>
                                <section class="chartnumber">1</section>
                                <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                <section class="djstyle"><a href="style.html">Electro House</a></section>
                            </li>
                            <li class="pair">
                                <section class="chartnumber">2</section>
                                <section class="chartpict"><a href="profile.html" title="Voir le profil de Steve Aoki"><img src="./img//steveaoki.jpg" alt="Steve Aoki"></a></section>
                                <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>
                                <section class="djstyle"><a href="style.html">House</a></section>
                            </li>
                            <li>
                                <section class="chartnumber">3</section>
                                <section class="chartpict"><a href="profile.html" title="Voir le profil d'Afrojack"><img src="./img//afrojack.jpg" alt="Jacked"></a></section>
                                <section class="charsectiondjname"><a href="profile.html" title="Voir le profil d'Afrojack">Afrojack</a></section>
                                <section class="djstyle"><a href="style.html">Electro</a></section>
                            </li>
                            <li class="pair">
                                <section class="chartnumber">4</section>
                                <section class="chartpict"><a href="profile.html" title="Voir le profil de Simina"><img src="./img//simina.jpg" alt="Simina"></a></section>
                                <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Simina">Simina</a></section>
                                <section class="djstyle"><a href="style.html">Funk</a></section>
                            </li>
                            <li>
                                <section class="chartnumber">5</section>
                                <section class="chartpict"><a href="profile.html" title="Voir le profil de Joyce"><img src="./img//joyce.jpg" alt="Joyce"></a></section>
                                <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Joyce">Joyce</a></section>
                                <section class="djstyle"><a href="style.html">Hip Hop</a></section>
                            </li>
                        </ol>

                        <section class="bloc">
                            <p class="recrute">Vous êtes DJ ou rêvez de le devenir? <a href="signup.html" title="Inscrivez-vous maintenant!!">Inscrivez-vous gratuitement</a> et faites découvrir votre talent! Et tout ça <a href="signup.html" title="Inscrivez-vous maintenant!!">gratuitement!</a></p>
                        </section>

                        <section class="bloc">
                            <h2 class="bigtitle">Nuages de Styles</h2>
                            <ul id="tags">
                                <li><a href="#" title="Funk" class="smalltag">Funk</a></li>
                                <li><a href="#" title="Urban" class="bigtag">Urban</a></li>
                                <li><a href="#" title="House" class="smalltag">House</a></li>
                                <li><a href="#" title="Electro" class="smalltag">Electro</a></li>
                                <li><a href="#" title="Electro House" class="mediumtag">Electro House</a></li>
                                <li><a href="#" title="Reggae" class="bigtag">Reggae</a></li>
                                <li><a href="#" title="Hip Hop" class="mediumtag">Hip Hop</a></li>
                                <li><a href="#" title="Classic" class="mediumtag">Classic</a></li>
                                <li><a href="#" title="Deep House" class="smalltag">Deep House</a></li>
                                <li><a href="#" title="Dubstep" class="bigtag">Dubstep</a></li>
                                <li><a href="#" title="Chillout" class="smalltag">Chillout</a></li>
                                <li><a href="#" title="Rap" class="mediumtag">Rap</a></li>
                                <li><a href="#" title="Slow" class="bigtag">Slow</a></li>
                                <li><a href="#" title="Dancehall" class="mediumtag">Dancehall</a></li>
                            </ul>
                        </section>

                        <section class="bloc">
                        <h2 class="bigtitle">Les derniers DJS Inscrits</h2>
                            <ol id="newdjs" class="pure-table">
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Nicky Romero"><img src="./img//nickyromero.jpg" alt="The One"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Nicky Romero">Nicky Romero</a></section>
                                    <section class="djstyle"><a href="style.html">Electro House</a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Steve Aoki"><img src="./img//steveaoki.jpg" alt="Steve Aoki"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Steve Aoki">Steve Aoki</a></section>
                                    <section class="djstyle"><a href="style.html">House</a></section>
                                </li>
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil d'Afrojack"><img src="./img//afrojack.jpg" alt="Jacked"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil d'Afrojack">Afrojack</a></section>
                                    <section class="djstyle"><a href="style.html">Electro</a></section>
                                </li>
                                <li class="pair">
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Simina"><img src="./img//simina.jpg" alt="Simina"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Simina">Simina</a></section>
                                    <section class="djstyle"><a href="style.html">Funk</a></section>
                                </li>
                                <li>
                                    <section class="chartpict"><a href="profile.html" title="Voir le profil de Joyce"><img src="./img//joyce.jpg" alt="Joyce"></a></section>
                                    <section class="charsectiondjname"><a href="profile.html" title="Voir le profil de Joyce">Joyce</a></section>
                                    <section class="djstyle"><a href="style.html">Hip Hop</a></section>
                                </li>
                            </ol>
                        </section>
                    </section><!-- sidebar -->

@stop
