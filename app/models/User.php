<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
    protected $fillable = ['email','password'];

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function podcasts(){
        return $this->hasMany('Podcast','belongsTo');
    }

    public function videos(){
        return $this->hasMany('Video','userId');
    }

    public function photos(){
        return $this->hasMany('Photo','userId');
    }

    public function djevents(){
        return $this->hasMany('DJEvent','userId');
    }

    /** Friendship **/

    public function friends(){
        return $this->belongsToMany('User','friend_user','userId','friendId');
    }

    public function genres(){
        return $this->belongsToMany('Genre','genre_user');
    }

    public function profiles(){
        return $this->hasMany('Profile');
    }

    public function country(){
        return $this->hasOne('Country','countryNameFr');
    }

    public function activities(){
        return $this->hasMany('Activity','userId');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    public function roles()
    {
        return $this->belongsToMany('Role')->withTimestamps();
    }


    /**
     * Does the user have a particular role?
     *
     * @param $name
     * @return bool
     */
    public function hasRole($name)
    {
        foreach ($this->roles as $role)
        {
            if ($role->name == $name) return true;
        }
        return false;
    }
    /**
     * Assign a role to the user
     *
     * @param $role
     * @return mixed
     */
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }
    /**
     * Remove a role from a user
     *
     * @param $role
     * @return mixed
     */
    public function removeRole($role)
    {
        return $this->roles()->detach($role);
    }


}
