<?php  

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

	class Comment extends Eloquent implements UserInterface, RemindableInterface{

		protected $table = 'comments';

	    public function user()
	    {
	        return $this->belongsTo('Podcast');
	    }

	}