<?php  

	class Country extends User {

		protected $table = 'countries';

	    public function users()
	    {
	        return $this->hasMany('User','country_id');
	    }

	}