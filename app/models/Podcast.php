<?php

	class Podcast extends User 
	{
		
		protected $table = 'podcasts';

		function __construct()
		{
			# code...
		}


		  public function user()
		  {
		    return $this->belongsTo('User');
		  }

		public function tags(){
			return $this->hasMany('Tag','podcastId');
		}	

		public function genres(){
			return $this->hasMany('Genre','podcastId');
		}		
		public function comments(){
			return $this->hasMany('Comment','podcastId');
		}

	}