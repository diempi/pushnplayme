<?php  

	class Friend extends Eloquent {

		protected $table = 'users';

	    public function user()
	    {
	        return $this->belongsTo('User');
	    }

	}