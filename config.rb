require 'compass/import-once/activate'
# Require any additional compass plugins here.
require 'zen-grids'
require 'animation'
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "public/assets/css"
sass_dir = "public/assets/scss"
images_dir = "public/assets/img"
javascripts_dir = "public/assets/js"
fonts_dir = "public/assets/fonts"

output_style = :nested

# To enable relative paths to assets via compass helper functions. Uncomment:
 relative_assets = true

line_comments = false
color_output = false

preferred_syntax = :scss
