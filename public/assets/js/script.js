(function($){
	var percent = $('.percent');
	var bar = $('.bar');
	//Fonction de modification de l'image de profil
		$('#profilepictform').on('submit', function(e) {
		    e.preventDefault(); 
		    $(this).ajaxSubmit({
			        target: ".userprofilepict img",
			        type: "post",
			        enctype: "multipart/form-data",
			        success: function(data) { 
		            	$('.userprofilepict img').attr("src",data).fadeIn('slow'); 
		        	},
		        	// error: function(data){
		        	// 	window.console.log(data);
		        	// },
		        	beforeSend: function(){
		        		$('.progress').css('display','bloc').fadeIn();
		        		bar.width('0%');
		        		percent.html('0%');
		        	}, 

		        	uploadProgress: function(event, position, total, percentComplete){
		        		var pVel = percentComplete + '%';
		        		bar.width(pVel);
		        		percent.html(pVel);
		        	},
				    complete: function() {
						$('.progress').fadeOut();
				    }
		    });
		});	

//============= PODCAST UPLOAD FORM
function validate(e){
    e.preventDefault();
    var postData = $(this).serializeArray();
    //var formURL = $(this).attr("action");
    $("#uploadform").ajaxSubmit({
            //url: formURL,
            method: "post",
            dataType: "json",
            data: postData,
//            beforeSubmit:function(){
//                $(".error").fadeOut("slow");
//            },
            success:function(data){
                // en cas d'erreur
                if(data.success === false){
                    //on affiche les erreurs

                    var allErrors = data.errors;
                    $.each(allErrors, function(index, value)
                    {
                        if (value.length !== 0)
                        {
                            $("#"+index+"").parent().append('<p class="error"><strong>'+ value +'</strong></p>').slideDown().fadeIn();
                        }
                    });
                }
                // en cas de succès on utilise la progression
                else{

                        var percent = $('.percent');
                        var bar = $('.bar');
                        $("#uploadform").ajaxSubmit({
                            /* set data type json */
                            beforeSend: function() {
                              $('.progress').css('display','bloc').fadeIn();
                              bar.width('0%');
                              percent.html('0%');
                            },

                            /* progress bar call back*/
                            uploadProgress: function(event, position, total, percentComplete) {
                              var pVel = percentComplete + '%';
                              bar.width(pVel);
                              percent.html(pVel);
                            },

                            /* complete call back */
                            complete: function() {
                                $('.progress').fadeOut();
                                window.location.href= '/studio/podcasts';
                            }
                          });
                }
        }
    });
}

	// Bar de progression d'envoi de fichier
     $("#uploadform").on("submit", validate);

		// Fonction d'ajout d'une date d'évènement


		// Fonction de la lightbox
		
		$.fn.heplbox = function (oGIvenSettings){
			
			var $overlay,
				$container,
				aElements = [],
				iCurrentIndex,
				oDefaultSettings = {
					cssPrefix: "heplbox-",
					overlayOpacity: 0.66,
					zIndexBase: 9999,
					spinner: "data:image/gif;base64,R0lGODlhIAAgAPYAAP///0RERPv7++Dg4OPj4/z8/MzMzKGhoampqdDQ0Pj4+O3t7aenp6CgoLm5uezs7JaWloCAgKOjo+rq6vX19Z6enrq6usPDw2tra25ubnJycpGRkeXl5fb29p2dnb29vfn5+dbW1m9vb5OTk+/v77e3t5ubm8/Pz93d3Wpqam1tbcHBwdTU1NnZ2fLy8t/f32dnZ8fHx83Nzb+/v/Pz85SUlK6uruLi4unp6aamplFRUU9PT15eXk5OTmVlZbS0tOjo6Obm5vDw8MTExLa2trGxsYuLi6urq7CwsHd3d3t7e35+fnV1dXp6eq2trdra2nh4eHR0dNzc3NLS0n19fbOzs1dXV2FhYWhoaFRUVFJSUsrKyoWFhYGBgby8vJeXl4SEhIiIiI2NjYeHh4KCglxcXFtbW1lZWWJiYtfX19PT05CQkMnJyY6OjmRkZMDAwFVVVYqKilhYWJiYmJqamsbGxl9fX6SkpHFxcaqqqkxMTAAAAAAAAAAAAAAAAAAAACH+GkNyZWF0ZWQgd2l0aCBhamF4bG9hZC5pbmZvACH5BAAFAAAAIf8LTkVUU0NBUEUyLjADAQAAACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKECzk2NJOCDxchgwU1OjsSmQoQGCIWghQiOz01npALERkYGQ4AFBqtP4ILN0ACjgISGhkpGDIANjw+KABCKNEujxMbGiowowAEHIIT0SgUkBwjGiIzhkIvKDiSJCsxwYYdmI8KFB0FjfqLAgYMEiSUEJeoAJABBAgiGnCgQQUPJlgoIgGuWyICCBhoRNBCEbRoFhEVSODAwocTIBQVwEEgiMJEChSkzNTPRQdEFF46KsABxYtphUisAxLpW7QJgkDMxAFO5yIC0V5gEjrg5kcUQB098ElCEFQURAH4CiLvEQUFg25ECwKLpiCmKBC6ui0kYILcuXjz6t3Ld1IgACH5BAAFAAEALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Ohw8Tj44XKlhbk4sKEVZZXAWZgwsxLYMdTJ1RCqEAIA1JSjOCFKhaUSCCoI8kRkpMULIKVFZaXaALN0C6jAVHS01RTFMAVVc8XgBCKNsujwsmS1AaCIJSpQAT2ygUk0AeS0oXhkIvKDihQjEyy4QdNJMgOqxqxC9RCyJFkKwYiKgAkAEE2CWi4CChDSdSFJFQx0ERiCEWQlq4oUjbto6KgCQwIOOJAEUFcBAIInGRgIKsGrrogIhCzUcFgqB40a0QiXpAMj1QJ6kVLgA41P1kxGHbi39HB/A0iaKoo6MvSAgisC0pAGRBXk4SOOjGtiCDFXCGSodCSM6GC7ze3cu3r9+/gAcFAgAh+QQABQACACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjoYkTj8Uj40SPGUMlYsdSzxmSiCbg0IyKIM0TTxnTAqjACAIYGNDgh1Uq1CiAB2VLl9hZGAXsGSrXAUKEjNABY4FRGJjXV0sAD8+aB8ANmItKC6PJAxiXBFIAAIhIYJVUygolI8TCNIxhkAvKDijLidTzgx1oLEJxC5GAReRkLFixZSDhwoAGUBAXiIWQy6smMFBEQl4KDoqenKi5Al+iYSAFJmIwgAUL5opKoCDQBCLM189c9HrEAWcz4LADFeIhD4gmxaAnCDIoCAcIIEuEgqToNEBvVTCI+rIxYAXJAQRgIcUwIIbQQQUPHiD7KCEOhMBTIAnJG7EBVzt6t3Lt6/fvYEAACH5BAAFAAMALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2OhiRVDhSPjQhYPkeViwpjWG5dIJuDBTdBgxRkWGhKCqOCK18QW4IdXKsRogAPHY8FNl8bG2wAIEarRgUKDW4ROI8XHl9rbS0ADhkYbwBIWj1wU48uPx4QYg4ABS1pgm09ZUc0lQtE5SeGR1hEz5sUIWkFDAkAIq9SAQGOAjIC8YLFFBQIExUAMoAAJUU41oVQs0ARCRQgOSyaABKkC0VCSopUJADHjRsTFhXAQSDIRZmvErrodYjCTV9BULw4WYjECxRANn0EGbNYRBwlfzIiKVSe0Ru9UpqsRGHAABKCCIBMCmCBqYiPBKC9MZZUTkJUEIW8PVRgAdG5ePPq3ctXbyAAIfkEAAUABAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GQhZDHY+NSFEiRZWLCmtRGXEgm4QgCoMdYhoZYKajAA9ETmqCnRoqY6IACy6VCQgHDQkAIBAaGCMAChIpShyPTzYMDR4oADNQUUMAVXJZOj+PHRdOOR4rAAVST4Ij3joXlS7jOSyGNnA7YRSbHSgvhyAMvBHiqlEBgxNu3MCxqACQAQT2KXKBoiIKGopIWHQ20eJFRUI2NsShcMJIAkEkNixo0AWlQxRUPioQxB+vQiReoACySWNFk8MECMJhUSajCRVfYMx5g1LIijcdKSAwgIQgAhV56roBRGilAgcF3cg6KCxLAEhREDxbqACJqGwI48qdS7fuqEAAIfkEAAUABQAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GLitsCo+NJRFUM5WLICYRTSMCm4kdc59iIIIgLw+VT2woggp0EVBrogtfblFSjhNeP0hpAAINEUl0AApfZWdyTr4rFkVOBAB1YBFsAD92zlZ1jiBTbw42WwAFL7ECRmZycEYUjxRqbyW9hUfwRiSbIEGCHKLwxoKQUY1AUCjQiAQBAhMWFWjRgkCHRRRQaERBQxGJjRwwbuSoSAhIRg9u3IioqAAOAkAuMmKIsFEBFzINUZi3qUAQFC9cGCKxDsimjxpZghAFAMdGno4eaHzRkeiNiyY1Cn0EgsAAfwAIaDQKYMENIEwr0QRwY+ygtTUUAUzQeDCuoQIkttrdy7ev3799AwEAIfkEAAUABgAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6GBQMDj45sI20ylIsgDG1jBwWaiQp3nl8ggiAyQxSPJCgPqZ1cdAIAJB4pbkeOCmoxF5MCR21cEgAKFTBodmO2jB0hqzM4ADIjRpkOKcw8P48cLAYrIQAFN5MFI252ZRutjiAELFschkVXZWskmgUkC4coXPjgQlQjEDj4MSJBgMCERRPA2MlgYJGCFygy0lCE5MwVH21QjcKoUREBNglY3GC04MaNh4oK4CAARIHBm4gKuOiAiAI8SgWCoHhRsBAJjEA0vcoIE8QzHBlR/Gz0IOOLjUdv8BQStWg8AjcUEsiYFEBLIM+ADrpBdlAonIIRJmQUAhcSCa918+rdy7evqEAAIfkEAAUABwAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6HIAKPjkFFP0CTjB8VXx+ZigI/FRAMkgACCWwdjwVCNIICRKMHkkJ3URlIj0FPITgABQ4VNUcFIDl4KiliposCLygtUyQAIXd0LQAzuClYDo9AKFIhN4ITmAV0GSkwX6uOIBziC4ZEKT4QQpmtr4YddStcfGoEYoI+RkIIEJiwaEIYNxpkLAIBDQWKfojy6NiYRIEiihYvKjrSo2QTEIsW3LjBUNEDD1SohBgIqlmjAi7eGaJA4VOBICheCCxEAhqmSSRCtowkCEfIno8eWHzxquiNVUJCDoVH4AY1AAQsHlUJpIDPQTfEDjJLc9AEiwcP2xYqQGKr3Lt48+rdizcQACH5BAAFAAgALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CHCmkhCpGLU0gMMpeJBUOaPwWCAiwyHZAdlgACF0g5NgIALkcRTSWPEy8DQgAFdUh3uCBOVFBMELKMBTcoKC8UAC8/CC8AQ11NTBozj0DOKA+CJOIFEtp4FaiOIBzPLoZeTHge8JAFLtGGHVt1NJ2MQEzoxUgIAQITFj1og4EJm0UCBoD7l8iGHCtWlIBQFHGiIhtZQmpcZPBGQkUPxIhY8hDgoQIUlDnCt84QBX33grwzROIFCiCRSIA7CUIZDnA4Gz1w9uJfzxuohICzx47ADRKCCDgDCmDBDRyjIoUF0OznoLEuJzgj6LJQARJUCtvKnUu3rt25gQAAIfkEAAUACQAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkIgkC5GMHEMzN5WKLBcOQ4MCL2oKkCAgggWdJR8FADREbWMfjyQvA0KCaRdEFwACJUZcXQ2ujRwoKC8UAEB1FhwABrJdS76OOMkoD4I0JIJOY11UOaWOIMgvNIYXZOTrkAUuzIYKJ1vwm4oCD0FCxomEECAwYRGQGhpUJPmSz5CAAdoaGrpjpyKPKzISFYCYTGIhBGZCmrFjQJELAjcKKnqwIQoTJk4E6DNUoIPNR/I6IGIxRGe8IMpcGCKR4EsbobW0qQQhE0A2KQ5QQHqQTB0AWzd0CtGW6xEIlN8AEEgGRNCCGzgA4hx0g+wgtfoTJiTrOrNQARJI6+rdy7evX76BAAAh+QQABQAKACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QiCACkYxCTywklYoEaTIsgwUcQJEgBYM3aQYygh1vHiYtj0IvN0KCnVtTAAUrJhBrDo8cKCgvFABCLQYTAGoVwGJbjzjFKA+CCjSCDl9rRkgKjyDEL9uFWxtxNuePBS7IhiAsJ/GbigILQED2iEIEBJop4jCHShImYlAkEjDAWrtDOVKkwEIRwilEBBwquuOmY0cIilwQuCEwEQ4ISpRQmUPgnqECHWJeZPSuwyEQQ4bYhFQgiDEXhhxo0TIG6CMS1gROEpQGih4dMSA9KGYOAIlaNoUYwKOHCCQQIzUByIiCFIAFMiqUdIeqmFleLhQHTSh2K26hAiSM2t3Lt6/fv5sCAQAh+QQABQALACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QiAWRjRQ3BAqUihwoKByEIJOQBaIABJ0vggoJRBeZjjQ3N0KCp1IDAAUyRzkHKI9BqBQAQgMoLgBSNgwNDZ+OOJ0oC4Igr3XMJl6ljCCcL8OFagd0Dh2RBS7hhSBPIeeaiwIkODjriC4EBBOLQAdjZLpAwJXoVCcaio4wicJQgwdFBlEgTJQng0WLDxNRIHCDn6IJHsiAAVPhWTxCBTp0eNUoHbxCAmLEeOmoQLAXyAoxsCLHSE5HJKR5BCFAUJgdWqywgfQAFUISL26cQ6IDqQNIIDiSqNUJCAAFDdyI8Thq0I2ugx4UPQlgQidabA4LFSDxM67du3jz6qUUCAAh+QQABQAMACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKECkBAApOJQCgoD5mDBQWDBJwcggUDUwSQHTc3QoKkKEGCTzMODjSPOJwvHQBCAwMUAEErDkVVLo8TnCgLggIggiwWRUd1kCAcKC/EhVJVeRcKkQUu34UCNwPln4kFQg8Pv4oUBAQTixN5NW1iDVYlkoVCV6IfZLp0iRAhhyKCBhEVaUKR4h17BG7oU/TgjpiPOWi9o6TAXaNz9dRt2ZLSUYEg3ZYVysPjyoaIjUg42wgCEwAjVs7YMQDpQS9dJF7c+FXESlAv2jKSiMUJCAAFErBwMWVu0I2qgxZMe9cMBayRhAqQkIm2rdu3cATjNgoEACH5BAAFAA0ALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQKQDgCk4k4KCgPmYMFBYMEnByDJBwUkB03N0KCpChBgkAsBiGQE5wvHQBCAwOqJCEydWyYjg+cKAuCAiCCHMUzuI8CHCgvqoU4dR8J0JAFLtuGOEHhn4gFNCQkyIkUBAQTiwtEBx4mSECKsSg0FH3YsKaNQST+lgVM5GDMmDAObSiSd6OeIhJHvnyZYwOHukIKFKRjNK6XIQpvLph8VCBINheGjrjBMufVIxLLLIIIKIALDzQ+6Ch4pCxbQBIvvrABgIQHjytYTjwCQeAGCVgoPJApoOBLmadeIokSdAMFka0AaHjAomTAJ10XFIiA4nD1UwESC0Z+3Mu3r9+/kAIBACH5BAAFAA4ALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQCEwsFk4k4KCgLmYOYgwScHIMULpEdBDdCgqMoQYITLyg4kBOcLx0AQgMDFLycLS+QC5ydggIgsigtakCQBRwoL8CFQi1TKKGPBS7WhkKXn4unHdyIFAQEE4tCK0VONh+tia8oNIoxBw0VFR5bFN3Ll+jCl4MHYyhSd6OdIiFEJNy54wAVOUIgMnZzscuQixVsOnYLQs0iIRsZNDQw2YjEMYdPSinggkUFngMiGT3IlQ+ICjQBq/jAggGPl0cgVpEQ9ELFjjEFQHgYimGEgGiDWvjYQQaTEAg+Uvz49OKKjiKm2IT8ROFIlZwXCOPKnUu3LqRAACH5BAAFAA8ALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFJCSTijgoKAuYiASbHIMdHZEKHARCgqAoQYITLy+Xjw+bL6VCAwMUAEKbrZALv50AAiCvv6qPBRwoL7yFvig4kgUu0IYUNJ6MChTHixQEBBOLHVMrHytSi6wo24ksVUVISD/wn7/4h1MM/gw2XCgSd6PcwDdIbBBhx62QAAUClrkoZYhGDBkKIhUI4kxgoR9NIiDYx4jEr3ICWrgCIUYDFCp5KDaq5WxbDjlYDABwIEJDEiorHoEgcOMSBRU64BgpAEJCzyQmCkCSCoAEjKRhpLrwICKKBU9tkv4YRMEARk8TjvyQ2bCt27dwBONGCgQAIfkEAAUAEAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAUkJJOKEygoC5iIBJscgyAgkQocBEKCoChBgg8vAzSQD5svHQBCAzcUuZsoOJALv50AAgKCmpuqjwUcKC+9hUKbwZEFLtKGFLOeiwIgBYwUBAQT3y9qCSzMiawo3Yg3dUMXFyeL7/GHUhb+FgYWUeBw45yiDgZmvIlxyVshAeKaucBliIYMNaUgFQgCzYUhL2PaVNHWiMSvcwKeAAEA4ksELnGqKHhUC9osBDxE4PtAJQKYODEegSBw4xIFPFbKbCgAIo8SnzkiOoooBEPSNuJo3KHS5Y2nEVZ4lBjUIc2UmZgm2HCA1qHbt3AF48qVFAgAIfkEAAUAEQAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAUkQpOKDygoC5iIBJscgyAFkQocBJcAoChBgg8vNx2Qmigvs0IDNxQAQpsoD5ALv50AAgKCE7+qjgUctryFQi8oOJIFLtGGHTSejAWljBQEBBOLBUADA0DIiqwo3YkPTy1padbuv/GIQTL+Mq4UUeBww5wiEC1OnJACwpshcJCwzdrG4knDiEFQSAlh6AIEDx8mOnKx6cgcYyFQGDvQpgadDxcbaXqDxQsAJz7wGAAwJE6bEXMSPALxQgwDARSS2IFhwliVMD9/QBJQDAcWOz7aIKPgxEibGJgWqMCqVZCCjTEjUVBix80dh4UQLuChkgZuoQck7Ordy5dQIAAh+QQABQASACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBSQuk4oPKCgkmIgEmxyDAgWRChwEQoKgKEGCDwMEIJCaKC8dAEIDNxS5mygLkAu/wQCkghO/qo8FHLa9hUIvKDiSBS7Qhh00noyljRQEBBOLBUC71YusKNyJw7/Zn7/tiO+b8YcUHDfkigVBLwak60bwWhABhkCguIEQUrMiWH4YksHAxhYFkIQgMLMDgrE0L4w5qXDnCJuGjWZY6QFnBoAiGZQkAGBgDsk8LR6lyeAmj4AOS1LguWPMyxwPEthAIvFAEAkmKUR8KdXBgok7UjA9jVrjm4AbrjC5aJIigwmChTxEfYOW0IISbwgwtp1Lt66gQAAh+QQABQATACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYIPAxwCkJooLx0AQgM3FLibKKmPC74LggKkABO+vI8FHLXLhEIvKDiSBS7QhR00nozHjBQEBBOLBUC6xIurKNyJwpu26r7tiEK+8YoUHDfkigU4BDgA60YQSAkZsgoJCILjm6MJSXrIKWEohIMVaRI6qrJDB5w5AAQ8uSFoho0SH1pAMqEjS5kVAIg0GcMCgBoENoh8ePCohYYUTgR0GBNliRMABergJAIEkpB0QpZEoXKAFIgtPwyAwBQ1ipIK3255okHG6x2Che54rYOWEIkPdQi2tp1Lt66gQAAh+QQABQAUACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYILN0ECkJooLx0AQgM3FLibKKmPC74LggKkABO+vI8FHLXLhEIvKDiSBS7QhR00nozHjBQEBBOLBUC6nYurKNyJwpsDsorr7YhCvvGLFBw35IoFOAhwqNetGw4HJ+QVInEp0gQlWXhYMHRDBosg3xodgSOnTAUABV60AnBixZYpIx15kGPGzRAAXrjUeAJAioUVbNSAePQECp4iAhSs6WKkBMgpXlac2PlICDEALsJ0iXOElIAXCaphchGnS5g8GbvREOPVRsFCR7waOBvtggGmbAbjyp0LIBAAIfkEAAUAFQAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiIBJscgwWSChwEQoKgKEGCCzdApI+aKC8dAEIDNxS4myi8jwu+C4ICshO+wI4FHLXKg0IvKDiSBS7PhB00noyyjBQEBBOLBUC6qYurKNuJJL433ogDagkxnYlC7/GHLWFNJrcSFcBBIAi7RR2E7ONGCAeRISAOubgUKUgXM24cGKIV6xGJMGWu+JAAoAABagBQhJCC4sEjByHdqFgB4EINCQMABDmxksAjCXbcpMgjQIGJNSZopuQpypGUCFGK3KJRYw0djSWBAFEAycU4QTQgrJlDhCEhCnPWfLFglpADtWoN2g6iIIOFALl48+YNBAAh+QQABQAWACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyDBZIKHARCgqAoQYILN0Ckj5ooLx0AQgM3FLibKLyPC74LggKyE77AjgUctcqDQi8oOJIFLs+EHTSejLKMuTcTiwVAupeKQmBKNRI3iiS+BIskKT09Ox/o8YwXTCk12AoVwEEgSMBDHVx442ZogoUYIA65OAcJyBgfKvIVgoci1iMhbXykEJEHADliAIAMe+QExkgodQBskVClFUcUohqB4JIiQxQHBUAwaODkhKAJ0h48YpBBg5OIFCQ0yBNTEAWKjSjIOKHA6p0GCIYwJAQiD9gtYwkZOOAkZ1qTHAeovZ1Ll24gACH5BAAFABcALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFQi6Tig8oKCSYiASbHJ4ACkEEQoKgKEGCJARABZCaKC8dAEIDNxS3myi7jwu9C4ICsQATvb+OBRy0yoNCLyg4kgUuz4QdNJFCqI3GjCsYMGudiQVAuduKQhg772+KJL0EiyQZWVlwM+y9ootDmoiYg61QARwEghQ8pMAFuFGGHswwAOIQhYWLcLQRAeWCIRLSYD0SAgEPEypVWl0CAETYoyomlXAxAEDNjyHDhPQC4ghEGyZNuswoIIBIkRlSBD148cJbIydNIhCpSMNGkQ8sBnVQAKnDFDVcAXQoUsSLGoiEBHwoYgEFWkI4DS4kWPdW0MO6ePPWDQQAIfkEAAUAGAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiIBJscngAKQQRCgqAoQYIkBEAFkJooLx0AQgM3FLebKLuPC70LggKxABO9v44FHLTKg0IvKDiSBS7PhB00kS6ojcaMQyIYI52JBUADBNiGQnhWcHAXiiS9oopCUWZmZW/49oxidEnigR0lHASCGDSkgAa4UYYWXEgg4BCFhYomzFHChY0hEtKAQHJRgQqZOF4E0VAgCEgvb40cLCETZoQaAFJipNklpNcERyDm0FwTo4CAIUPUUAPw4MUAjIaIhGnzpmKHGUOm3CMFAlKHEC2MgbgwJMFWiIJYDDkxDO0gBTcKfrqdS7euXUOBAAAh+QQABQAZACwAAAAAIAAgAAAH/4AAgoOEhYaHiImKi4yNjo+QkZKEBUIuk4oPKCgkmIgEmxyeAApBBEKCoChBgiQEQAWQMi0oLx0AQgM3FLibKLyPORC0C4ICsQATvsCOQFBfT8yDQi8oOJI4DsWHHTSPBS4kQgKNyIokXxoZIhuoiQVAAwS3iV52djw8ZQ7nvqKJM9wIFOhFkRBfrBKRoNMEypIGl97heKVgUSUSEUchIsEmBDlDFKQ5WnAgTo0EhkhUAwKJBoI4G+jUEaQAhCAgvtw1emNkwxwJTwAEeTLg1sFN2xgJkLDhS4UTAAqwoMUSwAN5FR3NcMqGnAA1tP4BOAZJgZQXyAqkoaqxEJAnLw1EtqWQta3du3jzKgoEACH5BAAFABoALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQFQi6Tig8oKCSYgx0FgwSbHJ4AaU0/QoKjKEGCJARAoY9zPSkGHQBCAzcUu5sov48SOz1GD4ICtBPBw444STtlT4ZCLyg4kjg/bLSFHTSPBTSWAo3fiSwbTUxJX52JBUADBLqIIEZY+zAwSIokgr3CtyGDQYMOFAkJBkRRiw1kyIxhEA9RARyyQCwCIUSIOFOJXCR4km4QhWePSDiZc6eFIRLYGj6iUIXOgTwJBIHQCABHsI+N2Jg4gODHDQAwB+hauGnBIyIHGCBxCaCVzAX1eDZSk6eImlAFbmwaCKBASUYTkonapA0kIV4EDRS4LWR2rt27ePMeCgQAIfkEAAUAGwAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiDFEKDBJscngAtTSlFgqMoQYIkBEAFkB5ZOlYGAEIDNxS7myi/jwxwWjsSggK0ABPBw444VHBnF4ZCLyg4khMlW8yFHTSPBTRCNOCK6Yhpc2RLER6hiQVAAwQdiSA1UVEaGniIKCIR7BUiAXSaKFQ4Q5GQYEAUSTHRps0IG/MQFcAhC8QiEC5cQDN1iEaaG+sEURjpyIWFPD9uGCKRLeIjEG+OVPmAQhAIjwBwBBvnCIWTKl5iPABAc0C+h5s6Fa1i4cIAVptsLrgHtJGCE2xkAihwY5PBsSkZCSDEYdMCkoUOKHDg0BWu3bt48+pdFAgAIfkEAAUAHAAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShAVCLpOKDygoJJiDNEKDBJscngAtUBlVgqMoQYIkBEAFkAdmVmUyAEIDNxS7myi/j0c8Z1Y5ggK0ABPBw44TZDx2dYZCLyg4khNeMsyFHTSPBRQuNOCK6YhSB2JhcTnjiQVAAwQKiQIVXV0RS0suKCIRDIi+O2MSJhyiSEhBRQMYmDDRwME8RAVwyAKxSAAFGh1MKerwwuAhCtAeUYjhhc0DQySymXx04kOdKdsAgOAIAMezRyRW1DnxZFzMASEdbrrkyAUbGWleAmhlcsGNIAIg2esEoMCNTa8ErZsUZNMCkYUUBJkwFq3bt3AF48pFFAgAIfkEAAUAHQAsAAAAACAAIAAAB/+AAIKDhIWGh4iJiouMjY6PkJGShA8XLpOECxOEX01SJJgAU0l4JYIUKkpSHKEVblduRAAUGWQoQYIkBEAFj04wbnZoBgBObTcUAEIozMmOD2EwaDwVghO9ABPMKM6ON9E+FoZCLyg4kg8fFwKHHTSQ7hTYi/OJL0dzEBBO74kFQAMIKEgkIM+aNm3EGGGjiMQ2IP6QfJk4kViiZcwgJuJQBQECJxe6HSqAYxeIRQI6UBgYSpECHEIQURDpCESIBE8uFSJRTuOjF1OeoNgEAMRJADi20XQZQuiLdzwHdFC2TWejAgNQvAAFgEBGQQtu4KjHSMECqzeY4RJEdhIQZgsPWhoSMOGa3Lt48+rdiykQACH5BAAFAB4ALAAAAAAgACAAAAf/gACCg4SFhoeIiYqLjI2Oj5CRkoQLRTMKk4JCFyGEdDs6R5kCBxgiFoIUeDs9Jpk0XBkpKg4AFBqsRIIkBEAFjwwaGVgYMgA2PFgoAEIozhSPExsaKjASggQPghPOKNCPHCMaIjOGQi8oOJIkKzEChx00kAoUHb+M94pCFjkSEiXfEBUAMoAApkRDGlTw4MFEAkUkugFRFIOBRYss9ElU5IKNAwcfTnRQVABHLxCMFChAmWmRABcjD1EI+KgABxQvXBgigW4iJG7OJggCwRJHN5qMCDh7IY/ngJHNnkECgpMENmc+F9xQB6mAi4MAbjgLMihfS6MorLY0JOCB2rVwB+PKnUtXbiAAOwAAAAAAAAAAAA=="
				},
				oSettings = $.extend(oDefaultSettings, oGIvenSettings);
				
			var elementClicked = function(e){
				e.preventDefault();
				iCurrentIndex = aElements.indexOf($(this).attr("href"));
				openBox();
			};
			var openBox = function(){
				$( window).scrollTop(0);
				$overlay.fadeIn("fast");
				$container.fadeIn("normal", function(){
					loadImage();
				});
			};

			var loadImage = function(){
				var $img = $("<img />")
				.css({
					borderRadius: 5,
					display: "block",
					cursor: "pointer"
				})
				.attr("src",aElements[iCurrentIndex])
				.hide()
				.appendTo("body")
				.on("load", function(){
					$container.animate({
						width: $img.width(),
						height: $img.height()
					}, "fast", function(){
						$img.remove().appendTo($container).show().on("click", nextImage);
					});
					
				});
				aElements[iCurrentIndex]
			};
			var nextImage = function(){
				var $curentImage; 
				if(aElements.length === 1){
					return closeBox();
				}
				iCurrentIndex = iCurrentIndex + 1 < aElements.length ? iCurrentIndex + 1 : 0;
				($curentImage = $container.find("img")).fadeOut("fast", function(){
					$container.find("img").remove();
					loadImage();
				});
			};
			var closeBox = function(){
				var $curentImage; 
				$container.fadeOut("fast");
				$overlay.fadeOut("normal", function(){
					$curentImage.remove();
				});
			};

			var setup = function(){
				// config
				$overlay = $("body > div."+ oSettings.cssPrefix +"overlay");
				$container = $("body > div."+ oSettings.cssPrefix +"container");
				if (!$overlay.size()){
					$overlay = $("<div class=\""+ oSettings.cssPrefix +"overlay\")></div>")
					.css({
						width: "100%",
						height: "100%",
						background: "rgba(0,0,0,0.5)",
						position: "fixed",
						top: 0,
						left: 0,
						zIndex: oSettings.zIndexBase,
						cursor: "pointer"
					})
					.hide()
					.appendTo("body")
					.on("click", closeBox);
				}

				if(!($container.size())){
					$container = $("<div class=\""+ oSettings.cssPrefix +"container\"></div>")
					.css({
						width: 50,
						height: 50,
						padding: 5,
						zIndex: oSettings.zIndexBase + 1,
						borderRadius: 5,
						boxShadow: "0 0 10px rgba(0,0,0,"+ oSettings.overlayOpacity +")",
						background: "white url(" + oSettings.spinner + ") center  no-repeat",
						position: "absolute",
						top: 0,
						right: 0,
						left: 0,
						bottom: 0,
						margin: "auto"
					})
					.hide()
					.appendTo("body");
				}
			};
			
			setup();

			return this.each( function(){
				aElements.push($(this).attr("href"));
				$(this).on("click",elementClicked);
			});

		};
		$( '.thumbnail' ).heplbox();

//        var $defaultdate = $("eventdateid").find("value");
//        $("#eventdateid").datepicker({
//            defaultDate: $defaultdate
//        });

		// Le calendrier s'ouvre au click sur l'icone
		$.datepicker.setDefaults({
		  showOn: "both",
		  buttonImageOnly: true,
		  buttonImage: "/assets/img/calendar.gif",
		  buttonText: "Calendar"
		});
		// On met le calendrier en français
		$.datepicker.setDefaults( $.datepicker.regional.fr );

		$("#eventdateid").datepicker({ dateFormat: "mm/dd/yy" });
		$("#eventdateid").datepicker("option", "dateFormat", "mm/dd/yy");


	// Feedback des flash messages
	$('.flash-message').delay(1500).slideUp().fadeOut();


	// Fonction de confirmation de suppression d'amis
	$('.removefriend').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous retirer cet utilisateur de vos amis?");
		if(r===true){
			//supprimer l'ami d'abord on le fait disparaitre
			$(this).parent().parent('.friend').fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().parent().attr("id");	
			var $url = "/unfriend/"+$id;
			$.ajax({
			      type: "POST",
			       url: $url,
			       data: $id
			});
		}
	});
	// Fonction de confirmation de suppression de podcast
	$('.podcastdeletion').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous supprimer ce podcast?");
		if(r===true){
			//supprimer le podcast d'abord on le fait disparaitre
			$(this).parent().parent('.podcast').fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().parent().attr("id");
			var $url = "/studio/podcasts/"+$id;
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});
	// Fonction de confirmation de suppression de photo
	$('.photodeletion').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous supprimer cette photo?");
		if(r===true){
			//supprimer la photo d'abord on le fait disparaitre
			$(this).parent().parent('.photo').fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().parent().attr("id");	
			var $url = "/studio/photos/"+$id;
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction de confirmation de suppression de video
	$('.videodeletion').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous supprimer cette vidéo?");
		if(r===true){
			$(this).parent().parent('.video').fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().parent().attr("id");	
			var $url = "/studio/videos/"+$id;
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction de confirmation de suppression d'évènement
	$('.eventdeletion').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous supprimer cet évènement?");
		if(r===true){
			$(this).parent().parent('.event').fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().parent().attr("id");	
			var $url = "/studio/events/"+$id;
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction de confirmation de suppression de genre
	$('.genredeletion').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Voulez-vous supprimer ce style?");
		if(r===true){
			$(this).parent().fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).parent().attr("id");	
			var $url = "/studio/genres/"+$id;
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction de confirmation de désactivation de profil
	$('#disableaccount').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Attention vous allez désactiver votre compte , êtes-vous sûr?");
		if(r===true){
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).attr("class");
			var $url = "/djs/"+$id+"/disable";
			$.ajax({
			      type: "POST",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction de confirmation de suppression de profil
	$('#deleteaccount').on('click', function(){
		//e.preventDefault();
		var r = window.confirm("Attention vous allez supprimer votre compte définitivement, êtes-vous sûr?");
		if(r===true){
			$(this).parent().fadeOut();
			// Ensuite on le supprime de la base de donnée
			var $id = $(this).attr("class");
			var $url = "/djs/"+$id+"/delete";
			$.ajax({
			      type: "DELETE",
			       url: $url,
			       data: $id
			});
		}
	});

	// Fonction gérant les podcasts aimés

	$('.likethis span').on('click',function(e){
		e.preventDefault();
		// On change l'apparence de l'icone

		var $iconlikedclass = '"'+$(this).attr("class")+'"';

		if ($iconlikedclass === '"icon-heart-empty"')
		{
			$(this).removeClass("icon-heart-empty").addClass("icon-heart");
			// On récupère le data-id
			var $liked_id = $(this).parent().attr("data-id");
			// met à jour dans la base de données 
			$.ajax({
				type: "get",
				url: '/podcasts/like/'+$liked_id,
				data: $liked_id
			});	
		}
		else
		{
			$(this).removeClass("icon-heart").addClass("icon-heart-empty").css("color","white");
		}
	});	

	// Fonction gérant le clic sur play

	$('.playthis span').on('click',function(e){
		e.preventDefault();
		// On change l'apparence de l'icone

		var $iconplayclass = '"'+$(this).attr("class")+'"';

		if ($iconplayclass === '"icon-play"')
		{
			$(this).removeClass("icon-play").addClass("icon-pause");
			// On récupère le data-id
			var $podcast_id = $(this).parent().attr("data-id");
			// met à jour dans la base de données 
			$.ajax({
				type: "get",
				url: '/podcasts/play/'+$podcast_id,
				data: $podcast_id
			});	
		}
		else
		{
			$(this).removeClass("icon-pause").addClass("icon-play").css("color","white");
		}

	});


//=============
//  FRIENDSHIP
//=============
    $('.follow').on('click',function(e){
        e.preventDefault();

        // on récupère le friendId
        var $friendid = $(this).attr("data-id");

        // On change le type du button
        $(this).removeClass("button-success").addClass("button-error");

        // On change le text en "ne plus suivre"
        $(this).text("Ne plus suivre");

        // On change la classe "follow" en "unfollow"
        $(this).removeClass("follow").addClass("unfollow");

        // On met à jour le nombre d'amis
//        var $friendsNb = parseInt($('.friendsNb').attr("data-id"));
//            $friendsNb = $friendsNb + 1;
//
//            $('.friendsNb').text($friendsNb);
        // On ajoute comme ami
        $.ajax({
            type: "post",
            url: "/friend/"+$friendid,
            data: $friendid
        });
    });
    $('.unfollow').on('click',function(e){
        e.preventDefault();

        // on récupère le friendId
        var $friendid = $(this).attr("data-id");

        // On change le type du button
        $(this).removeClass("button-error").addClass("button-success");

        // On change le text en "Suivre"
        $(this).text("Suivre");

        // On change la classe "unfollow" en "follow"
        $(this).removeClass("unfollow").addClass("follow");

        // On retire de ses amis
        $.ajax({
            type: "post",
            url: "/unfriend/"+$friendid,
            data: $friendid
        });

        // On le supprime de la liste
        $(this).parent().fadeOut();
    });

//=============
// RESPONSIVE
//=============

        //menuHeight  = menu.height();
  
    $('#pull').on('click', function(e) {
        e.preventDefault();  
        $('nav ul, nav section').slideToggle();
    });  

	// Fonction pour masquer le mot de passe sur la page d'inscription
	// var PassState = document.getElementById("unmask");

	// var function EditPassField(){
	//     var PassField = document.getElementById("password");
	//     if(PassState.checked){
	//         PassField.type = "password";
	//     }
	//     else{
	//         PassField.type = "text";
	//     }
	// };

	// PassState.checked.value = false;
	// PassState.addEventListener("click",EditPassField);
}).call(this, jQuery);